#include "StopPolarization/PolarizationReweight.h"


namespace StopPolarization{

  double PolarizationReweight::getReweightTopNeutralino(const xAOD::TruthParticleContainer *truth, 
							const float newThetaTop, 
							const float oriThetaTop)
  {
    TLorentzVector stop_hlv;
    TLorentzVector neut_hlv;
    TLorentzVector top_hlv;
    TLorentzVector wboson_hlv;
    TLorentzVector downtype_hlv;
    TLorentzVector uptype_hlv;
    TLorentzVector bottom_hlv;
    bool foundNeut = false;
    bool foundTop = false;
    bool foundWboson = false;
    bool foundDownType = false;
    bool foundUpType = false;
    bool foundBottom = false;
    bool selfDecay = false;
    int countReweightW = 0;
    int countReweightTop = 0;
    double newWeightW = 1.;
    double newWeightTop = 1.;
    const xAOD::TruthParticle* stop = 0;
    const xAOD::TruthParticle* top = 0;
    const xAOD::TruthParticle* wboson = 0;

    // loop over the truth particle in the container
    xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin();
    xAOD::TruthParticleContainer::const_iterator truth_end = truth->end();
    for( ; truth_itr != truth_end; ++truth_itr ) {
      if( (*truth_itr)->absPdgId()==1000006 ){
	stop = (*truth_itr);
	selfDecay = false;
	if( stop->nChildren()>0 ){
	  for(unsigned int i=0; i<stop->nChildren(); i++){
	    const xAOD::TruthParticle* child = stop->child(i);
	    if( child->pdgId()==(*truth_itr)->pdgId()){
	      stop = child;
	      selfDecay = true;
	      break;
	    }else if( child->absPdgId()==1000022 ){
	      neut_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundNeut = true;
	    }else if( child->isTop() ){
	      top_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundTop = true;
	      top = child;
	    }else if( child->isW() ){
	      wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundWboson = true;
	      wboson = child;
	    }else if( child->absPdgId()==5 ){
	      bottom_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundBottom = true;
	    }else if( child->absPdgId()==1 || child->absPdgId()==3 || child->isChLepton() ){
	      downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundDownType = true;
	    }else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
	      uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundUpType = true;
	    }
	  } // for nChildren
	} // nChildren>0
	if( selfDecay ) continue;

	stop_hlv.SetPtEtaPhiM( stop->pt(), stop->eta(), stop->phi(), stop->m() );

	if( foundTop ){
	  do{
	    if( m_generatorName=="MadGraphPythia8" ) top_hlv.SetPtEtaPhiM( top->pt(), top->eta(), top->phi(), top->m() );

	    selfDecay = false;
	    if( top->nChildren()>0 ){
	      for(unsigned int i=0; i<top->nChildren(); i++){
		const xAOD::TruthParticle* child = top->child(i);
		if( child->pdgId()==top->pdgId() ){
		  top = child;
		  selfDecay = true;
		  break;
		}else if( child->isW() ){
		  wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  wboson = child;
		  foundWboson = true;
		}else if( child->absPdgId()==5 || child->absPdgId()==1 || child->absPdgId()==3){
		  // treat down and strange as bottom also
		  bottom_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundBottom = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundTop
	
	if( foundWboson ){
	  do{
	    selfDecay = false;
	    if( wboson->nChildren()>0 ){
	      for(unsigned int i=0; i<wboson->nChildren(); i++){
		const xAOD::TruthParticle* child = wboson->child(i);
		if( child->pdgId()==wboson->pdgId() ){
		  wboson = child;
		  selfDecay = true;
		  break;
		}else if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
		  downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundDownType = true;
		}else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
		  uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundUpType = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundWboson
	
	double massTop = m_massTop;
	double widthTop = m_widthTop;
	double massTopThreshold = m_massTopThreshold;
	double massW = m_massW;
	double widthW = m_widthW;
	double massWThreshold = m_massWThreshold;

	if(!m_useGeV){
	  // scale to GeV unit
	  stop_hlv *= 0.001;
	  neut_hlv *= 0.001;
	  top_hlv *= 0.001;
	  wboson_hlv *= 0.001;
	  bottom_hlv *= 0.001;
	  downtype_hlv *= 0.001;
	  uptype_hlv *= 0.001;
	  massTop *= 0.001;
	  widthTop *= 0.001;
	  massTopThreshold *= 0.001;
	  massW *= 0.001;
	  widthW *= 0.001;
	  massWThreshold *= 0.001;
	}

	if( foundNeut && foundBottom && foundDownType && foundUpType ) {
	  double mstop = stop_hlv.M();
	  double mchi0 = neut_hlv.M();
	  double mb = bottom_hlv.M();

	  if(!foundWboson){
	    wboson_hlv = downtype_hlv + uptype_hlv;
	  }
	  double mw = wboson_hlv.M();

	  if(!foundTop){
	    top_hlv = wboson_hlv + bottom_hlv;
	  }
	  double mtop = top_hlv.M();

	  if(!foundTop){
	    if(m_decayPythia){
	      // boost by W
	      TVector3 boostVector = wboson_hlv.BoostVector();
	      bottom_hlv.Boost(-boostVector);
	      downtype_hlv.Boost(-boostVector);
	      double thetal = bottom_hlv.Angle(downtype_hlv.Vect());
	      
	      if(m_newIntegralW<0.){
		m_newIntegralW = 1.;
	      }
	      double numerator = (bottom_hlv.E()*bottom_hlv.E()-pow(bottom_hlv.P()*cos(thetal), 2)+mw*bottom_hlv.E()+mw*bottom_hlv.P()*cos(thetal)) / m_newIntegralW;
	      
	      double denominator = 1.;
	      
	      if(m_oriIntegralW<0.){
		if( m_phaseSpaceOnly && mtop-mb<massW ){
		  m_oriIntegralW = integrateMassWPhaseSpace(massTop, mb, massWThreshold);
		}else{
		  m_oriIntegralW = 1.;
		}
	      } // m_oriIntegralW<0
	      m_oriIntegralW = 1.;
	      
	      if( m_phaseSpaceOnly && mtop-mb<massW ){
		denominator = (pow(mw*mw-massW*massW,2)+massW*massW*widthW*widthW)/(2.*mw*mw)/m_oriIntegralW;
	      }else{
		denominator = 1./(2.*mw*mw*m_oriIntegralW);
	      }

	      if(denominator>0. && numerator>0.){
		newWeightW *= (numerator/denominator);
	      }
	      bottom_hlv.Boost(boostVector);
	      downtype_hlv.Boost(boostVector);
	      countReweightW++;
	    }
	  } // !foundTop
	
	  
	  // boost by top
	  TVector3 boostVector = top_hlv.BoostVector();
	  downtype_hlv.Boost(-boostVector);
	  neut_hlv.Boost(-boostVector);

	  Double_t thetal = neut_hlv.Angle(downtype_hlv.Vect());
	  Double_t newfactor = neut_hlv.P()*cos(2*newThetaTop)/(neut_hlv.E()+neut_hlv.M()*sin(2*newThetaTop));
	  Double_t orifactor = neut_hlv.P()*cos(2*oriThetaTop)/(neut_hlv.E()+neut_hlv.M()*sin(2*oriThetaTop));

	  if(m_newIntegralTop<0.){
	    if( mstop-mchi0<massTop ){
	      m_newIntegralTop = integrateMassTopBreitWignerMassW(mstop, mchi0, massTop, widthTop, TMath::Pi()*0.5, massW, widthW, mb, massWThreshold);
	    }else{
	      m_newIntegralTop = integrateMassWOnshell(mstop, mchi0, massTop, widthTop, massTopThreshold);
	    }
	  }
	  Double_t numerator = 1+newfactor*cos(thetal);

	  if( mstop-mchi0<massTop ){
	    numerator /= (2*m_newIntegralTop);
	  }else{
	    numerator /= m_newIntegralTop;
	  }

	  Double_t denominator = 1.;
	  if(m_decayPythia){
	    if( m_oriIntegralTop<0 ){
	      if( m_phaseSpaceOnly && mstop-mchi0<massTop ){
		m_oriIntegralTop = integrateMassTopPhaseSpaceMassWBreitWigner(mstop, mchi0, massW, widthW, mb, massWThreshold);
	      }else{
		if( mstop-mchi0<massTop ){
		  m_oriIntegralTop = integrateMassWBreitWigner(mstop, mchi0, massTop, widthTop, massTopThreshold);
		}else{
		  m_oriIntegralTop = integrateMassWOnshell(mstop, mchi0, massTop, widthTop, massTopThreshold);
		}
	      }
	    } // m_oriIntgralTop<0

	    if( m_phaseSpaceOnly && mstop-mchi0<massTop ){
	      denominator = (pow(mtop*mtop-massTop*massTop,2)+massTop*massTop*widthTop*widthTop)/(2.*m_oriIntegralTop);
	    }else{
	      if( mstop-mchi0<massTop ){
		denominator = 1./(2.*mtop*mtop*m_oriIntegralTop);
	      }else{
		denominator = 1./m_oriIntegralTop;
	      }
	    }
	  }else{
	    if(m_oriIntegralTop<0.){
	      m_oriIntegralTop = integrateMassWOnshell(mstop, mchi0, massTop, widthTop, massTopThreshold);
	    }
	    denominator = (1+orifactor*cos(thetal))/m_oriIntegralTop;
	  }
	  
	  if(denominator>0. && numerator>0.){
	    newWeightTop *= (numerator/denominator);
	  }
	  downtype_hlv.Boost(boostVector);
	  neut_hlv.Boost(boostVector);
	  countReweightTop ++;
	}

	foundNeut = false;
	foundTop = false;
	foundWboson = false;
	foundDownType = false;
	foundBottom = false;
      
      } // absPdgId()==1000006
    } // truth_itr
    
    if(countReweightTop!=2){
      std::cerr << "WARNING: There are not two stops in the event!" << std::endl;
    }
    return newWeightTop*newWeightW;

  } // getRweightTopNeutralino()


} // namespace StopPolarization
