#include "StopPolarization/PolarizationReweight.h"


namespace StopPolarization{

  double massWFunc(double *x, double *par)
  {
    float mw = x[0];
    
    float mchi0 = par[0];
    float mchip = par[1];
    float massW = par[2];
    float gammaW = par[3];
    float s2t = par[4];
    
    double phasespace = mchip*mchip*(mchip*mchip-2*mw*mw-2*mchi0*mchi0)+(mw*mw-mchi0*mchi0)*(mw*mw-mchi0*mchi0);
    if(phasespace>0. && mw>0.){
      return mw*sqrt(phasespace)*(pow(mchip*mchip-mchi0*mchi0-mw*mw,2)/3.+(mchip*mchip-mchi0*mchi0/3.-mw*mw -2*s2t*mchip*mchi0)*mw*mw)/(pow(mw*mw-massW*massW,2)+massW*massW*gammaW*gammaW);
    }else{
      return 0.;
    }
  }

  double massWBreitWignerFunc(double *x, double *par)
  {
    float mw = x[0];

    float mchi0 = par[0];
    float mchip = par[1];
    float massW = par[2];
    float gammaW = par[3];
    
    double phasespace = mchip*mchip*(mchip*mchip-2*mw*mw-2*mchi0*mchi0)+(mw*mw-mchi0*mchi0)*(mw*mw-mchi0*mchi0);
    if(phasespace>0. && mw>0.){
      return mw*sqrt(phasespace)/(pow(mw*mw-massW*massW,2)+massW*massW*gammaW*gammaW);
    }else{
      return 0.;
    }
  }

  double massWPhaseSpaceFunc(double *x, double *par)
  {
    float mw = x[0];

    float mchi0 = par[0];
    float mchip = par[1];

    double phasespace = mchip*mchip*(mchip*mchip-2*mw*mw-2*mchi0*mchi0)+(mw*mw-mchi0*mchi0)*(mw*mw-mchi0*mchi0);
    if(phasespace>0. && mw>0.){
      return mw*sqrt(phasespace); 
    }else{
      return 0.;
    }
  }

  double massWOnshellFunc(double *x, double *par)
  {
    float mw = x[0];

    float mchi0 = par[0];
    float mchip = par[1];
    float massW = par[2];
    float gammaW = par[3];
    
    double phasespace = mchip*mchip*(mchip*mchip-2*mw*mw-2*mchi0*mchi0)+(mw*mw-mchi0*mchi0)*(mw*mw-mchi0*mchi0);
    if(phasespace>0.){
      return 2*mw*mw*mw*sqrt(phasespace)/(pow(mw*mw-massW*massW,2)+massW*massW*gammaW*gammaW);
    }else{
      return 0.;
    }
  }

  double massTopBreitWignerMassWFunc(double *x, double *par)
  {
    float mtop = x[0];

    float mchi0 = par[0];
    float mstop = par[1];
    float massTop = par[2];
    float gammaTop = par[3];
    float thetaW = par[4];
    float massW = par[5];
    float gammaW = par[6];
    float massb = par[7];
    float massWThreshold = par[8];

    double phasespace = mstop*mstop*(mstop*mstop-2*mtop*mtop-2*mchi0*mchi0)+(mtop*mtop-mchi0*mchi0)*(mtop*mtop-mchi0*mchi0);
    if(phasespace>0. && mtop>massb){

      double F1 = integrateMassW(mtop, massb, thetaW, massW, gammaW, massWThreshold, 300);

      return sqrt(phasespace)*F1/mtop/(pow(mtop*mtop-massTop*massTop,2)+massTop*massTop*gammaTop*gammaTop);
    }else{
      return 0.;
    }
  }


  double massTopBreitWignerMassWBreitWignerFunc(double *x, double *par)
  {
    float mtop = x[0];

    float mchi0 = par[0];
    float mstop = par[1];
    float massTop = par[2];
    float gammaTop = par[3];
    float massW = par[4];
    float gammaW = par[5];
    float massb = par[6];
    float massWThreshold = par[7];

    double phasespace = mstop*mstop*(mstop*mstop-2*mtop*mtop-2*mchi0*mchi0)+(mtop*mtop-mchi0*mchi0)*(mtop*mtop-mchi0*mchi0);
    if(phasespace>0. && mtop>massb){
      
      double F1 = integrateMassWBreitWigner(mtop, massb, massW, gammaW, massWThreshold, 300);

      return sqrt(phasespace)*F1/mtop/(pow(mtop*mtop-massTop*massTop,2)+massTop*massTop*gammaTop*gammaTop);
    }else{
      return 0.;
    }
  }


  double massTopPhaseSpaceMassWBreitWignerFunc(double *x, double *par)
  {
    float mtop = x[0];
  
    float mchi0 = par[0];
    float mstop = par[1];
    float massW = par[2];
    float gammaW = par[3];
    float massb = par[4];
    float massWThreshold = par[5];

    double phasespace = mstop*mstop*(mstop*mstop-2*mtop*mtop-2*mchi0*mchi0)+(mtop*mtop-mchi0*mchi0)*(mtop*mtop-mchi0*mchi0);
    if(phasespace>0. && mtop>massb){

      double F1 = integrateMassWBreitWigner(mtop, massb, massW, gammaW, massWThreshold, 300);

      return sqrt(phasespace)*F1/mtop;
    }else{
      return 0.;
    }
  }

  double massZFunc(double *x, double *par)
  {
    float mz = x[0];
    
    float mchi1 = par[0];
    float mchi2 = par[1];
    float massZ = par[2];
    float gammaZ = par[3];
    
    double phasespace = mchi2*mchi2*(mchi2*mchi2-2*mz*mz-2*mchi1*mchi1)+(mz*mz-mchi1*mchi1)*(mz*mz-mchi1*mchi1);
    if(phasespace>0. && mz>0.){
      return 4*mz*sqrt(phasespace)*((mchi2+mchi1)*(mchi2+mchi1)-mz*mz)*((mchi2-mchi1)*(mchi2-mchi1)+2*mz*mz)/3./(pow(mz*mz-massZ*massZ,2)+massZ*massZ*gammaZ*gammaZ);
    }else{
      return 0.;
    }
  }


} // namespace StopPolarization
