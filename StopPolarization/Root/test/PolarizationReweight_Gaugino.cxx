#include "StopPolarization/PolarizationReweight.h"
#include <TF1.h>

namespace StopPolarization{

  double PolarizationReweight::getReweightGaugino(const xAOD::TruthParticleContainer *truth, 
						  const float newThetaW, 
						  const float oriThetaW)
  {
    TLorentzVector char1_hlv;
    TLorentzVector neut2_hlv;
    TLorentzVector neut1_hlv;
    TLorentzVector uptype_hlv;
    TLorentzVector downtype_hlv;
    TLorentzVector plustype_hlv;
    TLorentzVector minustype_hlv;
    TLorentzVector wboson_hlv;
    TLorentzVector zboson_hlv;
    bool foundWboson = false;
    bool foundZboson = false;
    bool foundNeut1 = false;
    bool foundUpType = false;
    bool foundDownType = false;
    bool foundPlusType = false;
    bool foundMinusType = false;
    bool selfDecay = false;
    int countReweightW = 0;
    int countReweightZ = 0;
    const xAOD::TruthParticle *char1 = 0;
    const xAOD::TruthParticle *neut2 = 0;
    const xAOD::TruthParticle *wboson = 0;
    const xAOD::TruthParticle *zboson = 0;
    double newWeightW = 1.;
    double newWeightZ = 1.;
    
    // loop over the truth particle in the container
    xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin();
    xAOD::TruthParticleContainer::const_iterator truth_end = truth->end();
    for( ; truth_itr != truth_end; ++truth_itr ) {

      if( (*truth_itr)->absPdgId()==1000024 ){
	char1 = (*truth_itr);
	if (char1->nChildren()>0 ){
	  selfDecay = false;
	  for(unsigned int i=0; i<char1->nChildren(); i++){
	    const xAOD::TruthParticle *child = char1->child(i);
	    if( child->pdgId()==char1->pdgId() ){
	      char1 = child;
	      selfDecay = true;
	      break;
	    }else if( child->absPdgId()==1000022 ){
	      neut1_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundNeut1 = true;
	    }else if( child->absPdgId()==24 ){
	      wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
              foundWboson = true;
              wboson = child;
	    }else if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
	      downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundDownType = true;
	    }else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
	      uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundUpType = true;
	    }
	  } // nChildren
	} // nChildren>0
	if( selfDecay ) continue;

	char1_hlv.SetPtEtaPhiM( char1->pt(), char1->eta(), char1->phi(), char1->m() );

	if( foundWboson ) {
	  do{
            if( wboson->nChildren() ){
              selfDecay = false;
              for(unsigned int i=0; i<wboson->nChildren(); i++){
                const xAOD::TruthParticle* child = wboson->child(i);
                if( child->pdgId()==wboson->pdgId() ){
                  wboson = child;
                  selfDecay = true;
                  break;
                }
                
                if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
                  downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
                  foundDownType = true;
		}else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
                  uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
                  foundUpType = true;
                }
              } // decayVertex
            } // hasDecayVtx
          }while(selfDecay);
        } // foundWboson

	double massW = m_massW;
	double widthW = m_widthW;
	double massWThreshold = m_massWThreshold;
	if(!m_useGeV){
	  char1_hlv *= 0.001;
	  neut1_hlv *= 0.001;
	  wboson_hlv *= 0.001;
	  downtype_hlv *= 0.001;
	  uptype_hlv *= 0.001;
	  massW *= 0.001;
	  widthW *= 0.001;
	  massWThreshold *= 0.001;
	}

	if( foundNeut1 && foundDownType && foundUpType ) {
	  double mchip = char1_hlv.M();
	  double mchi0 = neut1_hlv.M();
	  if(!foundWboson){
	    wboson_hlv = downtype_hlv + uptype_hlv;
	  }
	  double mw = wboson_hlv.M();

	  // boost by W
	  TVector3 boostVector = wboson_hlv.BoostVector();
	  neut1_hlv.Boost(-boostVector);
	  downtype_hlv.Boost(-boostVector);
	  double thetal = neut1_hlv.Angle( downtype_hlv.Vect() );

	  if(m_newIntegralW<0){
	    m_newIntegralW = integrateMassW(mchip, mchi0, newThetaW, massW, widthW, massWThreshold);
	  }

	  double numerator = (neut1_hlv.E()*neut1_hlv.E()-pow(neut1_hlv.P()*cos(thetal), 2) + mw*neut1_hlv.E() - cos(2*newThetaW)*mw*neut1_hlv.P()*cos(thetal) - sin(2*newThetaW)*mchi0*mchip) / m_newIntegralW;
          double denominator = 1;
          if(m_decayPythia){
            if(m_oriIntegralW<0){
              if( m_phaseSpaceOnly && mchip-mchi0<massW ){
                m_oriIntegralW = integrateMassWPhaseSpace(mchip, mchi0, massWThreshold);
              }else{
                if( mchip-mchi0<massW){
                  m_oriIntegralW = integrateMassWBreitWigner(mchip, mchi0, m_massW, m_widthW, m_massWThreshold);
                }else{
                  m_oriIntegralW = integrateMassWOnshell(mchip, mchi0, massW, widthW, massWThreshold);
                }
              }
            }
            if( m_phaseSpaceOnly && mchip-mchi0<massW ){
              denominator = (pow(mw*mw-massW*massW,2)+massW*massW*widthW*widthW)/(2.*mw*mw)/m_oriIntegralW;
            }else{
              if( mchip-mchi0<massW ){
                denominator = 1./(2.*mw*mw)/m_oriIntegralW;
              }else{
                denominator = 1./m_oriIntegralW;
              }
            }
          }else{
            if(m_oriIntegralW<0){
              m_oriIntegralW = integrateMassW(mchip, mchi0, oriThetaW, massW, widthW, massWThreshold);
            }
            denominator   = (neut1_hlv.E()*neut1_hlv.E()-pow(neut1_hlv.P()*cos(thetal), 2) + mw*neut1_hlv.E() - cos(2*oriThetaW)*mw*neut1_hlv.P()*cos(thetal) - sin(2*oriThetaW)*mchi0*mchip) / m_oriIntegralW;
          }

          if(denominator>0 && numerator>0){
            newWeightW *= (numerator/denominator);
          }       

	  neut1_hlv.Boost(boostVector);
	  downtype_hlv.Boost(boostVector);
          countReweightW++;
	} // found

	foundWboson = false;
	foundZboson = false;
	foundNeut1 = false;
	foundUpType = false;
	foundDownType = false;
	foundPlusType = false;
	foundMinusType = false;

      } // chargino
      

      if( (*truth_itr)->absPdgId()==1000023 ){
	neut2 = (*truth_itr);
	if (neut2->nChildren()>0 ){
	  selfDecay = false;
	  for(unsigned int i=0; i<neut2->nChildren(); i++){
	    const xAOD::TruthParticle *child = neut2->child(i);
	    if( child->pdgId()==neut2->pdgId() ){
	      neut2 = child;
	      selfDecay = true;
	      break;
	    }else if( child->absPdgId()==1000022 ){
	      neut1_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundNeut1 = true;
	    }else if( child->absPdgId()==23 ){
	      zboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
              foundZboson = true;
              zboson = child;
	    }
	  } // nChildren
	} // nChildren>0
	if( selfDecay ) continue;

	neut2_hlv.SetPtEtaPhiM( neut2->pt(), neut2->eta(), neut2->phi(), neut2->m() );

	if( foundZboson ) {
	  do{
            if( zboson->nChildren() ){
              selfDecay = false;
              for(unsigned int i=0; i<zboson->nChildren(); i++){
                const xAOD::TruthParticle* child = zboson->child(i);
                if( child->pdgId()==zboson->pdgId() ){
                  zboson = child;
                  selfDecay = true;
                  break;
                }
                
                if( child->pdgId()>0 ) {
                  plustype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
                  foundPlusType = true;
                }else if( child->pdgId()<0 ) {
                  minustype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
                  foundMinusType = true;
	        }
              } // decayVertex
            } // hasDecayVtx
          }while(selfDecay);
        } // foundZboson

	double massZ = m_massZ;
	double widthZ = m_widthZ;
	double massZThreshold = m_massZThreshold;
	if(!m_useGeV){
	  // scale to GeV unit internally
	  neut2_hlv *= 0.001;
	  zboson_hlv *= 0.001;
	  neut1_hlv *= 0.001;
	  plustype_hlv *= 0.001;
	  minustype_hlv *= 0.001;
	  massZ *= 0.001;
	  widthZ *= 0.001;
	  massZThreshold *= 0.001;
	}

	if( foundNeut1 && foundPlusType & foundMinusType) {
	  double mchi2 = neut2_hlv.M();
	  double mchi1 = neut1_hlv.M();
	  if(!foundZboson){
	    zboson_hlv = uptype_hlv + downtype_hlv;
	  }
	  double mz = zboson_hlv.M();

	  // boost by Z
	  TVector3 boostVector = zboson_hlv.BoostVector();
	  neut1_hlv.Boost(-boostVector);
	  plustype_hlv.Boost(-boostVector);
	  double theta = neut1_hlv.Angle( plustype_hlv.Vect() );
	  
	  if(m_newIntegralZ<0){
	    m_newIntegralZ = integrateMassZ(mchi2, mchi1, massZ, widthZ, massZThreshold);
	  }
	  double numerator = ((mchi2+mchi1)*(mchi2+mchi1)-mz*mz)/(mz*mz)*((mchi2-mchi1)*(mchi2-mchi1)+mz*mz - ((mchi2-mchi1)*(mchi2-mchi1)-mz*mz)*cos(theta)*cos(theta))/m_newIntegralZ;
	  double denominator = 1;

	  if(m_decayPythia){
	    if(m_oriIntegralZ<0){
	      if( m_phaseSpaceOnly && mchi2-mchi1<massZ ){
		m_oriIntegralZ = integrateMassWPhaseSpace(mchi2, mchi1, massZThreshold);
	      }else{
		if( mchi2-mchi1<massZ ){
		  m_oriIntegralZ = integrateMassWBreitWigner(mchi2, mchi1, massZ, widthZ, massZThreshold);
		}else{
		  m_oriIntegralZ = integrateMassWOnshell(mchi2, mchi1, massZ, widthZ, massZThreshold);
		}
	      }
	    }

	    if( m_phaseSpaceOnly && mchi2-mchi1<massZ ){
	      denominator = (pow(mz*mz-massZ*massZ,2)+massZ*massZ*widthZ*widthZ)/(2.*mz*mz)/m_oriIntegralZ;
	    }else{
	      if( mchi2-mchi1<massZ ){
		denominator = 1./(2.*mz*mz)/m_oriIntegralZ;
	      }else{
		denominator = 1./m_oriIntegralZ;
	      }
	    }
	  }else{
	    if(m_oriIntegralZ<0){
	      m_oriIntegralZ = integrateMassZ(mchi2, mchi1, massZ, widthZ, massZThreshold);
	    }
	    denominator = ((mchi2+mchi1)*(mchi2+mchi1)-mz*mz)/(mz*mz)*((mchi2-mchi1)*(mchi2-mchi1)+mz*mz - ((mchi2-mchi1)*(mchi2-mchi1)-mz*mz)*cos(theta)*cos(theta));
	  }

	  if( denominator>0 && numerator>0 ){
	    newWeightZ *= (numerator/denominator);
	  }

	  neut1_hlv.Boost(boostVector);
	  plustype_hlv.Boost(boostVector);
	  countReweightZ++;
	}
	foundWboson = false;
	foundZboson = false;
	foundNeut1 = false;
	foundUpType = false;
	foundDownType = false;
	foundPlusType = false;
	foundMinusType = false;

      } // neutralino2
      
    } // truth_itr

    if(countReweightW+countReweightZ!=2){
      std::cerr << "There are not two W or Z in the event." << std::endl;
      return 1.;
    }else{
      return newWeightW*newWeightZ;
    }

  } // getReweightGaugino()


} // StopPolarization
					  
