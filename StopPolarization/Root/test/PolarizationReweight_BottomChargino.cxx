#include "StopPolarization/PolarizationReweight.h"

namespace StopPolarization{

  double PolarizationReweight::getReweightBottomChargino(const xAOD::TruthParticleContainer *truth, 
							 const float newThetaW, 
							 const float oriThetaW,
							 const float newThetaCh,
							 const float oriThetaCh)
  {
    TLorentzVector stop_hlv;
    TLorentzVector char1_hlv;
    TLorentzVector bottom_hlv;
    TLorentzVector neut_hlv;
    TLorentzVector wboson_hlv;
    TLorentzVector downtype_hlv;
    TLorentzVector uptype_hlv;
    bool foundChar1 = false;
    bool foundBottom = false;
    bool foundNeut = false;
    bool foundWboson = false;
    bool foundDownType = false;
    bool foundUpType = false;
    int countReweightW = 0;
    int countReweightCh = 0;
    double newWeightW = 1.;
    double newWeightCh = 1.;
    bool selfDecay = false;
    const xAOD::TruthParticle *stop = 0;
    const xAOD::TruthParticle *wboson = 0;
    const xAOD::TruthParticle *char1 = 0;

    // loop over the truth particle in the container
    xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin();
    xAOD::TruthParticleContainer::const_iterator truth_end = truth->end();
    for( ; truth_itr != truth_end; ++truth_itr ) {

      if( (*truth_itr)->absPdgId()==1000006 ){
	stop = (*truth_itr);
	if( stop->nChildren()>0 ){
	  selfDecay = false;
	  for(unsigned int i=0; i<stop->nChildren(); i++){
	    const xAOD::TruthParticle* child = stop->child(i);
	    if( child->pdgId()==(*truth_itr)->pdgId()){
	      stop = child;
	      selfDecay = true;
	      break;
	    }else if( child->absPdgId()==1000024 ){
	      char1_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundChar1 = true;
	      char1 = child;
	    }else if( child->absPdgId()==5 ){
	      bottom_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundBottom = true;
	    }
	  } // for nChildren
	} // nChildren>0
	if( selfDecay ) continue;

	stop_hlv.SetPtEtaPhiM( stop->pt(), stop->eta(), stop->phi(), stop->m() );

	if( foundChar1 ){
	  do{
	    if(m_generatorName=="MadGraphPythia8") char1_hlv.SetPtEtaPhiM( char1->pt(), char1->eta(), char1->phi(), char1->m() );
	    if( char1->nChildren()>0 ){
	      selfDecay = false;
	      for(unsigned int i=0; i<char1->nChildren(); i++){
		const xAOD::TruthParticle* child = char1->child(i);
		if( child->pdgId()==char1->pdgId() ){
		  char1 = child;
		  selfDecay = true;
		  break;
		}else if( child->isW() ){
		  wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  wboson = child;
		  foundWboson = true;
		}else if( child->absPdgId()==1000022 ){
		  neut_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundNeut = true;
		}else if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
		  downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundDownType = true;
		}else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
		  uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundUpType = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundChar1

	if( foundWboson ){
	  do{
	    if( wboson->nChildren()>0 ){
	      selfDecay = false;
	      for(unsigned int i=0; i<wboson->nChildren(); i++){
		const xAOD::TruthParticle* child = wboson->child(i);
		if( child->pdgId()==wboson->pdgId() ){
		  wboson = child;
		  selfDecay = true;
		  break;
		}else if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
		  downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundDownType = true;
		}else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
		  uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundUpType = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundWboson
      
	double massW = m_massW;
	double widthW = m_widthW;
	double massWThreshold = m_massWThreshold;
	if(!m_useGeV){
	  // scale to GeV unit internally
	  stop_hlv *= 0.001;
	  char1_hlv *= 0.001;
	  neut_hlv *= 0.001;
	  wboson_hlv *= 0.001;
	  downtype_hlv *= 0.001;
	  uptype_hlv *= 0.001;
	  massW *= 0.001;
	  widthW *= 0.001;
	  massWThreshold *= 0.001;
	}

	if( foundNeut && foundChar1 && foundDownType && foundUpType ){
	  double mchip = char1_hlv.M();
	  double mchi0 = neut_hlv.M();
	  if(!foundWboson){
	    wboson_hlv = downtype_hlv + uptype_hlv;
	  }
	  double mw = wboson_hlv.M();

	  // boost by W
	  TVector3 boostVector = wboson_hlv.BoostVector();
	  neut_hlv.Boost(-boostVector);
	  downtype_hlv.Boost(-boostVector);
	  double thetal = neut_hlv.Angle(downtype_hlv.Vect());
	  
	  if(m_newIntegralW<0){
	    m_newIntegralW = integrateMassW(mchip, mchi0, newThetaW, massW, widthW, massWThreshold);
	  }
	  double numerator = (neut_hlv.E()*neut_hlv.E()-pow(neut_hlv.P()*cos(thetal), 2) + mw*neut_hlv.E() - cos(2*newThetaW)*mw*neut_hlv.P()*cos(thetal) - sin(2*newThetaW)*mchi0*mchip) / m_newIntegralW;
	  double denominator = 1;
	  if(m_decayPythia){
	    if(m_oriIntegralW<0){
	      if( m_phaseSpaceOnly && mchip-mchi0<massW ){
		m_oriIntegralW = integrateMassWPhaseSpace(mchip, mchi0, massWThreshold);
	      }else{
		if( mchip-mchi0<massW){
		  m_oriIntegralW = integrateMassWBreitWigner(mchip, mchi0, massW, widthW, massWThreshold);
		}else{
		  m_oriIntegralW = integrateMassWOnshell(mchip, mchi0, massW, widthW, massWThreshold);
		}
	      }
	    }
	    if( m_phaseSpaceOnly && mchip-mchi0<massW ){
	      denominator = (pow(mw*mw-massW*massW,2)+massW*massW*widthW*widthW)/(2.*mw*mw)/m_oriIntegralW;
	    }else{
	      if( mchip-mchi0<massW ){
		denominator = 1./(2.*mw*mw)/m_oriIntegralW;
	      }else{
		denominator = 1./m_oriIntegralW;
	      }
	    }
	  }else{
	    if(m_oriIntegralW<0){
	      m_oriIntegralW = integrateMassW(mchip, mchi0, oriThetaW, massW, widthW, massWThreshold);
	    }
	    denominator   = (neut_hlv.E()*neut_hlv.E()-pow(neut_hlv.P()*cos(thetal), 2) + mw*neut_hlv.E() - cos(2*oriThetaW)*mw*neut_hlv.P()*cos(thetal) - sin(2*oriThetaW)*mchi0*mchip) / m_oriIntegralW;
	  }
	  //std::cout << "numerator = " << numerator << " denominator = " << denominator << std::endl;

	  if(denominator>0 && numerator>0){
	    newWeightW *= (numerator/denominator);
	  }	  
	  neut_hlv.Boost(boostVector);
	  downtype_hlv.Boost(boostVector);
	  countReweightW++;

	  // boost by chargino
	  if(foundBottom){
	    boostVector = char1_hlv.BoostVector();
	    downtype_hlv.Boost(-boostVector);
	    uptype_hlv.Boost(-boostVector);
	    bottom_hlv.Boost(-boostVector);
	    double El = downtype_hlv.E();
	    double Enu = uptype_hlv.E();
	    thetal = bottom_hlv.Angle(downtype_hlv.Vect());
	    Double_t thetanu = bottom_hlv.Angle(uptype_hlv.Vect());
	    Double_t newfactor = bottom_hlv.P()*cos(2*newThetaCh)/(bottom_hlv.E()-bottom_hlv.M()*sin(2*newThetaCh));
	    Double_t orifactor = bottom_hlv.P()*cos(2*oriThetaCh)/(bottom_hlv.E()-bottom_hlv.M()*sin(2*oriThetaCh));

	    numerator = El*Enu*((1-cos(2*newThetaW))*(1+newfactor*cos(thetal))*(2*mchip-mw*mw/Enu)+(1+cos(2*newThetaW))*(1-newfactor*cos(thetanu))*(2*mchip-mw*mw/El)) - sin(2*newThetaW)*mchi0*(mw*mw+2*El*Enu*newfactor*(cos(thetal)-cos(thetanu)));
	    denominator = El*Enu*((1-cos(2*newThetaW))*(1+orifactor*cos(thetal))*(2*mchip-mw*mw/Enu)+(1+cos(2*newThetaW))*(1-orifactor*cos(thetanu))*(2*mchip-mw*mw/El)) - sin(2*newThetaW)*mchi0*(mw*mw+2*El*Enu*orifactor*(cos(thetal)-cos(thetanu)));
	    
	    if(denominator>0 && numerator>0){
	      newWeightCh *= (numerator/denominator);
	    }
	    downtype_hlv.Boost(boostVector);
	    uptype_hlv.Boost(boostVector);
	    bottom_hlv.Boost(boostVector);
	    countReweightCh++;
	  }
	}
	
	foundNeut = false;
	foundChar1 = false;
	foundWboson = false;
	foundDownType = false;
	foundUpType = false;
	foundBottom = false;

      } // pdgId()==1000006
    } // truth_itr
    
    if(countReweightW!=2){
      std::cerr << "There are not two W in the event." << std::endl;
      return 1.;
    }else if(countReweightCh!=2){
      std::cerr << "There are not two chargino in the event." << std::endl;
      return 1.;
    }else{
      return newWeightW*newWeightCh;
    }
  } // getReweightChargino()

}
