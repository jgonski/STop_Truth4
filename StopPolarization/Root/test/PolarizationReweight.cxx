#include "StopPolarization/PolarizationReweight.h"
#include <TF1.h>


namespace StopPolarization{
  
  PolarizationReweight::PolarizationReweight()
  {
    m_massW = 80399.; // MeV
    m_widthW = 2085.; // MeV
    m_massWThreshold = 0.;

    m_oriIntegralW = -1.;
    m_newIntegralW = -1.;
    
    m_massZ = 91187.6; // MeV
    m_widthZ = 2495.2; // MeV
    m_massZThreshold = 0.;
    
    m_oriIntegralZ = -1.;
    m_newIntegralZ = -1.;

    m_massTop = 172500.; // MeV
    m_widthTop = 1333.13; // MeV
    m_massTopThreshold = 86000.; // MeV

    m_oriIntegralTop = -1.;
    m_newIntegralTop = -1.;

    m_useGeV = false; // default unit is MeV

    m_generatorName = "MadGraphPythia8";
    m_decayPythia = true;
    m_phaseSpaceOnly = true;
  }

  //----------------------------------------------------------------------------------------------------------------------------

  double effTopTheta(const double s11, const double s12,
		     const double n11, const double n12, const double n13, const double n14, 
		     const double yt, const double alphaEM, const double mW, const double mZ) 
  {
    //    const double g1 = sqrt(GF*4*sqrt(2))*mW;
    if( alphaEM==0.){
      std::cerr << "Invalied value for alphaEM." << std::endl;
      return 0.;
    }else if( mW==0. ){
      std::cerr << "Invalid value for mW." << std::endl;
      return 0.;
    }

    const double g1 = 2*sqrt(TMath::Pi()/alphaEM);
    const double g2 = g1*sqrt(mZ*mZ-mW*mW)/mW;
    
    double denominator = sqrt(2)*(0.5*g1*n12+g2/6.*n11)*s11-yt*n14*s12;
    double numerator = yt*n14*s11+2*sqrt(2)/3.*g2*n11*s12;
    if( denominator!=0. ){
      return atan2(numerator, denominator);
    }else{
      if(numerator>0) return TMath::Pi()*0.5;
      else return -TMath::Pi()*0.5;
    }    
  } // effTopTheta()

  //-----------------------------------------------------------------------------------------------------------------------------

  double effWTheta(const double n11, const double n12, const double n13, const double n14, 
		   const double u11, const double u12, const double v11, const double v12) 
  {
    double denominator = n13*u12+sqrt(2)*n12*u11;
    double numerator = -n14*v12+sqrt(2)*n12*v11;
    if( denominator!=0. ){
      return atan2(numerator, denominator);
    }else{
      if(numerator>0) return TMath::Pi()*0.5;
      else return -TMath::Pi()*0.5;
    }
  } // effWTheta()

  //-------------------------------------------------------------------------------------------------------------------------------

  double effChiTheta(const double s11, const double s12,
		     const double u11, const double u12, const double v11, const double v12, 
		     const double yb, const double yt, const double alphaEM, const double mW, const double mZ) 
  {
    if( alphaEM==0. ){
      std::cerr << "Invalid value for alphaEM." << std::endl;
      return 0.;
    }else if( mW==0. ){
      std::cerr << "Invalid value for mW." << std::endl;
      return 0.;
    }

    const double g1 = 2*sqrt(TMath::Pi()/alphaEM);
    const double g2 = g1*sqrt(mZ*mZ-mW*mW)/mW;
    
    double denominator = -g2*v11*s11-yt*v12*s12;
    double numerator = -yb*u12*s11;
    if( denominator!=0. ){
      return atan2(numerator, denominator);
    }else{
      if(numerator>0) return TMath::Pi()*0.5;
      else return -TMath::Pi()*0.5;
    }    
  } // effChiTheta()

  //-------------------------------------------------------------------------------------------------------------------------------



  double integrateMassW(const double mchip, const double mchi0, const double thetaW,
			const double massW, const double widthW, const double massWThreshold,
			const int np)
  {
    const double xmax = mchip - mchi0;
    const double xmin = massWThreshold;

    TF1 *f1 = new TF1("f1", massWFunc, xmin, xmax, 5);
    f1->SetParameters(mchi0, mchip, massW, widthW, sin(2*thetaW));

    // IntegralFast
    //int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }


  double integrateMassWBreitWigner(const double mchip, const double mchi0,
				   const double massW, const double widthW, const double massWThreshold,
				   const int np)
  {
    const double xmax = mchip - mchi0;
    const double xmin = massWThreshold;

    TF1 *f1 = new TF1("f1", massWBreitWignerFunc, xmin, xmax, 4);
    f1->SetParameters(mchi0, mchip, massW, widthW);

    // IntegralFast
    //int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }

  double integrateMassWPhaseSpace(const double mchip, const double mchi0, const double massWThreshold,
				  const int np)
  {
    const double xmax = mchip - mchi0;
    const double xmin = massWThreshold;
    
    TF1 *f1 = new TF1("f1", massWPhaseSpaceFunc, xmin, xmax, 2);
    f1->SetParameters(mchi0, mchip);

    // IntegralFast
    //int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }

  double integrateMassWOnshell(const double mchip, const double mchi0,
			       const double massW, const double widthW, const double massWThreshold)
  {
    const double xmax = mchip - mchi0;
    const double xmin = massWThreshold;

    TF1 *f1 = new TF1("f1", massWOnshellFunc, xmin, xmax, 4);
    f1->SetParameters(mchi0, mchip, massW, widthW);

    // IntegralFast
    int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }

  double integrateMassTopBreitWignerMassWBreitWigner(const double mstop, const double mchi0, const double massTop, const double widthTop,
						     const double massW, const double widthW, const double massb, const double massWThreshold)
  {
    const double xmax = mstop - mchi0;
    const double xmin = massWThreshold;

    TF1 *f1 = new TF1("f1", massTopBreitWignerMassWBreitWignerFunc, xmin, xmax, 8);
    f1->SetParameters(mchi0, mstop, massTop, widthTop, massW, widthW, massb, massWThreshold);

    // IntegralFast
    int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }

  double integrateMassTopBreitWignerMassW(const double mstop, const double mchi0, const double massTop, const double widthTop,
					  const double thetaW, const double massW, const double widthW, const double massb, const double massWThreshold)
  {
    const double xmax = mstop - mchi0;
    const double xmin = massWThreshold;

    TF1 *f1 = new TF1("f1", massTopBreitWignerMassWFunc, xmin, xmax, 9);
    f1->SetParameters(mchi0, mstop, massTop, widthTop, thetaW,  massW, widthW, massb, massWThreshold);

    // IntegralFast
    int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;
  }


  double integrateMassTopPhaseSpaceMassWBreitWigner(const double mstop, const double mchi0, 
						    const double massW, const double widthW, const double massb,
						    const double massWThreshold)
  {
    const double xmax = mstop - mchi0;
    const double xmin = massWThreshold;
    
    TF1 *f1 = new TF1("f1", massTopPhaseSpaceMassWBreitWignerFunc, xmin, xmax, 6);
    f1->SetParameters(mchi0, mstop, massW, widthW, massb, massWThreshold);

    // IntegralFast
    int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;
    delete f1;

    return F1;

  }

  double integrateMassZ(const double mchi2, const double mchi1,
					      const double massZ, const double widthZ, const double massZThreshold)
  {
    const double xmax = mchi2 - mchi1;
    const double xmin = massZThreshold;

    TF1 *f1 = new TF1("f1", massZFunc, xmin, xmax, 4);
    f1->SetParameters(mchi1, mchi2, massZ, widthZ);

    // IntegralFast
    int np = 1000;
    double *x = new double[np];
    double *w = new double[np];
    f1->CalcGaussLegendreSamplingPoints(np, x, w, 1e-15);
    double F1 = f1->IntegralFast(np, x, w, xmin, xmax);

    delete [] x;
    delete [] w;

    return F1;
  }


} // namespace StopPolarization
