#ifndef PolarizationReweight_H
#define PolarizationReweight_H

#include <iostream>
#include <cmath>
#include <string>

#include <TLorentzVector.h>
#include <TVector3.h>
#include <TMath.h>

#include "xAODTruth/TruthParticleContainer.h"

//-------------------------------------------------------------------------------//
// PolarozatonReweight provides weight for stop polarization reweighting         //
// code is validate for Herwig++ and MadGraph+Pythia8 (with Pythia8 stop decay)  //
//                                                                               //
// see the following wiki for detail                                             //
// https://twiki.cern.ch/twiki/bin/view/Main/StopPolarization                    //
//                                                                               //
// default unit is MeV                                                           //
// if you want to use GeV unit,                                                  //
// do PolarizationReweight::setUnitGeV(true);                                    //
//-------------------------------------------------------------------------------//

namespace StopPolarization{

  class PolarizationReweight
  {
  public:
    
    PolarizationReweight();
    ~PolarizationReweight(){}

    //----------------------------------------------------------------------------------//
    // return weight for polarization reweighting                                       //
    // ~t1->t+~chi10 decay is only considered                                           //
    //                                                                                  //
    // newThetaTop [rad]: effective polarization angle of top to be reweighted          //
    // oriThetaTop [rad]: orignal effective polarization angle of top                   //
    //----------------------------------------------------------------------------------//
    
    double getReweightTopNeutralino(const xAOD::TruthParticleContainer *truth, 
				    const float newThetaTop, 
				    const float oriThetaTop);
    
    //-----------------------------------------------------------------------------//
    // calculate effective top mixing angle for  ~t1->t+~chi10 decay               //
    //-----------------------------------------------------------------------------//
    
    double effTopTheta(const double s11, const double s12, // ~t1 mixing matrix component
		       const double n11, const double n12, const double n13, const double n14, // ~chi10 mixing matrix component
		       const double yt, const double alphaEM, const double mW, const double mZ); // SM parameters
    
    //----------------------------------------------------------------------------------//
    // return weight for polarization reweighting                                       //
    // ~chi+->W+~chi10 decay is only considered                                         //
    //                                                                                  //
    // newThetaW [rad]: effective polarization angle of W to be reweighted              //
    // oriThetaW [rad]: orignal effective polarization angle of W                       //
    // newThetaCh [rad]: effective polarization angle of chargino to be reweighted      //
    // oriThetaCh [rad]: original effective polarization angle of chargino              //
    //----------------------------------------------------------------------------------//
    
    double getReweightBottomChargino(const xAOD::TruthParticleContainer *truth, 
				     const float newThetaW, 
				     const float oriThetaW,
				     const float newThetaCh,
				     const float oriThetaCh);
    
    //-----------------------------------------------------------------------------------//
    // calculate effective W mixing angle for ~chi1+ -> W+ ~chi10 decay                  //
    //-----------------------------------------------------------------------------------//
    
    double effWTheta(const double n11, const double n12, const double n13, const double n14, // ~chi10 mixing matrix component
		     const double u11, const double u12, const double v11, const double v12); // ~chi1+ mixing matrix coimponent
    
    //------------------------------------------------------------------------------------//
    // calculate effective ~chi1+ mixing angle for ~t1 -> ~chi1+ b decay                  //
    //------------------------------------------------------------------------------------//
    
    double effChiTheta(const double s11, const double s12, // ~t1 mixing matrix component
		       const double u11, const double u12, const double v11, const double v12, // ~chi1+ mixing matrix component
		       const double yb, const double yt, const double alphaEM, const double mW, const double mZ); // SM parameters

    void setMassW(const double massW){ m_massW = massW; }

    void setWidthW(const double widthW){ m_widthW = widthW; }

    void setMassZ(const double massZ){ m_massZ = massZ; }
    
    void setWidthZ(const double widthZ){ m_widthZ = widthZ; }

    void setMassTop(const double massTop){ m_massTop = massTop; }

    void setWidthTop(const double widthTop){ m_widthTop = widthTop; }

    // set a threshold if there is a lower bound in W mass distribution
    void setMassWThreshold(const double massWThreshold){ m_massWThreshold = massWThreshold; }

    // set a threshold if there is a lower bound in Z mass distribution
    void setMassZThreshold(const double massZThreshold){ m_massZThreshold = massZThreshold; }

    // set a threshold if there is a lower bound in top mass distribution
    void setMassTopThreshold(const double massTopThreshold){ m_massTopThreshold = massTopThreshold; }

    void clearIntegralW(){ m_newIntegralW = -1.; m_oriIntegralW = -1.; }

    void clearIntegralZ(){ m_newIntegralZ = -1.; m_oriIntegralZ = -1.; }

    void clearIntegralTop(){ m_newIntegralTop = -1.; m_oriIntegralTop = -1.; }

    // set true if GeV unit is used
    void setUnitGeV(const bool useGeV=true){ m_useGeV = useGeV; }

    // set true if MeV unit is used
    void setUnitMeV(const bool useMeV=true){ m_useGeV = (!useMeV); }

    // set generatorName="MadGraphPythia8" if the generator is MadGraph+Pythia8
    void setGeneratorName(const std::string generatorName){ m_generatorName = generatorName; }
    
    // set true if particle decay performed in Pythia(8) with MadGraph
    void setDecayPythia(const bool decayPythia){ m_decayPythia = decayPythia; }
    
    // set true if off-shell decay performed phase space calculation only
    void setPhaseSpaceOnly(const bool phaseSpaceOnly){ m_phaseSpaceOnly = phaseSpaceOnly; }

    //----------------------------------------------------------------------------------//
    // return weight for polarization reweighting                                       //
    // ~chi+->W+~chi10 and ~chi20->Z+~chi20 decays are considered                       //
    //                                                                                  //
    // newThetaW [rad]: effective polarization angle of W to be reweighted              //
    // oriThetaW [rad]: orignal effective polarization angle of W                       //
    //----------------------------------------------------------------------------------//

    double getReweightGaugino(const xAOD::TruthParticleContainer *truth, 
			      const float newThetaW, 
			      const float oriThetaW);

  private:

    bool m_useGeV;
    bool m_decayPythia;
    bool m_phaseSpaceOnly;

    double m_newIntegralW;
    double m_oriIntegralW;
    double m_massW;
    double m_widthW;
    double m_massWThreshold;

    double m_newIntegralZ;
    double m_oriIntegralZ;
    double m_massZ;
    double m_widthZ;
    double m_massZThreshold;

    double m_newIntegralTop;
    double m_oriIntegralTop;
    double m_massTop;
    double m_widthTop;
    double m_massTopThreshold;

    std::string m_generatorName;
  };

  double integrateMassW(const double mchip, const double mchi0, const double thetaW, 
			const double massW, const double widthW, const double massWThreshold,
			const int np=1000);
  double integrateMassWBreitWigner(const double mchip, const double mchi0,
				   const double massW, const double widthW, const double massWThreshold,
				   const int np=1000);
  double integrateMassWPhaseSpace(const double mchip, const double mchi0, const double massWThreshold,
				  const int np=1000);
  double integrateMassWOnshell(const double mchip, const double mchi0,
			       const double massW, const double widthW, const double massWThreshold);
  double integrateMassTopBreitWignerMassW(const double mstop, const double mchi0, const double massTop, const double widthTop,
					  const double thetaW, const double massW, const double widthW, const double massb, const double massWThreshold);
  double integrateMassTopBreitWignerMassWBreitWigner(const double mstop, const double mchi0, const double massTop, const double widthTop,
						     const double massW, const double widthW, const double massb, const double massWThreshold);
  double integrateMassTopPhaseSpaceMassWBreitWigner(const double mstop, const double mchi0,
						    const double massW, const double widthW, const double massb,
						    const double massTopThreshold);
  double integrateMassZ(const double mchi2, const double mchi1, 
			  const double massZ, const double widthZ, const double massZThreshold);

  double massWFunc(double *x, double *par);
  double massWBreitWignerFunc(double *x, double *par);
  double massWPhaseSpaceFunc(double *x, double *par);
  double massWOnshellFunc(double *x, double *par);
  double massTopBreitWignerMassWFunc(double *x, double *par);
  double massTopBreitWignerMassWBreitWignerFunc(double *x, double *par);
  double massTopPhaseSpaceMassWBreitWignerFunc(double *x, double *par);
  double massZFunc(double *x, double *par);

} // namespace StopPolarization

#endif // PolarizationReweight_H
