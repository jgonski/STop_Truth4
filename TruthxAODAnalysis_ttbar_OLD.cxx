#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <MyAnalysis/TruthxAODAnalysis_ttbar.h>
#include <MyAnalysis/Util.h>
#include <TString.h>

#include "xAODRootAccess/tools/Message.h"

using namespace MyAnalysis_ttbar;
using namespace RestFrames;

ClassImp(TruthxAODAnalysis_ttbar)

TruthxAODAnalysis_ttbar :: TruthxAODAnalysis_ttbar ()
{
}

EL::StatusCode TruthxAODAnalysis_ttbar :: setupJob (EL::Job& job)
{
  job.useXAOD ();
  xAOD::Init( "TruthxAODAnalysis_ttbar" ).ignore(); // call before opening first file 
  EL_RETURN_CHECK( "setupJob()", xAOD::Init() );
  
  EL::OutputStream treeOutput ( "TruthTree" );
  job.outputAdd (treeOutput);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: histInitialize ()
{
  if (verbose) std::cout << "In histInitialize " << std::endl;

  TFile *outputFile = wk()->getOutputFile ("TruthTree");
  tree = new TTree ("TruthTree", "TruthTree");
  tree->SetDirectory(outputFile);

  tree->Branch( "stop1_pt", &m_stop1_pt );
  tree->Branch( "stop1_b_pt", &m_stop1_b_pt );
  tree->Branch( "stop1_w1_pt", &m_stop1_w1_pt );
  tree->Branch( "stop1_w2_pt", &m_stop1_w2_pt );
  tree->Branch( "stop1_xi_pt", &m_stop1_xi_pt );
  tree->Branch( "stop1_eta", &m_stop1_eta );
  tree->Branch( "stop1_b_eta", &m_stop1_b_eta );
  tree->Branch( "stop1_w1_eta", &m_stop1_w1_eta );
  tree->Branch( "stop1_w2_eta", &m_stop1_w2_eta );
  tree->Branch( "stop1_xi_eta", &m_stop1_xi_eta );
  tree->Branch( "stop1_phi", &m_stop1_phi );
  tree->Branch( "stop1_b_phi", &m_stop1_b_phi );
  tree->Branch( "stop1_w1_phi", &m_stop1_w1_phi );
  tree->Branch( "stop1_w2_phi", &m_stop1_w2_phi );
  tree->Branch( "stop1_xi_phi", &m_stop1_xi_phi );
  tree->Branch( "stop1_m", &m_stop1_m );
  tree->Branch( "stop1_b_m", &m_stop1_b_m );
  tree->Branch( "stop1_w1_m", &m_stop1_w1_m );
  tree->Branch( "stop1_w2_m", &m_stop1_w2_m );
  tree->Branch( "stop1_xi_m", &m_stop1_xi_m );
  tree->Branch( "stop2_pt", &m_stop2_pt );
  tree->Branch( "stop2_b_pt", &m_stop2_b_pt );
  tree->Branch( "stop2_w1_pt", &m_stop2_w1_pt );
  tree->Branch( "stop2_w2_pt", &m_stop2_w2_pt );
  tree->Branch( "stop2_xi_pt", &m_stop2_xi_pt );
  tree->Branch( "stop2_eta", &m_stop2_eta );
  tree->Branch( "stop2_b_eta", &m_stop2_b_eta );
  tree->Branch( "stop2_w1_eta", &m_stop2_w1_eta );
  tree->Branch( "stop2_w2_eta", &m_stop2_w2_eta );
  tree->Branch( "stop2_xi_eta", &m_stop2_xi_eta );
  tree->Branch( "stop2_phi", &m_stop2_phi );
  tree->Branch( "stop2_b_phi", &m_stop2_b_phi );
  tree->Branch( "stop2_w1_phi", &m_stop2_w1_phi );
  tree->Branch( "stop2_w2_phi", &m_stop2_w2_phi );
  tree->Branch( "stop2_xi_phi", &m_stop2_xi_phi );
  tree->Branch( "stop2_m", &m_stop2_m );
  tree->Branch( "stop2_b_m", &m_stop2_b_m );
  tree->Branch( "stop2_w1_m", &m_stop2_w1_m );
  tree->Branch( "stop2_w2_m", &m_stop2_w2_m );
  tree->Branch( "stop2_xi_m", &m_stop2_xi_m );
  tree->Branch( "TotISR_pt", &m_TotISR_pt );
  tree->Branch( "TotISR_eta", &m_TotISR_eta );
  tree->Branch( "TotISR_phi", &m_TotISR_phi );
  tree->Branch( "TotISR_m", &m_TotISR_m );
  tree->Branch( "TotISR_ptThrust", &m_TotISR_ptThrust );
  tree->Branch( "PTISR", &m_PTISR );
  tree->Branch( "RISR", &m_RISR );
  tree->Branch( "nISR", &m_nISR );
  tree->Branch( "MS", &m_MS );
  tree->Branch( "MV", &m_MV );
  tree->Branch( "MISR", &m_MISR );
  tree->Branch( "MET", &m_metTruth );
  tree->Branch( "stop1_pThrust", &m_stop1_pThrust );
  tree->Branch( "stop2_pThrust", &m_stop2_pThrust );
  tree->Branch( "distop_pt", &m_distop_pt );
  tree->Branch( "distop_eta", &m_distop_eta );
  tree->Branch( "distop_phi", &m_distop_phi );
  tree->Branch( "distop_m", &m_distop_m );
  tree->Branch( "distop_pThrust", &m_distop_pThrust );
  tree->Branch( "dixi_pt", &m_dixi_pt );
  tree->Branch( "dixi_eta", &m_dixi_eta );
  tree->Branch( "dixi_phi", &m_dixi_phi );
  tree->Branch( "dixi_m", &m_dixi_m );
  tree->Branch( "dixi_pThrust", &m_dixi_pThrust );
  tree->Branch( "dphiISRI", &m_dphiISRI );
  tree->Branch( "isTop1Had", &isTop1Had );
  tree->Branch( "isTop2Had", &isTop2Had );
  
  return EL::StatusCode::SUCCESS;
}

void TruthxAODAnalysis_ttbar :: ResetVariables() {
   m_stop1_pt = 0.0;
   m_stop1_b_pt = 0.0;
   m_stop1_w1_pt = 0.0;
   m_stop1_w2_pt = 0.0;
   m_stop1_xi_pt = 0.0;
   m_stop1_eta = 0.0;
   m_stop1_b_eta = 0.0;
   m_stop1_w1_eta = 0.0;
   m_stop1_w2_eta = 0.0;
   m_stop1_xi_eta = 0.0;
   m_stop1_phi = 0.0;
   m_stop1_b_phi = 0.0;
   m_stop1_w1_phi = 0.0;
   m_stop1_w2_phi = 0.0;
   m_stop1_xi_phi = 0.0;
   m_stop1_m = 0.0;
   m_stop1_b_m = 0.0;
   m_stop1_w1_m = 0.0;
   m_stop1_w2_m = 0.0;
   m_stop1_xi_m = 0.0;
   m_stop2_pt = 0.0;
   m_stop2_b_pt = 0.0;
   m_stop2_w1_pt = 0.0;
   m_stop2_w2_pt = 0.0;
   m_stop2_xi_pt = 0.0;
   m_stop2_eta = 0.0;
   m_stop2_b_eta = 0.0;
   m_stop2_w1_eta = 0.0;
   m_stop2_w2_eta = 0.0;
   m_stop2_xi_eta = 0.0;
   m_stop2_phi = 0.0;
   m_stop2_b_phi = 0.0;
   m_stop2_w1_phi = 0.0;
   m_stop2_w2_phi = 0.0;
   m_stop2_xi_phi = 0.0;
   m_stop2_m = 0.0;
   m_stop2_b_m = 0.0;
   m_stop2_w1_m = 0.0;
   m_stop2_w2_m = 0.0;
   m_stop2_xi_m = 0.0;
   m_TotISR_pt = 0.0;
   m_TotISR_eta = 0.0;
   m_TotISR_phi = 0.0;
   m_TotISR_m = 0.0;
   m_TotISR_ptThrust = 0.0;
   m_metTruth = 0.0;
   m_metPhiTruth = 0.0;
   m_PTISR = 0.0;
   m_RISR = 0.0;
   m_nISR = 0.0;
   m_MS = 0.0;
   m_MV = 0.0;
   m_MISR = 0.0;
   m_stop1_pThrust = 0.0;
   m_stop2_pThrust = 0.0;
   m_distop_pt = 0.0;
   m_distop_eta = 0.0;
   m_distop_phi  = 0.0;
   m_distop_m = 0.0;
   m_distop_pThrust = 0.0;
   m_dixi_pt = 0.0;
   m_dixi_eta = 0.0;
   m_dixi_phi = 0.0;
   m_dixi_m = 0.0;
   m_dixi_pThrust = 0.0;
   m_dphiISRI = 0.0;
   isTop1Had = -1;
   isTop2Had = -1;   
}

EL::StatusCode TruthxAODAnalysis_ttbar :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: initialize ()
{
  xAOD::TEvent* event = wk()->xaodEvent();
  evt_display_name = "event_display.eps";
  ps = new TPostScript(evt_display_name.c_str(), 112);

  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // RestFrames stuff
  m_truthJets = new vector<TLorentzVector>;
  m_truthLeptons = new vector<TLorentzVector>;
  m_truthJetIsBTagged = new vector<bool>;
  truthJets = 0;       

  ////////////// Tree set-up /////////////////
  LAB = new LabRecoFrame("LAB", "lab");
  CM = new DecayRecoFrame("CM", "CM");
  S = new DecayRecoFrame("S", "S");
  ISR = new VisibleRecoFrame("ISR", "ISR");
  V = new VisibleRecoFrame("V", "Vis");
  L = new VisibleRecoFrame("L","Lep");
  I = new InvisibleRecoFrame("I", "Inv");
 
  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  S->AddChildFrame(*L);
 
  LAB->InitializeTree();
  ////////////// Tree set-up /////////////////
 
  ////////////// Jigsaw rules set-up /////////////////
  INV = new InvisibleGroup("INV", "Invisible System");
  INV->AddFrame(*I);
 
  VIS = new CombinatoricGroup("VIS", "Visible Objects");
  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR, 1, false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V, 0, false);
  //VIS->AddFrame(*L);
  //VIS->SetNElementsForFrame(*L, 1, false);
 
  // set the invisible system mass to zero
  InvMass = new SetMassInvJigsaw("InvMass", "Invisible system mass Jigsaw");
  INV->AddJigsaw(*InvMass);
 
 
  // define the rule for partitioning objects between "ISR" and "V"
  SplitVis = new MinMassesCombJigsaw("SplitVis", "Minimize M_{ISR} and M_{S} Jigsaw");
  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I + L)
  SplitVis->AddFrame(*V, 1);
  SplitVis->AddFrame(*I, 1);
  SplitVis->AddFrame(*L, 1);
 
  LAB->InitializeAnalysis();
  ////////////// Jigsaw rules set-up /////////////////

  if (verbose) std::cout << "in Initialize, about to initialise variables" << std::endl;
  m_eventCounter = 0;

  count = 0;

  thrust_vec = TLorentzVector(0.,0.,0.,0.);
  thrust_perp_vec = TLorentzVector(0.,0.,0.,0.);
  
  stop1 = TLorentzVector(0.,0.,0.,0.);
  stop1_b = TLorentzVector(0.,0.,0.,0.);
  stop1_wj1 = TLorentzVector(0.,0.,0.,0.);
  stop1_wj2 = TLorentzVector(0.,0.,0.,0.);
  stop1_xi =  TLorentzVector(0.,0.,0.,0.);

  stop2 = TLorentzVector(0.,0.,0.,0.);
  stop2_b = TLorentzVector(0.,0.,0.,0.);
  stop2_wj1 = TLorentzVector(0.,0.,0.,0.);
  stop2_wj2 = TLorentzVector(0.,0.,0.,0.);
  stop2_xi = TLorentzVector(0.,0.,0.,0.);

  ISR_total = TLorentzVector(0.,0.,0.,0.);
  verbose = true;

  w_polemass = 80.35;
  b_polemass = 4.65;
  t_polemass = 173.34;

  t_reduced_mass= t_polemass*t_polemass- w_polemass*w_polemass- b_polemass*b_polemass;

  expected_b_momentum = sqrt( t_reduced_mass *t_reduced_mass / 4. / t_polemass / t_polemass -
                              b_polemass*b_polemass*w_polemass*w_polemass/t_polemass/t_polemass );

  GeV = 1000.;

  if (verbose) std::cout << "Finished intializing variables " << std::endl;
  return EL::StatusCode::SUCCESS;
}

void TruthxAODAnalysis_ttbar::setEvtDisplay(std::string eps) {
  evt_display_name = eps;
  return;
}

void TruthxAODAnalysis_ttbar::DrawEvtDisplay(){

  std::cout << "drawing display" << std::endl;

  TCanvas *c2 = new TCanvas("c2");
  c2->Range(0,0,1,1);

  TArrow *metArrow = new TArrow();
  TArrow *arrow = new TArrow();

  TLatex p;
  p.SetTextSize(0.04);
  p.SetTextFont(42);

  metArrow->SetLineColor(13);
  metArrow->SetLineWidth(3);
  metArrow->SetLineStyle(2);
  arrow->SetLineWidth(3);

  std::vector<float> jetpt_vec;
  jetpt_vec.resize(0);
  jetpt_vec.push_back(stop1_b.Pt());
  jetpt_vec.push_back(stop1_wj1.Pt());
  jetpt_vec.push_back(stop1_wj2.Pt());
  jetpt_vec.push_back(stop2_b.Pt());
  jetpt_vec.push_back(stop2_wj1.Pt());
  jetpt_vec.push_back(stop2_wj2.Pt());
  jetpt_vec.push_back(m_metTruth);
  for (int i=0;i<nISR;i++){
    jetpt_vec.push_back(ISR_p[i][3]);
  }
  std::sort (jetpt_vec.begin(), jetpt_vec.end());
  double maximum = 2.2*jetpt_vec.back();

  
  double length  = m_metTruth/maximum;
  metArrow->DrawArrow(0.62, 0.5, 0.7-length*cos(m_metPhiTruth), 0.5+length*sin(m_metPhiTruth),0.02,"|>");

  length = stop1_b.Pt()/maximum;
  double phi = stop1_b.Phi();
  arrow->SetLineColor(kBlue);
  arrow->SetLineStyle(1);
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  p.DrawLatex(0.1,0.70,Form("#color[4]{b-jet p_{T} = %3.0f GeV}",stop1_b.Pt()));

  length = stop2_b.Pt()/maximum;
  phi =stop2_b.Phi();
  arrow->SetLineColor(kBlue);
  arrow->SetLineStyle(1);
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  p.DrawLatex(0.1,0.64,Form("#color[4]{b-jet p_{T} = %3.0f GeV}",stop2_b.Pt()));

  length = stop1_wj1.Pt()/maximum;
  phi =stop1_wj1.Phi();
  if( isTop1Had==1 ) {
    arrow->SetLineColor(kMagenta);
    p.DrawLatex(0.1,0.58,Form("#color[6]{lepton p_{T} = %3.0f GeV}",stop1_wj1.Pt()));
  }
  else {
    arrow->SetLineColor(kOrange);
    p.DrawLatex(0.1,0.46,Form("#color[800]{Wjet p_{T} = %3.0f GeV}",stop1_wj1.Pt()));
  }
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  length = stop1_wj2.Pt()/maximum;
  phi =stop1_wj2.Phi();
  if( isTop1Had==1 ) {
    arrow->SetLineStyle(2);
    p.DrawLatex(0.1,0.52,Form("#color[6]{neutrino p_{T} = %3.0f GeV}",stop1_wj2.Pt()));
  }
  else {
    arrow->SetLineStyle(1);
    p.DrawLatex(0.1,0.40,Form("#color[800]{Wjet p_{T} = %3.0f GeV}",stop1_wj2.Pt()));
  }
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");

  length = stop2_wj1.Pt()/maximum;
  phi =stop2_wj1.Phi();
  if( isTop2Had==1 ) {
    arrow->SetLineColor(kMagenta);
    p.DrawLatex(0.1,0.46,Form("#color[6]{lepton p_{T} = %3.0f GeV}",stop2_wj1.Pt()));
  }
  else {
    arrow->SetLineColor(kOrange);
    p.DrawLatex(0.1,0.46,Form("#color[800]{W-jet p_{T} = %3.0f GeV}",stop2_wj1.Pt()));
  }
  arrow->SetLineStyle(1);
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  length = stop2_wj2.Pt()/maximum;
  phi =stop2_wj2.Phi();
  if( isTop2Had==1 ) {
    arrow->SetLineStyle(2);
    p.DrawLatex(0.1,0.40,Form("#color[6]{neutrino p_{T} = %3.0f GeV}",stop2_wj2.Pt()));
  }
  else {
    arrow->SetLineStyle(1);
    p.DrawLatex(0.1,0.40,Form("#color[800]{W-jet p_{T} = %3.0f GeV}",stop2_wj2.Pt()));
  }
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  
  double place = 0.14;
  if(verbose) std::cout << "nISR: " << nISR << std::endl;
  for (int i=0; i<nISR; i ++){
    arrow->SetLineColor(kRed);
    arrow->SetLineStyle(1);
    arrow->DrawArrow(0.62, 0.5, 0.62-ISR_p[i][0]/maximum, 0.5+ISR_p[i][1]/maximum,0.02,"|>");
    p.DrawLatex(0.1,place,Form("#color[2]{ISR jet_{%1i} p_{T} = %3.0f GeV}",i+1 , ISR_p[i][3]));
    place -= 0.06;
  }

  p.DrawLatex(0.1,0.97,Form("#color[1]{R_{ISR}=%3.2f, p_{T,ISR}=%3.0f GeV, M_{T,S}= %3.0f GeV}",m_RISR,m_PTISR,m_MS) );
  p.DrawLatex(0.1,0.90,Form("#color[13]{MET = %3.0f GeV, NjV = %3.0i, NjISR = %3.0i, nISR = %3.0f}",m_metTruth, m_NjV,m_NjISR,m_nISR) );
  p.DrawLatex(0.1,0.3,Form("#color[1]{Event Number = %6.0i}",eventNumber) );
  p.DrawLatex(0.1,0.20,Form("#color[2]{Tot. ISR p_{T}  = %3.0f GeV}",ISR_total.Pt()) );

  c2->Update();  ps->NewPage();

  delete c2;
  delete arrow;
  delete metArrow;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: execute ()
{
  if ( verbose ) std::cout << std::endl;
  if ( verbose ) std::cout << "new evt" << std::endl;

  // reset variables saved in output tree
  ResetVariables();

  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent* event = wk()->xaodEvent();

  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));  

  std::cout << "Event number: " << eventInfo->mcEventNumber() << std::endl; 
  if(eventInfo->mcEventNumber() == 175964) std::cout << "Found the event Julia!!" << std::endl;

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }   

  //--------------------------------------------------------//
  //     Look in the event's truth container                //
  //--------------------------------------------------------//

  //  Full Truth information is not available.
  //  Check here for basic information
  //
  //  https://indico.cern.ch/event/307549/session/7/contribution/31/material/slides/0.pdf

  //-------------------------------------------------------//

  //  Load the list of truth particles in the event
  //  Note not all particles in the truth has been saved:  Only the main decay tree have been saved
  //  If your code dies because you can't find a particle->barcode() for example
  //  it might mean that you are trying to access a particle that was not saved.
  
  const xAOD::TruthParticleContainer* TruthParticleCont = 0;
  EL_RETURN_CHECK("execute()",event->retrieve(TruthParticleCont, "TruthParticles"));

  //--------------------------------------------------------------//
  //             Loop over all saved truth particles              //
  //--------------------------------------------------------------//

  xAOD::TruthParticleContainer::const_iterator ipart_itr = TruthParticleCont->begin();
  xAOD::TruthParticleContainer::const_iterator ipart_end = TruthParticleCont->end();

  if (verbose) std::cout << "in execute, about to set lorentz vectors to 0 " << std::endl;
  stop1.SetPxPyPzE(0.,0.,0.,0.);
  stop1_b.SetPxPyPzE(0.,0.,0.,0.);
  stop1_wj1.SetPxPyPzE(0.,0.,0.,0.);
  stop1_wj2.SetPxPyPzE(0.,0.,0.,0.);
  stop1_xi.SetPxPyPzE(0.,0.,0.,0.);

  stop2.SetPxPyPzE(0.,0.,0.,0.);
  stop2_b.SetPxPyPzE(0.,0.,0.,0.);
  stop2_wj1.SetPxPyPzE(0.,0.,0.,0.);
  stop2_wj2.SetPxPyPzE(0.,0.,0.,0.);
  stop2_xi.SetPxPyPzE(0.,0.,0.,0.);
  dixi.SetPxPyPzE(0.,0.,0.,0.);

  nISR = 0;
  ISR_p.resize(0);

  thrust_vec.SetXYZT(0,0,0,0);
  thrust_perp_vec.SetXYZT(0,0,0,0);

  isTop1Had = -1;
  isTop2Had = -1;   //both tops should decay leptonically to emulate LEPLEP decay mode 
  plotEvent = false;
  
  //--------------------------------------------------------//
  //           Start to loop over truth here                //
  //--------------------------------------------------------//

  for( ; ipart_itr != ipart_end; ++ipart_itr ) {

    //if (verbose) std::cout << "inside particle loop" << std::endl;
    //    Get the particle from the particle container
    const xAOD::TruthParticle* particle = (*ipart_itr);

    //--------------------------------------------------------------------------------------------//
    //  access all the information from the particle
    //  what information can be accessed is documented here
    //  http://hep.uchicago.edu/~kkrizka/rootcoreapis/dd/dc2/classxAOD_1_1TruthParticle__v1.html
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    // This is the unique barcode of the particle.
    // if two particles have with the same barcode then they
    // are the same particle with same pt, pz, m, etc...
    //int barcode = particle->barcode(); 
    //  The PDG "ID" is the physical type of particle
    //  an id of 6 is a top quark, -6 is a anti-top
    //  an id of 11 is an electron, -11 is an positron
    //  a table of pdg ID can be found here
    //  http://pdg.lbl.gov/2005/reviews/montecarlorpp.pdf
    //--------------------------------------------------------------------------------------------//

    int pdgId = particle->pdgId();

    //--------------------------------------------------------------------------------------------//
    //  status of the particle is the stage of the particle in the decay chain
    //  Unfortunately depending on the MC generator, different status code means different things
    //  http://lcgapp.cern.ch/project/simu/HepMC/205/status.html
    //  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookGenParticleCandidate
    //  a status code of 1 is a final state particle.  It will not decay anymore
    //  a status code of 2 is an intermediate particle. It will decay further
    //  a status code of 3 identifies the "hard part" of the interaction, 
    //  i.e. the partons that are used in the matrix element calculation, including immediate decays of resonances.
    //  Sometimes a mid decay particle is more interesting than a final state particle.
    //  For quarks/gluon, you are not interested in all the final quarks.  You want the initial
    //  quark/gluon with status code 2 that makes all the other quark/gluons in the chain
    //  The original unstable quark/gluon will have the energy of the full jet
    //  ignore particles with all other status codes 
    //  they dont mean anything they are internal book-keeping for 
    //  the MC generation to balance out 4 momenta.  They aren't "real" or part of the simulation
    //--------------------------------------------------------------------------------------------//

    int status = particle->status();

    //--------------------------------------------------------------------------------------------//    
    //  Four momentum values of the particle
    //  Everthing is in MeV so you'll have to convert to GeV manually
    //--------------------------------------------------------------------------------------------//

    //    float px = particle->px();
    //    float py = particle->py();
    //    float pz = particle->pz();
    //    float pt = particle->pt();

    //--------------------------------------------------------------------------------------------//
    //         Charge of the particle
    //--------------------------------------------------------------------------------------------//

    //    float e = particle->e();

    //--------------------------------------------------------------------------------------------//
    //          Mass of the particle
    //--------------------------------------------------------------------------------------------//

    //    float m = particle->m();

    //  if debugging output every particle
    // if ( verbose ) std::if (verbose) std::cout << "pdgId " << pdgId << " status " << status << " pt " << pt << " pz " << pz << std::std::endl;

    //---------------------------------------------------------//
    //           Look for all the s-top/anti stops             //
    //---------------------------------------------------------//

    //  for s-top decay, the pdgId == 100006 is the stop.  
    //  In MC, stops and any paricle can be passed along a chain to tweak its momentum slightly
    //  So the decay chain can be stop -> stop -> stop -> top+neutralino
    //  each stop in the chain will have a different status
    //  the initial pp -> stop stop + ISR is status code 22
    //  the final stop -> b W + neutralino is at status code 62

    //std::vector <std::vector<float>> ISR_p;
    if(abs(pdgId) == 6) {if (verbose) std::cout << "The id of this particle is " << pdgId << std::endl;
    if (verbose) std::cout << "The status of this particle is " << status << std::endl;}

    if (abs(pdgId) == 6) {// && status == 22 
      if (  pdgId ==  6)   isTop1Had = Find_TopDecayProducts( particle,&stop1_b, &stop1_wj1, &stop1_wj2 ); 
      if (  pdgId == -6)   isTop2Had = Find_TopDecayProducts( particle,&stop2_b, &stop2_wj1, &stop2_wj2 );
    }
  } // end of first loop over particles

  // found some issues... skipping event
  if( isTop1Had < -1 || isTop2Had < -1 ) { return EL::StatusCode::SUCCESS; } 

  //-------------------------------------------------------//                                                                                                                                                                                               
  //    Find the initial pp -> stop + stop + ISR decay     //                                                                                                                                                                                               
  //-------------------------------------------------------//     
  ipart_itr = TruthParticleCont->begin();
  for( ; ipart_itr != ipart_end; ++ipart_itr ) {
    //if (verbose) std::cout << "inside particle loop" << std::endl;
    const xAOD::TruthParticle* particle = (*ipart_itr);
    int pdgId = particle->pdgId();
    int status = particle->status();

    //  The first stop has status 22 - only considering one stop to avoid double-counting
    if ( pdgId == 6 && status == 3) {

      if (verbose) std::cout << "inside top + status" << std::endl;
      //std::cout << "--------> inside stop + status" << std::endl;
      
      //---------------------------------------------------------//
      //      Look at pp -> Stop+AntiStop+ISR Decay Vertex       //
      //---------------------------------------------------------//

      bool hasProdVtx = particle->hasProdVtx();
      if (hasProdVtx){

	const xAOD::TruthVertex* prodVtx = particle->prodVtx();
	int npart = prodVtx->nOutgoingParticles();

	if (verbose) std::cout << "npart for prodvtx is  " << npart << std::endl;
	//---------------------------------------------------------------------//
	//     Loop over all products of the pp -> Stop + Stop + ISR Vertex    //
	//---------------------------------------------------------------------//

	//std::cout << "=====ENTERING Stop + Stop + ISR Vertex LOOP; npart for prodvtx is  " << npart << std::endl;
	for (int j=0; j<npart; j++){
	  const xAOD::TruthParticle* particle = prodVtx->outgoingParticle(j);
	  if( !particle ) {
	    //std::cout << " Missing particle link for particle " << j << " of " << npart << std::endl;
	    return EL::StatusCode::SUCCESS;
	  } else {
	  pdgId = particle->pdgId();
	  status = particle->status();

	  if ( verbose ) std::cout << "The stop decay vertex, id of particle " << j << " of stop1 prodvtx is " << pdgId << std::endl;
	  //std::cout << "The stop decay vertex, id of particle " << j << " of stop1 prodvtx is " << pdgId << std::endl;

	  //-------------------------------------------//
	  //      Find Decay Products of STop          //
	  //-------------------------------------------//
	  if (pdgId ==  6) {
	    if (verbose) std::cout << "inside top1 production vertex loop..... " << std::endl;
	    if (verbose) std::cout << "The id of particle " << j << " of top1 prodvtx is " << pdgId << std::endl;
	    if (verbose) std::cout << "The status of particle "<< j << " of top1 prodvtx " << status << std::endl;
	  }
          //-------------------------------------------//
          //      Find Decay Products of AntiSTop      //
          //-------------------------------------------//
	  else if (pdgId == -6){
	    if (verbose) std::cout << "inside top2 production vertex loop..... " << std::endl;
	    if (verbose) std::cout << "The id of particle " << j << " of stop2 prodvtx is " << pdgId << std::endl;
	    if (verbose) std::cout << "The status of particle "<< j << " of stop2 prodvtx " << status << std::endl;
	  }
          //-------------------------------------------//
          //      Find Decay Products of ISR           //
          //-------------------------------------------//
	  else{
	    nISR++;
	    //std::cout << "pdgId " << pdgId << " status " << status << " pt " << particle->pt() << " pz " << particle->pz() << " nISR " << nISR << " barcode " << particle->barcode() << std::endl;

	    float temp_px=0.;
	    float temp_py=0.;
	    float temp_pz=0.;
	    float temp_pt=0.;
	    float temp_e=0.;
	    std::vector<float> p;
	    p.resize(0);

	    temp_px = particle->px()/GeV;
	    temp_py = particle->py()/GeV;
	    temp_pz = particle->pz()/GeV;
	    temp_pt = particle->pt()/GeV;
	    temp_e = particle->e()/GeV;

	    p.push_back(temp_px);
	    p.push_back(temp_py);
	    p.push_back(temp_pz);
	    p.push_back(temp_pt);
	    p.push_back(temp_e);
	    ISR_p.push_back(p);

	    if ( verbose ) std::cout << "ISR E " << temp_e << "ISR pt " << sqrt(temp_px*temp_px + temp_py*temp_py) << std::endl;
	  }
	}
      }
      }//S
      //------------------------------------------------------//
      //      break out of the loop if we found the stop      //
      //------------------------------------------------------//
      //      break;
    }


    //Currently, only draw histograms if both W decays are hadronic
  } // finished loop over all truth particles

  //-------------------------------------------------//
  //           if not semileptonic give up           //
  //-------------------------------------------------//

  if ( verbose ) std::cout << isTop1Had << " " << isTop2Had << std::endl;
  //if( isTop1Had < -1 || isTop2Had < -1 ) { return EL::StatusCode::SUCCESS; }
  //if( !((isTop1Had==1 && isTop2Had!=1)||(isTop1Had!=1 && isTop2Had==1)) ) { return EL::StatusCode::SUCCESS; }

  //-------------------------------------------------//
  //             Find Direction of Thrust            //
  //-------------------------------------------------//

  stop1_t=stop1_b+stop1_wj1+stop1_wj2;
  stop2_t=stop2_b+stop2_wj1+stop2_wj2;

  ditop = stop1_t + stop2_t;

  float temp_px = 0.;
  float temp_py = 0.;
  float temp_pz = 0.;
  float temp_pt = 0.;
  float temp_e = 0.;
  m_nISR = nISR; 
  std::cout << "nISR: " << nISR << std::endl;
  for (int i=0; i<nISR; i ++){
    temp_px += ISR_p[i][0];
    temp_py += ISR_p[i][1];
    temp_pz += ISR_p[i][2];
    temp_pt += ISR_p[i][3];
    temp_e += ISR_p[i][4];
  }
  ISR_total.SetPxPyPzE(temp_px,temp_py,temp_pz,temp_e);

  TLorentzVector total = ditop+ISR_total;

  dixi = stop1_xi + stop2_xi;

  //  std::cout << (stop1_t+stop1_xi).M() << " " << stop1_xi.M() << " " 
  //	    << (stop2_t+stop2_xi).M() << " " << stop2_xi.M() << std::endl;

  MET_TLV = dixi;
  if ( !(isTop1Had==0) ) MET_TLV+=stop1_wj2;
  if ( !(isTop2Had==0) ) MET_TLV+=stop2_wj2;

  //  if ( dixi.Pt() < 250. ) { return EL::StatusCode::SUCCESS; }

  if ( verbose ) std::cout << "di top P " << ditop.P() << " M " << ditop.M() << std::endl;
  if ( verbose ) std::cout << " +ISR P " <<  total.P() << " M " << total.M() << std::endl;

  //  if ( !(isTop1Had==0) || !(isTop2Had==0) ) { return EL::StatusCode::SUCCESS; }     

  FindThrustDir();

  //------------------------------------------------------------//
  //         Order STops by B Quark P Along Thrust              //
  //------------------------------------------------------------//

  double b1_pthrust = CalculatePAlongThrust( &stop1_b );
  double b2_pthrust = CalculatePAlongThrust( &stop2_b );
  

  //-----------------------------------------------------//
  //           Construct Combined Objects                //
  //-----------------------------------------------------//

  if ( verbose ) std::cout << "stop decay " << isTop1Had << " " << isTop2Had << std::endl;

  //if ( !(isTop1Had==0) || !(isTop2Had==0) ) { return EL::StatusCode::SUCCESS; }
  //  if ( (isTop1Had==0) && (isTop2Had==0) ) { return EL::StatusCode::SUCCESS; }
  //  if ( (isTop1Had!=0) && (isTop2Had!=0) ) { return EL::StatusCode::SUCCESS; }
  //  if ( !((isTop1Had!=0) && (isTop2Had!=0)) ) { return EL::StatusCode::SUCCESS; }

  //-----------------------------------------------------//

  stop1 = stop1_b + stop1_wj1 + stop1_wj2 + stop1_xi;
  stop2 = stop2_b + stop2_wj1 + stop2_wj2 + stop2_xi;

  distop = stop1 + stop2;
  dixi = stop1_xi + stop2_xi;
  stop1_jets = stop1_b + stop1_wj1 + stop1_wj2;
  stop2_jets = stop2_b + stop2_wj1 + stop2_wj2;
  stop1_w = stop1_wj1 + stop1_wj2;
  stop2_w = stop2_wj1 + stop2_wj2;

  //-----------------------------------------------------//  
  // -------------------------------------
  // Fill output variables
  // -------------------------------------  
  m_stop1_pt = stop1.Pt();
  m_stop1_b_pt = stop1_b.Pt();
  m_stop1_w1_pt = stop1_wj1.Pt();
  m_stop1_w2_pt = stop1_wj2.Pt();
  m_stop1_xi_pt = stop1_xi.Pt();
  m_stop1_eta = stop1.Eta();
  m_stop1_b_eta = stop1_b.Eta();
  m_stop1_w1_eta = stop1_wj1.Eta();
  m_stop1_w2_eta = stop1_wj2.Eta();
  m_stop1_xi_eta = stop1_xi.Eta();
  m_stop1_phi = stop1.Phi();
  m_stop1_b_phi = stop1_b.Phi();
  m_stop1_w1_phi = stop1_wj1.Phi();
  m_stop1_w2_phi = stop1_wj2.Phi();
  m_stop1_xi_phi = stop1_xi.Phi();
  m_stop1_m = stop1.M();
  m_stop1_b_m = stop1_b.M();
  m_stop1_w1_m = stop1_wj1.M();
  m_stop1_w2_m = stop1_wj2.M();
  m_stop1_xi_m = stop1_xi.M();

  m_stop2_pt = stop2.Pt();
  m_stop2_b_pt = stop2_b.Pt();
  m_stop2_w1_pt = stop2_wj1.Pt();
  m_stop2_w2_pt = stop2_wj2.Pt();
  m_stop2_xi_pt = stop2_xi.Pt();
  m_stop2_eta = stop2.Eta();
  m_stop2_b_eta = stop2_b.Eta();
  m_stop2_w1_eta = stop2_wj1.Eta();
  m_stop2_w2_eta = stop2_wj2.Eta();
  m_stop2_xi_eta = stop2_xi.Eta();
  m_stop2_phi = stop2.Phi();
  m_stop2_b_phi = stop2_b.Phi();
  m_stop2_w1_phi = stop2_wj1.Phi();
  m_stop2_w2_phi = stop2_wj2.Phi();
  m_stop2_xi_phi = stop2_xi.Phi();
  m_stop2_m = stop2.M();
  m_stop2_b_m = stop2_b.M();
  m_stop2_w1_m = stop2_wj1.M();
  m_stop2_w2_m = stop2_wj2.M();
  m_stop2_xi_m = stop2_xi.M();
  m_stop1_pThrust = CalculatePAlongThrust( &stop1 );
  m_stop2_pThrust = CalculatePAlongThrust( &stop2 );

  m_distop_pt = distop.Pt();
  m_distop_eta = distop.Eta(); 
  m_distop_phi = distop.Phi();
  m_distop_m = distop.M();
  m_distop_pThrust = CalculatePAlongThrust( &distop );

  m_dixi_pt = dixi.Pt();
  m_dixi_eta = dixi.Eta();
  m_dixi_phi = dixi.Phi();
  m_dixi_m = dixi.M();
  m_dixi_pThrust = CalculatePAlongThrust( &dixi );

  m_TotISR_pt = ISR_total.Pt();
  m_TotISR_eta = ISR_total.Eta();
  m_TotISR_phi = ISR_total.Phi();
  m_TotISR_m = ISR_total.M();
  m_TotISR_ptThrust = CalculatePAlongThrust( &ISR_total );

  eventNumber = eventInfo->mcEventNumber(); 

  std::cout << "Event number : Julia;" << eventNumber << std::endl;
  //-------------------------------------------------//

  //return EL::StatusCode::SUCCESS; // cut off  

  std::stringstream ssi;
  ssi << eventInfo->runNumber();
  std::string istr = ssi.str();


  //----------------------------------------//
  //  vector<TLorentzVector> *m_truthJets;
  //  vector<bool> *m_truthJetIsBTagged;
  m_truthJets->resize(0);
  m_truthJetIsBTagged->resize(0);

  //  const xAOD::JetContainer* truthJets = 0;
  truthJets = 0;
  if (event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
    for (unsigned int jetI=0; jetI<truthJets->size(); ++jetI) {
      const xAOD::Jet* jet = (truthJets->at(jetI));
      if (jet->pt()/1000. < 20) // Modify this and make it 35 GeV
	continue;
      if ( fabs(jet->eta()) > 4.5 )
	continue;
      if ( fabs(jet->eta()) > 2.8 )
	continue;

      m_truthJets->push_back(TLorentzVector(jet->px()/1000.,jet->py()/1000.,
					    jet->pz()/1000.,jet->e()/1000.));

      //      int jetFlav = xAOD::jetFlavourLabel(jet, xAOD::GAFinalHadron);

      int flavour = -1;
      jet->getAttribute("ConeTruthLabelID",flavour);   
      m_truthJetIsBTagged->push_back(flavour==5);
    }
  }


  //Julia fix this! 
  m_truthLeptons->resize(0);
  if(!isTop1Had) m_truthLeptons->push_back(stop1_wj1);
  if(!isTop2Had) m_truthLeptons->push_back(stop2_wj1);

  if ( isTop1Had>0 ) {
    if (stop1_wj1.Pt() > 20. && fabs(stop1_wj1.Eta()) < 2.5 ){
      m_truthJets->push_back(stop1_wj1);
      m_truthJetIsBTagged->push_back(false);
    }
  }

  if ( isTop2Had>0 ) {
    if (stop2_wj1.Pt() > 20. && fabs(stop2_wj1.Eta()) < 2.5 ){
      m_truthJets->push_back(stop2_wj1);
      m_truthJetIsBTagged->push_back(false);
    }
  }

  //--------------------------//

  double m_metXTruth=0.0;
  double m_metYTruth=0.0;
  m_metPhiTruth=0.0;
  double m_metSumEtTruth=0.0;
  m_metTruth=0.0;

  truthMetContainer = 0;
  if (event->retrieve(truthMetContainer, "MET_Truth").isSuccess()) {
    xAOD::MissingETContainer::const_iterator it = truthMetContainer->find("NonInt");
    if (it == truthMetContainer->end()) {
      std::cout << "not truthmet" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
    m_metXTruth        = (*it)->mpx()/1000.;
    m_metYTruth        = (*it)->mpy()/1000.;
    m_metPhiTruth   = (*it)->phi();
    m_metSumEtTruth = (*it)->sumet()/1000.;
    m_metTruth = TMath::Sqrt(m_metXTruth*m_metXTruth + m_metYTruth*m_metYTruth);
  }

  //--------------------------//

  // analyze event in RestFrames tree
  LAB->ClearEvent();
  vector<RFKey> jetID;
  std::cout << "truthJets size: " << m_truthJets->size() << std::endl;
  for (UInt_t i = 0; i < m_truthJets->size(); i++)
    {
      TLorentzVector jet;
      jet.SetPtEtaPhiM(m_truthJets->at(i).Pt(), 0.0, m_truthJets->at(i).Phi(), m_truthJets->at(i).M());
      jetID.push_back(VIS->AddLabFrameFourVector(jet));
    }

  TLorentzVector lepSys;
  vector<RFKey> lepID;
  for(UInt_t i = 0; i < m_truthLeptons->size(); i++){
          TLorentzVector lep1;
          lep1.SetPtEtaPhiM(m_truthLeptons->at(i).Pt(), 0.0, m_truthLeptons->at(i).Phi(), m_truthLeptons->at(i).M());
          lepID.push_back(VIS->AddLabFrameFourVector(lep1));
          lepSys = lepSys + lep1;
  }

  L->SetLabFrameFourVector(lepSys);

  TVector3 met(m_metXTruth,m_metYTruth, 0.0);
  INV->SetLabFrameThreeVector(met);
  if (!LAB->AnalyzeEvent()) cout << "Something went wrong..." << endl;
 
  // Compressed variables from tree
  m_NjV = 0;
  m_NbV = 0;
  m_NjISR = 0;
  m_NbISR = 0;
  m_pTjV1 = 0.;
  m_pTjV2 = 0.;
  m_pTjV3 = 0.;
  m_pTjV4 = 0.;
  m_pTjV5 = 0.;
  m_pTjV6 = 0.;
  m_pTbV1 = 0.;
  m_pTbV2 = 0.;

  double Ht = 0;
  double jet1_pt = -999;
  double jet2_pt = -999;
  double jet3_pt = -999;
  double jet4_pt = -999;
  double jet5_pt = -999;
  double jet6_pt = -999;

  for (uint i = 0; i < m_truthJets->size(); i++) {
    std::cout << "Truth jet #" << i << std::endl;
    TLorentzVector *jet = &(m_truthJets->at(i));

    Ht += jet->Pt();
    
    if ( i == 0 ) jet1_pt = jet->Pt();
    if ( i == 1 ) jet2_pt = jet->Pt();
    if ( i == 2 ) jet3_pt = jet->Pt();
    if ( i == 3 ) jet4_pt = jet->Pt();
    if ( i == 4 ) jet5_pt = jet->Pt();
    if ( i == 5 ) jet6_pt = jet->Pt();

    if (VIS->GetFrame(jetID[i]) == *V) { // sparticle group
      std::cout << "Is a V jet!" << std::endl;
      m_NjV++;
      if (m_NjV == 1)
	m_pTjV1 = jet->Pt();
      if (m_NjV == 2)
	m_pTjV2 = jet->Pt();
      if (m_NjV == 3)
	m_pTjV3 = jet->Pt();
      if (m_NjV == 4)
	m_pTjV4 = jet->Pt();
      if (m_NjV == 5)
	m_pTjV5 = jet->Pt();
      if (m_NjV == 6)
	m_pTjV6 = jet->Pt();
      if (m_truthJetIsBTagged->at(i)) {
      std::cout << "Is a V B jet!" << std::endl;
	m_NbV++;
	if (m_NbV == 1)
	  m_pTbV1 = jet->Pt();
	if (m_NbV == 2)
	  m_pTbV2 = jet->Pt();
      }
    }
    else {
      std::cout << "Is a ISR jet!" << std::endl;
      m_NjISR++;
      if (m_truthJetIsBTagged->at(i))
        std::cout << "Is a ISR B jet!" << std::endl;
	m_NbISR++;
    }
  }
 
  // need at least one jet associated with sparticle-side of event
  //if (m_NjV < 1) {
    m_PTISR = 0.;
    m_RISR = 0.;
    m_cosS = 0.;
    m_MS = 0.;
    m_MV = 0.;
    m_MISR = 0.;
    m_dphiCMI = 0.;
    m_dphiISRI = 0.;
  //}
  //else {
 
    TVector3 vP_ISR = ISR->GetFourVector(*CM).Vect();
    TVector3 vP_I   = I->GetFourVector(*CM).Vect();
 
    m_PTISR = vP_ISR.Mag();
    m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
    m_cosS = S->GetCosDecayAngle();
    m_MS = S->GetMass();
    m_MV = V->GetMass();
    m_MISR = ISR->GetMass();
    m_dphiCMI = acos(-1.)-fabs(CM->GetDeltaPhiBoostVisible());
    m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));
  //}

  //----------------------------------------//
   if(verbose) {std::cout << "RISR: " << m_RISR << ", NjV: " << m_NjV << ", NjISR: " << m_NjISR << ", Nl: " << m_truthLeptons->size() << std::endl;
        std::cout << "Truth jets: " << std::endl;
  	for(int k =0; k<m_truthJets->size(); k++){
                std::cout << " jet pt: " << m_truthJets->at(k).Pt() << " eta: " << m_truthJets->at(k).Eta() << ", phi: " << m_truthJets->at(k).Phi() << std::endl;}
        std::cout << "Truth leptons: " << std::endl;
        for(int j =0; j<m_truthLeptons->size(); j++){
                std::cout << " lepton pt: " << m_truthLeptons->at(j).Pt();}
        std::cout << "Truth MET: " << m_metTruth << std::endl;
        std::cout << "dixi pt; " << m_dixi_pt << ", total ISR pt: " << m_TotISR_pt << ", distop? " << m_distop_pt << endl;
        std::cout << "Things that go into RISR: PTI " << vP_I.Mag() << ", dot product numerator: " << fabs(vP_I.Dot(vP_ISR.Unit())) << ", PTISR: " << m_PTISR << std::endl;
   }
  //----------------------------------------//

  // Draw event displays 
  //if( m_eventCounter < 10000 && m_metTruth>200 )  
  if(eventInfo->mcEventNumber() == 175964 || eventInfo->mcEventNumber() == 172412 || eventInfo->mcEventNumber() == 174910 || eventInfo->mcEventNumber() == 175964 || eventInfo->mcEventNumber() == 175357 || eventInfo->mcEventNumber() == 168006 || eventInfo->mcEventNumber() == 167459 || eventInfo->mcEventNumber() == 175313 || eventInfo->mcEventNumber() == 169520 || eventInfo->mcEventNumber() == 167071 || eventInfo->mcEventNumber() == 169883 || eventInfo->mcEventNumber() == 173839 || eventInfo->mcEventNumber() == 162701 || eventInfo->mcEventNumber() == 172664 //here is where we break
  || eventNumber== 161740 || eventNumber== 174015 || eventNumber== 172412 || eventNumber== 169022 ||eventNumber== 175964 || eventNumber== 161372 || eventNumber== 167071 || eventNumber== 169883) {DrawEvtDisplay(); 

//EventNumber : 161740, DecayModeTTbar: 4
//EventNumber : 174015, DecayModeTTbar: 4
//EventNumber : 172412, DecayModeTTbar: 1
//EventNumber : 169022, DecayModeTTbar: 5
//EventNumber : 175964, DecayModeTTbar: 2
//EventNumber : 161372, DecayModeTTbar: 2
//EventNumber : 167071, DecayModeTTbar: 1
//EventNumber : 169883, DecayModeTTbar: 2

//EventNumber : 172412, DecayModeTTbar: 1
//EventNumber : 174910, DecayModeTTbar: 1
//EventNumber : 175964, DecayModeTTbar: 2
//EventNumber : 175357, DecayModeTTbar: 2
//EventNumber : 168006, DecayModeTTbar: 1
//EventNumber : 167459, DecayModeTTbar: 2
//EventNumber : 175313, DecayModeTTbar: 2
//EventNumber : 169520, DecayModeTTbar: 1
//EventNumber : 167071, DecayModeTTbar: 1
//EventNumber : 169883, DecayModeTTbar: 2
//EventNumber : 173839, DecayModeTTbar: 1
//EventNumber : 162701, DecayModeTTbar: 4
//EventNumber : 172664, DecayModeTTbar: 1
  // fill the output tree                                                                                                                                                          
  tree->Fill();}

  return EL::StatusCode::SUCCESS; // cut off for now
}


double TruthxAODAnalysis_ttbar::CalculatePAlongThrust( TLorentzVector *Vec ) {
  return Vec->Px()*thrust_vec.Px() + Vec->Py()*thrust_vec.Py();
}
double TruthxAODAnalysis_ttbar::CalculatePAlongThrust( double px, double py ) {
  return px*thrust_vec.Px() + py*thrust_vec.Py();
}

//int TruthxAODAnalysis_ttbar::Find_STopDecayProducts( const xAOD::TruthParticle* particle, 
//						     TLorentzVector *Top_tmp,
//						     TLorentzVector *Xi_tmp,  TLorentzVector *B_tmp,
//						     TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp ) {
//
//  int isSTopHad = -1;
//
//  if (verbose) std::cout << "inside top1 loop (prod)" << std::endl;
//
//  //  const xAOD::TruthVertex* decayVtx = particle->decayVtx();
//  const xAOD::TruthParticle* particle_daughter;
//  //  const xAOD::TruthVertex* decayVtx_daughter = decayVtx;
//
//  int npart_daughter = particle->nChildren(); //decayVtx_daughter->nOutgoingParticles();
//
//
//  int pdgId, status;
//
//  //------------------------------------------------------------//
//  //       Look at decay vertex see what the stop decays to     //
//  //------------------------------------------------------------//
//
//  //---------------------------------------------------------------------------------------//
//  //  The stop can decay into another stop in a stop->stop->stop chain
//  //  Make sure you reach the end of the chain and the decay product is stop -> W b + Xi
//  //  Checking to see if it has more than one decay product (i.e. not a chain of astops).
//  //  Doing this recursively until we get astop->W b + xi    
//  //---------------------------------------------------------------------------------------//
//
//  if (verbose) std::cout << "npart daughter before while is : " << npart_daughter << std::endl;
//
//  bool foundSTop = false;
//
//  while (!foundSTop){
//    //particle_daughter = particle->child(0); //decayVtx_daughter->outgoingParticle(0);
//    //decayVtx = decayVtx_daughter;
//    //decayVtx_daughter = particle_daughter->decayVtx();
//    npart_daughter = particle->nChildren();//decayVtx_daughter->nOutgoingParticles();
//
//    if ( npart_daughter == 1 ) {
//      foundSTop = false;
//      particle_daughter = particle->child(0);
//      particle = particle_daughter;
//    }
//    else  {
//      foundSTop = true;
//
//      /*
//      for ( int i=0; i<npart_daughter; i++) {
//	particle_daughter = particle->child(i);
//	int pdgId = particle_daughter->pdgId();//decayVtx_daughter->outgoingParticle(i)->pdgId();
//	if ( abs(pdgId)==1000006 ) {
//	  foundSTop = false;
//	  particle = particle_daughter;
//	}
//      }
//      */
//    }
//
//    //    particle = particle_daughter;
//  }
//
//  status = particle->status();
//  if (verbose) std::cout << "npart daughter after while is : " << npart_daughter << std::endl;
//  if (verbose) std::cout << "particle's status is: " << status << std::endl;
//
//  //----------------------------------------------------------------//
//  //               Found last stop in the chain.                    //
//  //  This stop decay vertex should be the stop -> W b + Xi vertex  //
//  //----------------------------------------------------------------//
//
//  //  bool hasDecayVtx = particle->hasDecayVtx();
//  //  if (hasDecayVtx){
//
//  //    const xAOD::TruthVertex* decayVtx = particle->decayVtx();
//  int npart = particle->nChildren(); //decayVtx->nOutgoingParticles();
//
//    //----------------------------------------------------------------//
//    //             Loop over decay products of the stop               //
//    //----------------------------------------------------------------//
//
//    for (int j=0; j<npart; j++){
//
//      const xAOD::TruthParticle* particle1 = particle->child(j); //decayVtx->outgoingParticle1(j);
//      pdgId = particle1->pdgId();
//      status = particle1->status();
//
//      if ( verbose ) std::cout << "The stop to top decay vertex, id of particle " << j << " of stop1 decayvtx is " << pdgId << std::endl;
//
//      double px = particle1->px()/GeV;
//      double py = particle1->py()/GeV;
//      double pz = particle1->pz()/GeV;
//      double e = particle1->e()/GeV;
//
//      if (verbose) std::cout << "inside stop1 decay vertex loop..... " << std::endl;
//      if (verbose) std::cout << "The id of particle1 " << j << " of stop1 decayvtx is " << pdgId << std::endl;
//      if (verbose) std::cout << "The status of particle1 "<< j << " of stop1 decayvtx " << status << std::endl;
//
//      //---------------------//
//      //      Find the B     //
//      //---------------------//
//
//      if (abs(pdgId) == 5){
//	B_tmp->SetPxPyPzE(px,py,pz,e);
//	if (verbose) std::cout << "stop1_b_px is " << px << std::endl;
//      }
//
//      //----------------------------------------------//
//      //     Find any susy particle1s, this is the Xi  //
//      //----------------------------------------------//
//
//      //if (abs(pdgId) >= 100000){
//      //  Xi_tmp->SetPxPyPzE(px,py,pz,e);
//      //}
//
//      //-------------------------//
//      //     Find the W boson    //
//      //-------------------------//
//
//      if (abs(pdgId) == 24){
//	isSTopHad = Find_WDecayProducts( particle1, WJ1_tmp, WJ2_tmp );          
//      }
//
//      if ( abs(pdgId) == 6 ) {
//	Top_tmp->SetPxPyPzE(px,py,pz,e);
//	if (verbose) std::cout << "stop1_t_p is " << sqrt(px*px+py*py+pz*pz) << std::endl;
//	isSTopHad = Find_TopDecayProducts( particle1, B_tmp, WJ1_tmp, WJ2_tmp );
//      }
//    }
//
//  return isSTopHad;
//}

int TruthxAODAnalysis_ttbar::Find_TopDecayProducts( const xAOD::TruthParticle* particle,
						    TLorentzVector *B_tmp,
						    TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp ) {


  int isSTopHad = 2;

  //---------------------------------------//
  //     Find decay product of Top boson   //
  //---------------------------------------//

  //  bool hasDecayVtx = particle->hasDecayVtx();
  int pdgId = particle->pdgId();
  int status = particle->status();
  //  if (hasDecayVtx){
  //    const xAOD::TruthVertex* decayVtx = particle->decayVtx();

    const xAOD::TruthParticle* particle_daughter;
    //    const xAOD::TruthVertex* decayVtx_daughter = decayVtx;


    //----------------------------------------------------//
    //   Again the W can decay into other W in a chain    //
    //   Keep looking until you find the final t->b,w     //
    //----------------------------------------------------//

    while (abs(pdgId)==6){
      particle_daughter = particle->child(0); //decayVtx_daughter->outgoingParticle(0);
      //      decayVtx = decayVtx_daughter;
      //    decayVtx_daughter = particle_daughter->decayVtx();
      pdgId = particle_daughter->pdgId();
      if ( abs(pdgId)==6 ) particle = particle_daughter;
    }

    //-----------------------------------------//
    //         Finally found T decay           //
    //-----------------------------------------//

    int npart = particle->nChildren();//decayVtx->nOutgoingParticles();

    if (verbose) std::cout << "top npart is " << npart << std::endl;
    status = particle->status();
    if (verbose) std::cout << "(top) status of top that decays to non top is: " << status << std::endl;

    //------------------------------------------//
    //         Assign T decay products          //
    //------------------------------------------//

    //bool j1_exists = false;

    for (int k=0; k<npart;k++) {
      const xAOD::TruthParticle* particle_daugtherS = particle->child(k);//decayVtx->outgoingParticle(k);
      pdgId = particle_daugtherS->pdgId();
      status = particle_daugtherS->status();
      //if (verbose) std::cout << "inside top decay vertex loop..... " << std::endl;
      //if (verbose) std::cout << "The id of particle " << k << " of tdecayvtx is " << pdgId << std::endl;
      //if (verbose) std::cout << "The status of particle "<< k << " of tdecayvtx " << status << std::endl;

      if (abs(pdgId) ==5) {

        double px = particle_daugtherS->px()/GeV;
        double py = particle_daugtherS->py()/GeV;
        double pz = particle_daugtherS->pz()/GeV;
        double e = particle_daugtherS->e()/GeV;

	B_tmp->SetPxPyPzE(px,py,pz,e);
        std::cout << "B pt: " << B_tmp->Pt() << " eta: " << B_tmp->Eta() << ", phi: " << B_tmp->Phi() << std::endl;

      }
      
      if (abs(pdgId) == 24) {
	//std::cout << "inside top decay vertex loop..... " << std::endl;
	//std::cout << "The id of particle " << k << " of tdecayvtx is " << particle_daugtherS->pdgId() << std::endl;
	//std::cout << "The status of particle "<< k << " of tdecayvtx " << particle_daugtherS->status() << std::endl;
	//std::cout << "Child1ID " << particle_daugtherS->nChildren() << std::endl;
	//std::cout << "Child2ID" << particle_daugtherS->child(1)->pdgId() << std::endl;
        isSTopHad = Find_WDecayProducts( particle_daugtherS, WJ1_tmp, WJ2_tmp );
      }
    }

  return isSTopHad;

}

int TruthxAODAnalysis_ttbar::Find_WDecayProducts( const xAOD::TruthParticle* particle, 
						  TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp ) {
  
  //---------------------------------------------------//
  //  I'm passing the address of WJ1_tmp and WJ2_tmp   //
  //  This means that the TLorentzVector sent to WJ1   //
  //  and WJ2 will have its momenta set as those of    //
  //  the WDecay product.                              //
  //  Use pointers to pass objects between methods     //
  //---------------------------------------------------//

  int isSTopHad = 1;
  if( particle->nChildren()<1 ) return -999;  

  //---------------------------------------//                                                       
  //     Find decay product of W boson     //                                                       
  //---------------------------------------//                                                       
  //  bool hasDecayVtx = particle->hasDecayVtx();
  int pdgId = particle->pdgId();
  int status = particle->status();
  //  if (hasDecayVtx){
  //const xAOD::TruthVertex* decayVtx = particle->decayVtx();

  const xAOD::TruthParticle* particle_daughter;
  //  const xAOD::TruthVertex* decayVtx_daughter = decayVtx;


    //----------------------------------------------------//                                        
    //   Again the W can decay into other W in a chain    //                                        
    //   Keep looking until you find the final W->j1,j2   //                                        
    //----------------------------------------------------//                                        

    while (abs(pdgId)==24){
      particle_daughter = particle->child(0);//outgoingParticle(0);
      //      decayVtx = decayVtx_daughter;
      //      decayVtx_daughter = particle_daughter->decayVtx();
      pdgId = particle_daughter->pdgId();
      if ( abs(pdgId) == 24 ) {
	particle = particle_daughter;
      }
    }

    //-----------------------------------------//                                                   
    //         Finally found W decay           //                                                   
    //-----------------------------------------//                                                   

    int npart = particle->nChildren();//decayVtx->nOutgoingParticles();

    if (verbose) std::cout << "W npart is " << npart << " " << particle->pdgId() << std::endl;
    status = particle->status();
    if (verbose) std::cout << "(Stop1) status of W that decays to non W is: " << status << std::endl;

    //------------------------------------------//                                                  
    //         Assign W decay products          //                                                  
    //------------------------------------------//                                                  

    bool j1_exists = false;

    for (int k=0; k<npart;k++){
      const xAOD::TruthParticle* particle_daugther = particle->child(k);//decayVtx->outgoingParticle(k);
      pdgId = particle_daugther->pdgId();
      status = particle_daugther->status();
      //if (verbose) std::cout << "inside W decay vertex loop..... " << std::endl;
      //if (verbose) std::cout << "The id of particle " << k << " of Wdecayvtx is " << pdgId << std::endl;
      //if (verbose) std::cout << "The status of particle "<< k << " of Wdecayvtx " << status << std::endl;

      //we expect dileptonic or semileptonic ttbar!!
      if ((abs(pdgId) >=1) && (abs(pdgId) <=6)) {
        double wj_px = particle_daugther->px()/GeV;
        double wj_py = particle_daugther->py()/GeV;
        double wj_pz = particle_daugther->pz()/GeV;
        double wj_e = particle_daugther->e()/GeV;
      
        if ( !j1_exists ) {
          if (verbose) std::cout << "inside stop1W first jet loop " << std::endl;
          j1_exists = true;
          WJ1_tmp->SetPxPyPzE(wj_px,wj_py,wj_pz,wj_e);
        }
        else {
          if (verbose) std::cout << "inside stop2W first jet loop " << std::endl;
          WJ2_tmp->SetPxPyPzE(wj_px,wj_py,wj_pz,wj_e);
          isSTopHad = 0;
        }
      }
      if ((abs(pdgId) == 11) || (abs(pdgId)==13)) {
        double wj_px = particle_daugther->px()/GeV;
        double wj_py = particle_daugther->py()/GeV;
        double wj_pz = particle_daugther->pz()/GeV;
        double wj_e = particle_daugther->e()/GeV;
	WJ1_tmp->SetPxPyPzE(wj_px,wj_py,wj_pz,wj_e);
	isSTopHad = 1;
      }
      if ( abs(pdgId) == 15 ) {
        double wj_px = particle_daugther->px()/GeV;
        double wj_py = particle_daugther->py()/GeV;
        double wj_pz = particle_daugther->pz()/GeV;
        double wj_e = particle_daugther->e()/GeV;
        WJ1_tmp->SetPxPyPzE(wj_px,wj_py,wj_pz,wj_e);
        isSTopHad = 2;
      } 
      if ((abs(pdgId) == 12) || (abs(pdgId)==14) || (abs(pdgId)==16)) {
        double wj_px = particle_daugther->px()/GeV;
        double wj_py = particle_daugther->py()/GeV;
        double wj_pz = particle_daugther->pz()/GeV;
        double wj_e = particle_daugther->e()/GeV;
	WJ2_tmp->SetPxPyPzE(wj_px,wj_py,wj_pz,wj_e);
      }

      std::cout << "w decay product " << pdgId << std::endl;
      std::cout << " WJ1 pt: " << WJ1_tmp->Pt() << " eta: " << WJ1_tmp->Eta() << ", phi: " << WJ1_tmp->Phi() << std::endl;
      std::cout << " WJ2 pt: " << WJ2_tmp->Pt() << " eta: " << WJ2_tmp->Eta() << ", phi: " << WJ2_tmp->Phi() << std::endl;
 
    }
    //  }
    //  else {
    //    std::cout << "WTF, W has no decay product.  MC is broken!!!!" << std::endl;
    //  }

  return isSTopHad;

}

int TruthxAODAnalysis_ttbar::PlotTruth_SoftTop(TVector3 boost1, TVector3 boost2, double weight){

  return 1.0;
}

void TruthxAODAnalysis_ttbar::SetVerbose(int i) {
  if ( i == 0 ) verbose = false;
  else verbose = true;
  return;
}

void TruthxAODAnalysis_ttbar::plot_signal(int icut, double weight){
}

void TruthxAODAnalysis_ttbar::FindThrustDir(){

  if (verbose) if (verbose) std::cout <<"finding thrust"<<std::endl;

  thrust_vec.SetXYZT(0,0,0,0);

  TString Formula("(abs(x*[0]+y*[1])+");
  TString tmp;

  uint nsize = 4+nISR;
  if ( nISR > 3 ) nsize = 7;
  for( uint i=1; i < nsize; i++ ) {
    tmp = TString(Form("abs(x*[%u]+y*[%u])+",2*i,2*i+1));
    Formula = Formula+tmp;
    }

  tmp = TString(Form("abs(x*[%u]+y*[%u]))",2*nsize,2*nsize+1));
  Formula = Formula+tmp;

  TF2 fcn("ThrustFunction",Formula.Data(),-1.,1.,-1.,1.);

  fcn.FixParameter(0, stop1_b.Px());
  fcn.FixParameter(1, stop1_b.Py());
  fcn.FixParameter(2, stop1_wj1.Px());
  fcn.FixParameter(3, stop1_wj1.Py());
  fcn.FixParameter(4, stop1_wj2.Px());
  fcn.FixParameter(5, stop1_wj2.Py());

  fcn.FixParameter(6, stop2_b.Px());
  fcn.FixParameter(7, stop2_b.Py());
  fcn.FixParameter(8, stop2_wj1.Px());
  fcn.FixParameter(9, stop2_wj1.Py());
  fcn.FixParameter(10, stop2_wj2.Px());
  fcn.FixParameter(11, stop2_wj2.Py());

  fcn.FixParameter(12, dixi.Px());
  fcn.FixParameter(13, dixi.Py());

  for (int i = 0; i<nISR; i++) {
    fcn.FixParameter(14+2*i, ISR_p[i][0]);
    fcn.FixParameter(15+2*i, ISR_p[i][1]);
  }
  /*
  if ( nISR > 1 ) {
    fcn.FixParameter(12, ISR_p[;
    fcn.FixParameter(13, ISR2.Py());
  }
  if ( nISR > 2 ) {
    fcn.FixParameter(14, ISR3.Px());
    fcn.FixParameter(15, ISR3.Py());
    }*/

  double bestX, bestY;
  fcn.Eval(0.5,0.5);
  fcn.GetMaximumXY(bestX,bestY);

  double total = sqrt(bestX*bestX+bestY*bestY);
  bestX = bestX/total;
  bestY = bestY/total;

  // point positive in the direction of the met                                                     
  if ( (dixi.Px()*bestX + dixi.Py()*bestY) < 0 ) {
    bestX = -bestX;
    bestY = -bestY;
  }

  thrust_vec.SetXYZT(bestX,bestY,0,0);

  double bestPerpX, bestPerpY;
  bestPerpX = 1.0 - bestX*bestX;
  bestPerpY = 0.0 - bestX*bestY;
  double totPerp = sqrt(bestPerpX*bestPerpX+bestPerpY*bestPerpY);
  bestPerpX = bestPerpX/totPerp;
  bestPerpY = bestPerpY/totPerp;

  //  define positive as cross product with positive result              
  if ( (bestX*bestPerpY - bestPerpX*bestY) < 0 ) {
    bestPerpX = - bestPerpX;
    bestPerpY = - bestPerpY;
  }

  thrust_perp_vec.SetXYZT(bestPerpX,bestPerpY,0,0);

  return;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: finalize ()
{
  ps->Close();
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_ttbar :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}

//  LocalWords:  DeltaEta
