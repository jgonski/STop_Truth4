// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RestFrames_Dict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "../inc/RestFrames/CombinatoricGroup.hh"
#include "../inc/RestFrames/CombinatoricJigsaw.hh"
#include "../inc/RestFrames/CombinatoricState.hh"
#include "../inc/RestFrames/ContraBoostInvJigsaw.hh"
#include "../inc/RestFrames/DecayFrame.hh"
#include "../inc/RestFrames/DecayGenFrame.hh"
#include "../inc/RestFrames/DecayRecoFrame.hh"
#include "../inc/RestFrames/GeneratorFrame.hh"
#include "../inc/RestFrames/Group.hh"
#include "../inc/RestFrames/HistPlot.hh"
#include "../inc/RestFrames/HistPlotVar.hh"
#include "../inc/RestFrames/InvisibleFrame.hh"
#include "../inc/RestFrames/InvisibleGenFrame.hh"
#include "../inc/RestFrames/InvisibleGroup.hh"
#include "../inc/RestFrames/InvisibleJigsaw.hh"
#include "../inc/RestFrames/InvisibleRecoFrame.hh"
#include "../inc/RestFrames/InvisibleState.hh"
#include "../inc/RestFrames/Jigsaw.hh"
#include "../inc/RestFrames/LabFrame.hh"
#include "../inc/RestFrames/LabGenFrame.hh"
#include "../inc/RestFrames/LabRecoFrame.hh"
#include "../inc/RestFrames/MinMassesCombJigsaw.hh"
#include "../inc/RestFrames/ReconstructionFrame.hh"
#include "../inc/RestFrames/ResonanceGenFrame.hh"
#include "../inc/RestFrames/RestFrame.hh"
#include "../inc/RestFrames/RestFrames.hh"
#include "../inc/RestFrames/RFBase.hh"
#include "../inc/RestFrames/RFKey.hh"
#include "../inc/RestFrames/RFList.hh"
#include "../inc/RestFrames/RFLog.hh"
#include "../inc/RestFrames/RFPlot.hh"
#include "../inc/RestFrames/SelfAssemblingRecoFrame.hh"
#include "../inc/RestFrames/SetMassInvJigsaw.hh"
#include "../inc/RestFrames/SetRapidityInvJigsaw.hh"
#include "../inc/RestFrames/State.hh"
#include "../inc/RestFrames/TreePlot.hh"
#include "../inc/RestFrames/TreePlotLink.hh"
#include "../inc/RestFrames/TreePlotNode.hh"
#include "../inc/RestFrames/VisibleFrame.hh"
#include "../inc/RestFrames/VisibleGenFrame.hh"
#include "../inc/RestFrames/VisibleRecoFrame.hh"
#include "../inc/RestFrames/VisibleState.hh"

// Header files passed via #pragma extra_include

namespace RestFrames {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *RestFrames_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("RestFrames", 0 /*version*/, "RestFrames/RFKey.hh", 33,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &RestFrames_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *RestFrames_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *RestFramescLcLRFBase_Dictionary();
   static void RestFramescLcLRFBase_TClassManip(TClass*);
   static void *new_RestFramescLcLRFBase(void *p = 0);
   static void *newArray_RestFramescLcLRFBase(Long_t size, void *p);
   static void delete_RestFramescLcLRFBase(void *p);
   static void deleteArray_RestFramescLcLRFBase(void *p);
   static void destruct_RestFramescLcLRFBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFBase*)
   {
      ::RestFrames::RFBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFBase));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFBase", "RestFrames/RFBase.hh", 71,
                  typeid(::RestFrames::RFBase), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFBase_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFBase) );
      instance.SetNew(&new_RestFramescLcLRFBase);
      instance.SetNewArray(&newArray_RestFramescLcLRFBase);
      instance.SetDelete(&delete_RestFramescLcLRFBase);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFBase);
      instance.SetDestructor(&destruct_RestFramescLcLRFBase);

      ::ROOT::AddClassAlternate("RestFrames::RFBase","RFBase");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFBase*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFBase*)0x0)->GetClass();
      RestFramescLcLRFBase_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFBase_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFKey_Dictionary();
   static void RestFramescLcLRFKey_TClassManip(TClass*);
   static void delete_RestFramescLcLRFKey(void *p);
   static void deleteArray_RestFramescLcLRFKey(void *p);
   static void destruct_RestFramescLcLRFKey(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFKey*)
   {
      ::RestFrames::RFKey *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFKey));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFKey", "RestFrames/RFKey.hh", 38,
                  typeid(::RestFrames::RFKey), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFKey_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFKey) );
      instance.SetDelete(&delete_RestFramescLcLRFKey);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFKey);
      instance.SetDestructor(&destruct_RestFramescLcLRFKey);

      ::ROOT::AddClassAlternate("RestFrames::RFKey","RFKey");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFKey*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFKey*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFKey*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFKey_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFKey*)0x0)->GetClass();
      RestFramescLcLRFKey_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFKey_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLRFBasegR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLRFBasegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLRFBasegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::RFBase>*)
   {
      ::RestFrames::RFList<RestFrames::RFBase> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::RFBase>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::RFBase>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::RFBase>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLRFBasegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::RFBase>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLRFBasegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLRFBasegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLRFBasegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLRFBasegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLRFBasegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::RFBase>","RFList<RFBase>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::RFBase>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::RFBase>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::RFBase>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLRFBasegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::RFBase>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLRFBasegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLRFBasegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLRestFramegR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLRestFramegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLRestFramegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::RestFrame>*)
   {
      ::RestFrames::RFList<RestFrames::RestFrame> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::RestFrame>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::RestFrame>", "RestFrames/RFList.hh", 239,
                  typeid(::RestFrames::RFList<RestFrames::RestFrame>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLRestFramegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::RestFrame>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLRestFramegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLRestFramegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLRestFramegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLRestFramegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLRestFramegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::RestFrame>","RFList<RestFrame>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::RestFrame>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::RestFrame>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::RestFrame>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLRestFramegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::RestFrame>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLRestFramegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLRestFramegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_Dictionary();
   static void RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p);
   static void deleteArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p);
   static void destruct_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<const RestFrames::RestFrame>*)
   {
      ::RestFrames::RFList<const RestFrames::RestFrame> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<const RestFrames::RestFrame>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<const RestFrames::RestFrame>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<const RestFrames::RestFrame>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<const RestFrames::RestFrame>) );
      instance.SetNew(&new_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<const RestFrames::RestFrame>","RFList<const RestFrame>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<const RestFrames::RestFrame>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<const RestFrames::RestFrame>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<const RestFrames::RestFrame>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<const RestFrames::RestFrame>*)0x0)->GetClass();
      RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::ReconstructionFrame>*)
   {
      ::RestFrames::RFList<RestFrames::ReconstructionFrame> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::ReconstructionFrame>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::ReconstructionFrame>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::ReconstructionFrame>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::ReconstructionFrame>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::ReconstructionFrame>","RFList<ReconstructionFrame>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::ReconstructionFrame>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::ReconstructionFrame>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::ReconstructionFrame>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::ReconstructionFrame>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLReconstructionFramegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::GeneratorFrame>*)
   {
      ::RestFrames::RFList<RestFrames::GeneratorFrame> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::GeneratorFrame>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::GeneratorFrame>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::GeneratorFrame>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::GeneratorFrame>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::GeneratorFrame>","RFList<GeneratorFrame>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::GeneratorFrame>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::GeneratorFrame>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::GeneratorFrame>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::GeneratorFrame>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLGeneratorFramegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)
   {
      ::RestFrames::RFList<RestFrames::ResonanceGenFrame> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::ResonanceGenFrame>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::ResonanceGenFrame>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::ResonanceGenFrame>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::ResonanceGenFrame>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::ResonanceGenFrame>","RFList<ResonanceGenFrame>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLJigsawgR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLJigsawgR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLJigsawgR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::Jigsaw>*)
   {
      ::RestFrames::RFList<RestFrames::Jigsaw> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::Jigsaw>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::Jigsaw>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::Jigsaw>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLJigsawgR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::Jigsaw>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLJigsawgR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLJigsawgR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLJigsawgR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLJigsawgR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLJigsawgR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::Jigsaw>","RFList<Jigsaw>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::Jigsaw>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::Jigsaw>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::Jigsaw>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLJigsawgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::Jigsaw>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLJigsawgR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLJigsawgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLGroupgR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLGroupgR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLGroupgR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::Group>*)
   {
      ::RestFrames::RFList<RestFrames::Group> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::Group>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::Group>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::Group>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLGroupgR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::Group>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLGroupgR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLGroupgR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLGroupgR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLGroupgR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLGroupgR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::Group>","RFList<Group>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::Group>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::Group>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::Group>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLGroupgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::Group>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLGroupgR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLGroupgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLStategR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLStategR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLStategR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLStategR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLStategR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLStategR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLStategR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::State>*)
   {
      ::RestFrames::RFList<RestFrames::State> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::State>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::State>", "RestFrames/RFList.hh", 221,
                  typeid(::RestFrames::RFList<RestFrames::State>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLStategR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::State>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLStategR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLStategR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLStategR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLStategR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLStategR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::State>","RFList<State>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::State>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::State>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::State>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLStategR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::State>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLStategR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLStategR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLVisibleStategR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLVisibleStategR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::VisibleState>*)
   {
      ::RestFrames::RFList<RestFrames::VisibleState> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::VisibleState>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::VisibleState>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::VisibleState>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLVisibleStategR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::VisibleState>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLVisibleStategR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLVisibleStategR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLVisibleStategR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::VisibleState>","RFList<VisibleState>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::VisibleState>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::VisibleState>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::VisibleState>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLVisibleStategR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::VisibleState>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLVisibleStategR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLVisibleStategR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLInvisibleStategR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLInvisibleStategR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::InvisibleState>*)
   {
      ::RestFrames::RFList<RestFrames::InvisibleState> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::InvisibleState>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::InvisibleState>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::InvisibleState>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLInvisibleStategR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::InvisibleState>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLInvisibleStategR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLInvisibleStategR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLInvisibleStategR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::InvisibleState>","RFList<InvisibleState>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::InvisibleState>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::InvisibleState>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::InvisibleState>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLInvisibleStategR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::InvisibleState>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLInvisibleStategR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLInvisibleStategR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_Dictionary();
   static void RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_TClassManip(TClass*);
   static void *new_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p = 0);
   static void *newArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(Long_t size, void *p);
   static void delete_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p);
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p);
   static void destruct_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFList<RestFrames::CombinatoricState>*)
   {
      ::RestFrames::RFList<RestFrames::CombinatoricState> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFList<RestFrames::CombinatoricState>));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFList<RestFrames::CombinatoricState>", "RestFrames/RFList.hh", 208,
                  typeid(::RestFrames::RFList<RestFrames::CombinatoricState>), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFList<RestFrames::CombinatoricState>) );
      instance.SetNew(&new_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR);
      instance.SetNewArray(&newArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR);
      instance.SetDelete(&delete_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR);
      instance.SetDestructor(&destruct_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR);

      ::ROOT::AddClassAlternate("RestFrames::RFList<RestFrames::CombinatoricState>","RFList<CombinatoricState>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFList<RestFrames::CombinatoricState>*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFList<RestFrames::CombinatoricState>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::CombinatoricState>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFList<RestFrames::CombinatoricState>*)0x0)->GetClass();
      RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFListlERestFramescLcLCombinatoricStategR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRFLog_Dictionary();
   static void RestFramescLcLRFLog_TClassManip(TClass*);
   static void *new_RestFramescLcLRFLog(void *p = 0);
   static void *newArray_RestFramescLcLRFLog(Long_t size, void *p);
   static void delete_RestFramescLcLRFLog(void *p);
   static void deleteArray_RestFramescLcLRFLog(void *p);
   static void destruct_RestFramescLcLRFLog(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RFLog*)
   {
      ::RestFrames::RFLog *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RFLog));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RFLog", "RestFrames/RFLog.hh", 53,
                  typeid(::RestFrames::RFLog), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRFLog_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RFLog) );
      instance.SetNew(&new_RestFramescLcLRFLog);
      instance.SetNewArray(&newArray_RestFramescLcLRFLog);
      instance.SetDelete(&delete_RestFramescLcLRFLog);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRFLog);
      instance.SetDestructor(&destruct_RestFramescLcLRFLog);

      ::ROOT::AddClassAlternate("RestFrames::RFLog","RFLog");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RFLog*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RFLog*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RFLog*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRFLog_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RFLog*)0x0)->GetClass();
      RestFramescLcLRFLog_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRFLog_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLRestFrame_Dictionary();
   static void RestFramescLcLRestFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLRestFrame(void *p);
   static void deleteArray_RestFramescLcLRestFrame(void *p);
   static void destruct_RestFramescLcLRestFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::RestFrame*)
   {
      ::RestFrames::RestFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::RestFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::RestFrame", "RestFrames/RestFrame.hh", 46,
                  typeid(::RestFrames::RestFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLRestFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::RestFrame) );
      instance.SetDelete(&delete_RestFramescLcLRestFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLRestFrame);
      instance.SetDestructor(&destruct_RestFramescLcLRestFrame);

      ::ROOT::AddClassAlternate("RestFrames::RestFrame","RestFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::RestFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::RestFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::RestFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLRestFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::RestFrame*)0x0)->GetClass();
      RestFramescLcLRestFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLRestFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLGeneratorFrame_Dictionary();
   static void RestFramescLcLGeneratorFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLGeneratorFrame(void *p = 0);
   static void *newArray_RestFramescLcLGeneratorFrame(Long_t size, void *p);
   static void delete_RestFramescLcLGeneratorFrame(void *p);
   static void deleteArray_RestFramescLcLGeneratorFrame(void *p);
   static void destruct_RestFramescLcLGeneratorFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::GeneratorFrame*)
   {
      ::RestFrames::GeneratorFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::GeneratorFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::GeneratorFrame", "RestFrames/GeneratorFrame.hh", 43,
                  typeid(::RestFrames::GeneratorFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLGeneratorFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::GeneratorFrame) );
      instance.SetNew(&new_RestFramescLcLGeneratorFrame);
      instance.SetNewArray(&newArray_RestFramescLcLGeneratorFrame);
      instance.SetDelete(&delete_RestFramescLcLGeneratorFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLGeneratorFrame);
      instance.SetDestructor(&destruct_RestFramescLcLGeneratorFrame);

      ::ROOT::AddClassAlternate("RestFrames::GeneratorFrame","GeneratorFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::GeneratorFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::GeneratorFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::GeneratorFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLGeneratorFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::GeneratorFrame*)0x0)->GetClass();
      RestFramescLcLGeneratorFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLGeneratorFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLLabGenFrame_Dictionary();
   static void RestFramescLcLLabGenFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLLabGenFrame(void *p);
   static void deleteArray_RestFramescLcLLabGenFrame(void *p);
   static void destruct_RestFramescLcLLabGenFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::LabGenFrame*)
   {
      ::RestFrames::LabGenFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::LabGenFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::LabGenFrame", "RestFrames/LabGenFrame.hh", 43,
                  typeid(::RestFrames::LabGenFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLLabGenFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::LabGenFrame) );
      instance.SetDelete(&delete_RestFramescLcLLabGenFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLLabGenFrame);
      instance.SetDestructor(&destruct_RestFramescLcLLabGenFrame);

      ::ROOT::AddClassAlternate("RestFrames::LabGenFrame","LabGenFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::LabGenFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::LabGenFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::LabGenFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLLabGenFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::LabGenFrame*)0x0)->GetClass();
      RestFramescLcLLabGenFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLLabGenFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLDecayGenFrame_Dictionary();
   static void RestFramescLcLDecayGenFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLDecayGenFrame(void *p);
   static void deleteArray_RestFramescLcLDecayGenFrame(void *p);
   static void destruct_RestFramescLcLDecayGenFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::DecayGenFrame*)
   {
      ::RestFrames::DecayGenFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::DecayGenFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::DecayGenFrame", "RestFrames/DecayGenFrame.hh", 47,
                  typeid(::RestFrames::DecayGenFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLDecayGenFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::DecayGenFrame) );
      instance.SetDelete(&delete_RestFramescLcLDecayGenFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLDecayGenFrame);
      instance.SetDestructor(&destruct_RestFramescLcLDecayGenFrame);

      ::ROOT::AddClassAlternate("RestFrames::DecayGenFrame","DecayGenFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::DecayGenFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::DecayGenFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::DecayGenFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLDecayGenFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::DecayGenFrame*)0x0)->GetClass();
      RestFramescLcLDecayGenFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLDecayGenFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLInvisibleGenFrame_Dictionary();
   static void RestFramescLcLInvisibleGenFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLInvisibleGenFrame(void *p);
   static void deleteArray_RestFramescLcLInvisibleGenFrame(void *p);
   static void destruct_RestFramescLcLInvisibleGenFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::InvisibleGenFrame*)
   {
      ::RestFrames::InvisibleGenFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::InvisibleGenFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::InvisibleGenFrame", "RestFrames/InvisibleGenFrame.hh", 43,
                  typeid(::RestFrames::InvisibleGenFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLInvisibleGenFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::InvisibleGenFrame) );
      instance.SetDelete(&delete_RestFramescLcLInvisibleGenFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLInvisibleGenFrame);
      instance.SetDestructor(&destruct_RestFramescLcLInvisibleGenFrame);

      ::ROOT::AddClassAlternate("RestFrames::InvisibleGenFrame","InvisibleGenFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::InvisibleGenFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::InvisibleGenFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::InvisibleGenFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLInvisibleGenFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::InvisibleGenFrame*)0x0)->GetClass();
      RestFramescLcLInvisibleGenFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLInvisibleGenFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLVisibleGenFrame_Dictionary();
   static void RestFramescLcLVisibleGenFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLVisibleGenFrame(void *p = 0);
   static void *newArray_RestFramescLcLVisibleGenFrame(Long_t size, void *p);
   static void delete_RestFramescLcLVisibleGenFrame(void *p);
   static void deleteArray_RestFramescLcLVisibleGenFrame(void *p);
   static void destruct_RestFramescLcLVisibleGenFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::VisibleGenFrame*)
   {
      ::RestFrames::VisibleGenFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::VisibleGenFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::VisibleGenFrame", "RestFrames/VisibleGenFrame.hh", 43,
                  typeid(::RestFrames::VisibleGenFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLVisibleGenFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::VisibleGenFrame) );
      instance.SetNew(&new_RestFramescLcLVisibleGenFrame);
      instance.SetNewArray(&newArray_RestFramescLcLVisibleGenFrame);
      instance.SetDelete(&delete_RestFramescLcLVisibleGenFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLVisibleGenFrame);
      instance.SetDestructor(&destruct_RestFramescLcLVisibleGenFrame);

      ::ROOT::AddClassAlternate("RestFrames::VisibleGenFrame","VisibleGenFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::VisibleGenFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::VisibleGenFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::VisibleGenFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLVisibleGenFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::VisibleGenFrame*)0x0)->GetClass();
      RestFramescLcLVisibleGenFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLVisibleGenFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLResonanceGenFrame_Dictionary();
   static void RestFramescLcLResonanceGenFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLResonanceGenFrame(void *p = 0);
   static void *newArray_RestFramescLcLResonanceGenFrame(Long_t size, void *p);
   static void delete_RestFramescLcLResonanceGenFrame(void *p);
   static void deleteArray_RestFramescLcLResonanceGenFrame(void *p);
   static void destruct_RestFramescLcLResonanceGenFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::ResonanceGenFrame*)
   {
      ::RestFrames::ResonanceGenFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::ResonanceGenFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::ResonanceGenFrame", "RestFrames/ResonanceGenFrame.hh", 42,
                  typeid(::RestFrames::ResonanceGenFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLResonanceGenFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::ResonanceGenFrame) );
      instance.SetNew(&new_RestFramescLcLResonanceGenFrame);
      instance.SetNewArray(&newArray_RestFramescLcLResonanceGenFrame);
      instance.SetDelete(&delete_RestFramescLcLResonanceGenFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLResonanceGenFrame);
      instance.SetDestructor(&destruct_RestFramescLcLResonanceGenFrame);

      ::ROOT::AddClassAlternate("RestFrames::ResonanceGenFrame","ResonanceGenFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::ResonanceGenFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::ResonanceGenFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::ResonanceGenFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLResonanceGenFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::ResonanceGenFrame*)0x0)->GetClass();
      RestFramescLcLResonanceGenFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLResonanceGenFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLReconstructionFrame_Dictionary();
   static void RestFramescLcLReconstructionFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLReconstructionFrame(void *p = 0);
   static void *newArray_RestFramescLcLReconstructionFrame(Long_t size, void *p);
   static void delete_RestFramescLcLReconstructionFrame(void *p);
   static void deleteArray_RestFramescLcLReconstructionFrame(void *p);
   static void destruct_RestFramescLcLReconstructionFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::ReconstructionFrame*)
   {
      ::RestFrames::ReconstructionFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::ReconstructionFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::ReconstructionFrame", "RestFrames/ReconstructionFrame.hh", 43,
                  typeid(::RestFrames::ReconstructionFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLReconstructionFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::ReconstructionFrame) );
      instance.SetNew(&new_RestFramescLcLReconstructionFrame);
      instance.SetNewArray(&newArray_RestFramescLcLReconstructionFrame);
      instance.SetDelete(&delete_RestFramescLcLReconstructionFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLReconstructionFrame);
      instance.SetDestructor(&destruct_RestFramescLcLReconstructionFrame);

      ::ROOT::AddClassAlternate("RestFrames::ReconstructionFrame","ReconstructionFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::ReconstructionFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::ReconstructionFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::ReconstructionFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLReconstructionFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::ReconstructionFrame*)0x0)->GetClass();
      RestFramescLcLReconstructionFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLReconstructionFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLLabRecoFrame_Dictionary();
   static void RestFramescLcLLabRecoFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLLabRecoFrame(void *p = 0);
   static void *newArray_RestFramescLcLLabRecoFrame(Long_t size, void *p);
   static void delete_RestFramescLcLLabRecoFrame(void *p);
   static void deleteArray_RestFramescLcLLabRecoFrame(void *p);
   static void destruct_RestFramescLcLLabRecoFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::LabRecoFrame*)
   {
      ::RestFrames::LabRecoFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::LabRecoFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::LabRecoFrame", "RestFrames/LabRecoFrame.hh", 46,
                  typeid(::RestFrames::LabRecoFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLLabRecoFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::LabRecoFrame) );
      instance.SetNew(&new_RestFramescLcLLabRecoFrame);
      instance.SetNewArray(&newArray_RestFramescLcLLabRecoFrame);
      instance.SetDelete(&delete_RestFramescLcLLabRecoFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLLabRecoFrame);
      instance.SetDestructor(&destruct_RestFramescLcLLabRecoFrame);

      ::ROOT::AddClassAlternate("RestFrames::LabRecoFrame","LabRecoFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::LabRecoFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::LabRecoFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::LabRecoFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLLabRecoFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::LabRecoFrame*)0x0)->GetClass();
      RestFramescLcLLabRecoFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLLabRecoFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLDecayRecoFrame_Dictionary();
   static void RestFramescLcLDecayRecoFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLDecayRecoFrame(void *p);
   static void deleteArray_RestFramescLcLDecayRecoFrame(void *p);
   static void destruct_RestFramescLcLDecayRecoFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::DecayRecoFrame*)
   {
      ::RestFrames::DecayRecoFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::DecayRecoFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::DecayRecoFrame", "RestFrames/DecayRecoFrame.hh", 45,
                  typeid(::RestFrames::DecayRecoFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLDecayRecoFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::DecayRecoFrame) );
      instance.SetDelete(&delete_RestFramescLcLDecayRecoFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLDecayRecoFrame);
      instance.SetDestructor(&destruct_RestFramescLcLDecayRecoFrame);

      ::ROOT::AddClassAlternate("RestFrames::DecayRecoFrame","DecayRecoFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::DecayRecoFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::DecayRecoFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::DecayRecoFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLDecayRecoFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::DecayRecoFrame*)0x0)->GetClass();
      RestFramescLcLDecayRecoFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLDecayRecoFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLInvisibleRecoFrame_Dictionary();
   static void RestFramescLcLInvisibleRecoFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLInvisibleRecoFrame(void *p);
   static void deleteArray_RestFramescLcLInvisibleRecoFrame(void *p);
   static void destruct_RestFramescLcLInvisibleRecoFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::InvisibleRecoFrame*)
   {
      ::RestFrames::InvisibleRecoFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::InvisibleRecoFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::InvisibleRecoFrame", "RestFrames/InvisibleRecoFrame.hh", 43,
                  typeid(::RestFrames::InvisibleRecoFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLInvisibleRecoFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::InvisibleRecoFrame) );
      instance.SetDelete(&delete_RestFramescLcLInvisibleRecoFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLInvisibleRecoFrame);
      instance.SetDestructor(&destruct_RestFramescLcLInvisibleRecoFrame);

      ::ROOT::AddClassAlternate("RestFrames::InvisibleRecoFrame","InvisibleRecoFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::InvisibleRecoFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::InvisibleRecoFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::InvisibleRecoFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLInvisibleRecoFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::InvisibleRecoFrame*)0x0)->GetClass();
      RestFramescLcLInvisibleRecoFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLInvisibleRecoFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLVisibleRecoFrame_Dictionary();
   static void RestFramescLcLVisibleRecoFrame_TClassManip(TClass*);
   static void *new_RestFramescLcLVisibleRecoFrame(void *p = 0);
   static void *newArray_RestFramescLcLVisibleRecoFrame(Long_t size, void *p);
   static void delete_RestFramescLcLVisibleRecoFrame(void *p);
   static void deleteArray_RestFramescLcLVisibleRecoFrame(void *p);
   static void destruct_RestFramescLcLVisibleRecoFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::VisibleRecoFrame*)
   {
      ::RestFrames::VisibleRecoFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::VisibleRecoFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::VisibleRecoFrame", "RestFrames/VisibleRecoFrame.hh", 43,
                  typeid(::RestFrames::VisibleRecoFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLVisibleRecoFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::VisibleRecoFrame) );
      instance.SetNew(&new_RestFramescLcLVisibleRecoFrame);
      instance.SetNewArray(&newArray_RestFramescLcLVisibleRecoFrame);
      instance.SetDelete(&delete_RestFramescLcLVisibleRecoFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLVisibleRecoFrame);
      instance.SetDestructor(&destruct_RestFramescLcLVisibleRecoFrame);

      ::ROOT::AddClassAlternate("RestFrames::VisibleRecoFrame","VisibleRecoFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::VisibleRecoFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::VisibleRecoFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::VisibleRecoFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLVisibleRecoFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::VisibleRecoFrame*)0x0)->GetClass();
      RestFramescLcLVisibleRecoFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLVisibleRecoFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLSelfAssemblingRecoFrame_Dictionary();
   static void RestFramescLcLSelfAssemblingRecoFrame_TClassManip(TClass*);
   static void delete_RestFramescLcLSelfAssemblingRecoFrame(void *p);
   static void deleteArray_RestFramescLcLSelfAssemblingRecoFrame(void *p);
   static void destruct_RestFramescLcLSelfAssemblingRecoFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::SelfAssemblingRecoFrame*)
   {
      ::RestFrames::SelfAssemblingRecoFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::SelfAssemblingRecoFrame));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::SelfAssemblingRecoFrame", "RestFrames/SelfAssemblingRecoFrame.hh", 43,
                  typeid(::RestFrames::SelfAssemblingRecoFrame), DefineBehavior(ptr, ptr),
                  &RestFramescLcLSelfAssemblingRecoFrame_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::SelfAssemblingRecoFrame) );
      instance.SetDelete(&delete_RestFramescLcLSelfAssemblingRecoFrame);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLSelfAssemblingRecoFrame);
      instance.SetDestructor(&destruct_RestFramescLcLSelfAssemblingRecoFrame);

      ::ROOT::AddClassAlternate("RestFrames::SelfAssemblingRecoFrame","SelfAssemblingRecoFrame");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::SelfAssemblingRecoFrame*)
   {
      return GenerateInitInstanceLocal((::RestFrames::SelfAssemblingRecoFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::SelfAssemblingRecoFrame*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLSelfAssemblingRecoFrame_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::SelfAssemblingRecoFrame*)0x0)->GetClass();
      RestFramescLcLSelfAssemblingRecoFrame_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLSelfAssemblingRecoFrame_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLGroup_Dictionary();
   static void RestFramescLcLGroup_TClassManip(TClass*);
   static void delete_RestFramescLcLGroup(void *p);
   static void deleteArray_RestFramescLcLGroup(void *p);
   static void destruct_RestFramescLcLGroup(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::Group*)
   {
      ::RestFrames::Group *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::Group));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::Group", "RestFrames/Group.hh", 48,
                  typeid(::RestFrames::Group), DefineBehavior(ptr, ptr),
                  &RestFramescLcLGroup_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::Group) );
      instance.SetDelete(&delete_RestFramescLcLGroup);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLGroup);
      instance.SetDestructor(&destruct_RestFramescLcLGroup);

      ::ROOT::AddClassAlternate("RestFrames::Group","Group");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::Group*)
   {
      return GenerateInitInstanceLocal((::RestFrames::Group*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::Group*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLGroup_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::Group*)0x0)->GetClass();
      RestFramescLcLGroup_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLGroup_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLInvisibleGroup_Dictionary();
   static void RestFramescLcLInvisibleGroup_TClassManip(TClass*);
   static void *new_RestFramescLcLInvisibleGroup(void *p = 0);
   static void *newArray_RestFramescLcLInvisibleGroup(Long_t size, void *p);
   static void delete_RestFramescLcLInvisibleGroup(void *p);
   static void deleteArray_RestFramescLcLInvisibleGroup(void *p);
   static void destruct_RestFramescLcLInvisibleGroup(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::InvisibleGroup*)
   {
      ::RestFrames::InvisibleGroup *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::InvisibleGroup));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::InvisibleGroup", "RestFrames/InvisibleGroup.hh", 42,
                  typeid(::RestFrames::InvisibleGroup), DefineBehavior(ptr, ptr),
                  &RestFramescLcLInvisibleGroup_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::InvisibleGroup) );
      instance.SetNew(&new_RestFramescLcLInvisibleGroup);
      instance.SetNewArray(&newArray_RestFramescLcLInvisibleGroup);
      instance.SetDelete(&delete_RestFramescLcLInvisibleGroup);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLInvisibleGroup);
      instance.SetDestructor(&destruct_RestFramescLcLInvisibleGroup);

      ::ROOT::AddClassAlternate("RestFrames::InvisibleGroup","InvisibleGroup");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::InvisibleGroup*)
   {
      return GenerateInitInstanceLocal((::RestFrames::InvisibleGroup*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::InvisibleGroup*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLInvisibleGroup_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::InvisibleGroup*)0x0)->GetClass();
      RestFramescLcLInvisibleGroup_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLInvisibleGroup_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLCombinatoricGroup_Dictionary();
   static void RestFramescLcLCombinatoricGroup_TClassManip(TClass*);
   static void *new_RestFramescLcLCombinatoricGroup(void *p = 0);
   static void *newArray_RestFramescLcLCombinatoricGroup(Long_t size, void *p);
   static void delete_RestFramescLcLCombinatoricGroup(void *p);
   static void deleteArray_RestFramescLcLCombinatoricGroup(void *p);
   static void destruct_RestFramescLcLCombinatoricGroup(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::CombinatoricGroup*)
   {
      ::RestFrames::CombinatoricGroup *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::CombinatoricGroup));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::CombinatoricGroup", "RestFrames/CombinatoricGroup.hh", 41,
                  typeid(::RestFrames::CombinatoricGroup), DefineBehavior(ptr, ptr),
                  &RestFramescLcLCombinatoricGroup_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::CombinatoricGroup) );
      instance.SetNew(&new_RestFramescLcLCombinatoricGroup);
      instance.SetNewArray(&newArray_RestFramescLcLCombinatoricGroup);
      instance.SetDelete(&delete_RestFramescLcLCombinatoricGroup);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLCombinatoricGroup);
      instance.SetDestructor(&destruct_RestFramescLcLCombinatoricGroup);

      ::ROOT::AddClassAlternate("RestFrames::CombinatoricGroup","CombinatoricGroup");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::CombinatoricGroup*)
   {
      return GenerateInitInstanceLocal((::RestFrames::CombinatoricGroup*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::CombinatoricGroup*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLCombinatoricGroup_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::CombinatoricGroup*)0x0)->GetClass();
      RestFramescLcLCombinatoricGroup_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLCombinatoricGroup_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLJigsaw_Dictionary();
   static void RestFramescLcLJigsaw_TClassManip(TClass*);
   static void delete_RestFramescLcLJigsaw(void *p);
   static void deleteArray_RestFramescLcLJigsaw(void *p);
   static void destruct_RestFramescLcLJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::Jigsaw*)
   {
      ::RestFrames::Jigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::Jigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::Jigsaw", "RestFrames/Jigsaw.hh", 46,
                  typeid(::RestFrames::Jigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::Jigsaw) );
      instance.SetDelete(&delete_RestFramescLcLJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::Jigsaw","Jigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::Jigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::Jigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::Jigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::Jigsaw*)0x0)->GetClass();
      RestFramescLcLJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLInvisibleJigsaw_Dictionary();
   static void RestFramescLcLInvisibleJigsaw_TClassManip(TClass*);
   static void delete_RestFramescLcLInvisibleJigsaw(void *p);
   static void deleteArray_RestFramescLcLInvisibleJigsaw(void *p);
   static void destruct_RestFramescLcLInvisibleJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::InvisibleJigsaw*)
   {
      ::RestFrames::InvisibleJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::InvisibleJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::InvisibleJigsaw", "RestFrames/InvisibleJigsaw.hh", 41,
                  typeid(::RestFrames::InvisibleJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLInvisibleJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::InvisibleJigsaw) );
      instance.SetDelete(&delete_RestFramescLcLInvisibleJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLInvisibleJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLInvisibleJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::InvisibleJigsaw","InvisibleJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::InvisibleJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::InvisibleJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::InvisibleJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLInvisibleJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::InvisibleJigsaw*)0x0)->GetClass();
      RestFramescLcLInvisibleJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLInvisibleJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLSetMassInvJigsaw_Dictionary();
   static void RestFramescLcLSetMassInvJigsaw_TClassManip(TClass*);
   static void *new_RestFramescLcLSetMassInvJigsaw(void *p = 0);
   static void *newArray_RestFramescLcLSetMassInvJigsaw(Long_t size, void *p);
   static void delete_RestFramescLcLSetMassInvJigsaw(void *p);
   static void deleteArray_RestFramescLcLSetMassInvJigsaw(void *p);
   static void destruct_RestFramescLcLSetMassInvJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::SetMassInvJigsaw*)
   {
      ::RestFrames::SetMassInvJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::SetMassInvJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::SetMassInvJigsaw", "RestFrames/SetMassInvJigsaw.hh", 39,
                  typeid(::RestFrames::SetMassInvJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLSetMassInvJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::SetMassInvJigsaw) );
      instance.SetNew(&new_RestFramescLcLSetMassInvJigsaw);
      instance.SetNewArray(&newArray_RestFramescLcLSetMassInvJigsaw);
      instance.SetDelete(&delete_RestFramescLcLSetMassInvJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLSetMassInvJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLSetMassInvJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::SetMassInvJigsaw","SetMassInvJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::SetMassInvJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::SetMassInvJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::SetMassInvJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLSetMassInvJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::SetMassInvJigsaw*)0x0)->GetClass();
      RestFramescLcLSetMassInvJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLSetMassInvJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLSetRapidityInvJigsaw_Dictionary();
   static void RestFramescLcLSetRapidityInvJigsaw_TClassManip(TClass*);
   static void *new_RestFramescLcLSetRapidityInvJigsaw(void *p = 0);
   static void *newArray_RestFramescLcLSetRapidityInvJigsaw(Long_t size, void *p);
   static void delete_RestFramescLcLSetRapidityInvJigsaw(void *p);
   static void deleteArray_RestFramescLcLSetRapidityInvJigsaw(void *p);
   static void destruct_RestFramescLcLSetRapidityInvJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::SetRapidityInvJigsaw*)
   {
      ::RestFrames::SetRapidityInvJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::SetRapidityInvJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::SetRapidityInvJigsaw", "RestFrames/SetRapidityInvJigsaw.hh", 39,
                  typeid(::RestFrames::SetRapidityInvJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLSetRapidityInvJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::SetRapidityInvJigsaw) );
      instance.SetNew(&new_RestFramescLcLSetRapidityInvJigsaw);
      instance.SetNewArray(&newArray_RestFramescLcLSetRapidityInvJigsaw);
      instance.SetDelete(&delete_RestFramescLcLSetRapidityInvJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLSetRapidityInvJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLSetRapidityInvJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::SetRapidityInvJigsaw","SetRapidityInvJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::SetRapidityInvJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::SetRapidityInvJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::SetRapidityInvJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLSetRapidityInvJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::SetRapidityInvJigsaw*)0x0)->GetClass();
      RestFramescLcLSetRapidityInvJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLSetRapidityInvJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLContraBoostInvJigsaw_Dictionary();
   static void RestFramescLcLContraBoostInvJigsaw_TClassManip(TClass*);
   static void *new_RestFramescLcLContraBoostInvJigsaw(void *p = 0);
   static void *newArray_RestFramescLcLContraBoostInvJigsaw(Long_t size, void *p);
   static void delete_RestFramescLcLContraBoostInvJigsaw(void *p);
   static void deleteArray_RestFramescLcLContraBoostInvJigsaw(void *p);
   static void destruct_RestFramescLcLContraBoostInvJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::ContraBoostInvJigsaw*)
   {
      ::RestFrames::ContraBoostInvJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::ContraBoostInvJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::ContraBoostInvJigsaw", "RestFrames/ContraBoostInvJigsaw.hh", 39,
                  typeid(::RestFrames::ContraBoostInvJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLContraBoostInvJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::ContraBoostInvJigsaw) );
      instance.SetNew(&new_RestFramescLcLContraBoostInvJigsaw);
      instance.SetNewArray(&newArray_RestFramescLcLContraBoostInvJigsaw);
      instance.SetDelete(&delete_RestFramescLcLContraBoostInvJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLContraBoostInvJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLContraBoostInvJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::ContraBoostInvJigsaw","ContraBoostInvJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::ContraBoostInvJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::ContraBoostInvJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::ContraBoostInvJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLContraBoostInvJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::ContraBoostInvJigsaw*)0x0)->GetClass();
      RestFramescLcLContraBoostInvJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLContraBoostInvJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLCombinatoricJigsaw_Dictionary();
   static void RestFramescLcLCombinatoricJigsaw_TClassManip(TClass*);
   static void delete_RestFramescLcLCombinatoricJigsaw(void *p);
   static void deleteArray_RestFramescLcLCombinatoricJigsaw(void *p);
   static void destruct_RestFramescLcLCombinatoricJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::CombinatoricJigsaw*)
   {
      ::RestFrames::CombinatoricJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::CombinatoricJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::CombinatoricJigsaw", "RestFrames/CombinatoricJigsaw.hh", 41,
                  typeid(::RestFrames::CombinatoricJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLCombinatoricJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::CombinatoricJigsaw) );
      instance.SetDelete(&delete_RestFramescLcLCombinatoricJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLCombinatoricJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLCombinatoricJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::CombinatoricJigsaw","CombinatoricJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::CombinatoricJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::CombinatoricJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::CombinatoricJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLCombinatoricJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::CombinatoricJigsaw*)0x0)->GetClass();
      RestFramescLcLCombinatoricJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLCombinatoricJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLMinMassesCombJigsaw_Dictionary();
   static void RestFramescLcLMinMassesCombJigsaw_TClassManip(TClass*);
   static void *new_RestFramescLcLMinMassesCombJigsaw(void *p = 0);
   static void *newArray_RestFramescLcLMinMassesCombJigsaw(Long_t size, void *p);
   static void delete_RestFramescLcLMinMassesCombJigsaw(void *p);
   static void deleteArray_RestFramescLcLMinMassesCombJigsaw(void *p);
   static void destruct_RestFramescLcLMinMassesCombJigsaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::MinMassesCombJigsaw*)
   {
      ::RestFrames::MinMassesCombJigsaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::MinMassesCombJigsaw));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::MinMassesCombJigsaw", "RestFrames/MinMassesCombJigsaw.hh", 42,
                  typeid(::RestFrames::MinMassesCombJigsaw), DefineBehavior(ptr, ptr),
                  &RestFramescLcLMinMassesCombJigsaw_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::MinMassesCombJigsaw) );
      instance.SetNew(&new_RestFramescLcLMinMassesCombJigsaw);
      instance.SetNewArray(&newArray_RestFramescLcLMinMassesCombJigsaw);
      instance.SetDelete(&delete_RestFramescLcLMinMassesCombJigsaw);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLMinMassesCombJigsaw);
      instance.SetDestructor(&destruct_RestFramescLcLMinMassesCombJigsaw);

      ::ROOT::AddClassAlternate("RestFrames::MinMassesCombJigsaw","MinMassesCombJigsaw");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::MinMassesCombJigsaw*)
   {
      return GenerateInitInstanceLocal((::RestFrames::MinMassesCombJigsaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::MinMassesCombJigsaw*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLMinMassesCombJigsaw_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::MinMassesCombJigsaw*)0x0)->GetClass();
      RestFramescLcLMinMassesCombJigsaw_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLMinMassesCombJigsaw_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLState_Dictionary();
   static void RestFramescLcLState_TClassManip(TClass*);
   static void delete_RestFramescLcLState(void *p);
   static void deleteArray_RestFramescLcLState(void *p);
   static void destruct_RestFramescLcLState(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::State*)
   {
      ::RestFrames::State *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::State));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::State", "RestFrames/State.hh", 45,
                  typeid(::RestFrames::State), DefineBehavior(ptr, ptr),
                  &RestFramescLcLState_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::State) );
      instance.SetDelete(&delete_RestFramescLcLState);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLState);
      instance.SetDestructor(&destruct_RestFramescLcLState);

      ::ROOT::AddClassAlternate("RestFrames::State","State");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::State*)
   {
      return GenerateInitInstanceLocal((::RestFrames::State*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::State*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLState_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::State*)0x0)->GetClass();
      RestFramescLcLState_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLState_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLVisibleState_Dictionary();
   static void RestFramescLcLVisibleState_TClassManip(TClass*);
   static void *new_RestFramescLcLVisibleState(void *p = 0);
   static void *newArray_RestFramescLcLVisibleState(Long_t size, void *p);
   static void delete_RestFramescLcLVisibleState(void *p);
   static void deleteArray_RestFramescLcLVisibleState(void *p);
   static void destruct_RestFramescLcLVisibleState(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::VisibleState*)
   {
      ::RestFrames::VisibleState *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::VisibleState));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::VisibleState", "RestFrames/VisibleState.hh", 41,
                  typeid(::RestFrames::VisibleState), DefineBehavior(ptr, ptr),
                  &RestFramescLcLVisibleState_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::VisibleState) );
      instance.SetNew(&new_RestFramescLcLVisibleState);
      instance.SetNewArray(&newArray_RestFramescLcLVisibleState);
      instance.SetDelete(&delete_RestFramescLcLVisibleState);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLVisibleState);
      instance.SetDestructor(&destruct_RestFramescLcLVisibleState);

      ::ROOT::AddClassAlternate("RestFrames::VisibleState","VisibleState");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::VisibleState*)
   {
      return GenerateInitInstanceLocal((::RestFrames::VisibleState*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::VisibleState*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLVisibleState_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::VisibleState*)0x0)->GetClass();
      RestFramescLcLVisibleState_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLVisibleState_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLInvisibleState_Dictionary();
   static void RestFramescLcLInvisibleState_TClassManip(TClass*);
   static void *new_RestFramescLcLInvisibleState(void *p = 0);
   static void *newArray_RestFramescLcLInvisibleState(Long_t size, void *p);
   static void delete_RestFramescLcLInvisibleState(void *p);
   static void deleteArray_RestFramescLcLInvisibleState(void *p);
   static void destruct_RestFramescLcLInvisibleState(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::InvisibleState*)
   {
      ::RestFrames::InvisibleState *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::InvisibleState));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::InvisibleState", "RestFrames/InvisibleState.hh", 42,
                  typeid(::RestFrames::InvisibleState), DefineBehavior(ptr, ptr),
                  &RestFramescLcLInvisibleState_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::InvisibleState) );
      instance.SetNew(&new_RestFramescLcLInvisibleState);
      instance.SetNewArray(&newArray_RestFramescLcLInvisibleState);
      instance.SetDelete(&delete_RestFramescLcLInvisibleState);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLInvisibleState);
      instance.SetDestructor(&destruct_RestFramescLcLInvisibleState);

      ::ROOT::AddClassAlternate("RestFrames::InvisibleState","InvisibleState");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::InvisibleState*)
   {
      return GenerateInitInstanceLocal((::RestFrames::InvisibleState*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::InvisibleState*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLInvisibleState_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::InvisibleState*)0x0)->GetClass();
      RestFramescLcLInvisibleState_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLInvisibleState_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLCombinatoricState_Dictionary();
   static void RestFramescLcLCombinatoricState_TClassManip(TClass*);
   static void *new_RestFramescLcLCombinatoricState(void *p = 0);
   static void *newArray_RestFramescLcLCombinatoricState(Long_t size, void *p);
   static void delete_RestFramescLcLCombinatoricState(void *p);
   static void deleteArray_RestFramescLcLCombinatoricState(void *p);
   static void destruct_RestFramescLcLCombinatoricState(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::CombinatoricState*)
   {
      ::RestFrames::CombinatoricState *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::CombinatoricState));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::CombinatoricState", "RestFrames/CombinatoricState.hh", 45,
                  typeid(::RestFrames::CombinatoricState), DefineBehavior(ptr, ptr),
                  &RestFramescLcLCombinatoricState_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::CombinatoricState) );
      instance.SetNew(&new_RestFramescLcLCombinatoricState);
      instance.SetNewArray(&newArray_RestFramescLcLCombinatoricState);
      instance.SetDelete(&delete_RestFramescLcLCombinatoricState);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLCombinatoricState);
      instance.SetDestructor(&destruct_RestFramescLcLCombinatoricState);

      ::ROOT::AddClassAlternate("RestFrames::CombinatoricState","CombinatoricState");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::CombinatoricState*)
   {
      return GenerateInitInstanceLocal((::RestFrames::CombinatoricState*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::CombinatoricState*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLCombinatoricState_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::CombinatoricState*)0x0)->GetClass();
      RestFramescLcLCombinatoricState_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLCombinatoricState_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLTreePlot_Dictionary();
   static void RestFramescLcLTreePlot_TClassManip(TClass*);
   static void delete_RestFramescLcLTreePlot(void *p);
   static void deleteArray_RestFramescLcLTreePlot(void *p);
   static void destruct_RestFramescLcLTreePlot(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::TreePlot*)
   {
      ::RestFrames::TreePlot *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::TreePlot));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::TreePlot", "RestFrames/TreePlot.hh", 66,
                  typeid(::RestFrames::TreePlot), DefineBehavior(ptr, ptr),
                  &RestFramescLcLTreePlot_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::TreePlot) );
      instance.SetDelete(&delete_RestFramescLcLTreePlot);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLTreePlot);
      instance.SetDestructor(&destruct_RestFramescLcLTreePlot);

      ::ROOT::AddClassAlternate("RestFrames::TreePlot","TreePlot");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::TreePlot*)
   {
      return GenerateInitInstanceLocal((::RestFrames::TreePlot*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::TreePlot*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLTreePlot_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::TreePlot*)0x0)->GetClass();
      RestFramescLcLTreePlot_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLTreePlot_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLHistPlot_Dictionary();
   static void RestFramescLcLHistPlot_TClassManip(TClass*);
   static void delete_RestFramescLcLHistPlot(void *p);
   static void deleteArray_RestFramescLcLHistPlot(void *p);
   static void destruct_RestFramescLcLHistPlot(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::HistPlot*)
   {
      ::RestFrames::HistPlot *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::HistPlot));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::HistPlot", "RestFrames/HistPlot.hh", 46,
                  typeid(::RestFrames::HistPlot), DefineBehavior(ptr, ptr),
                  &RestFramescLcLHistPlot_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::HistPlot) );
      instance.SetDelete(&delete_RestFramescLcLHistPlot);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLHistPlot);
      instance.SetDestructor(&destruct_RestFramescLcLHistPlot);

      ::ROOT::AddClassAlternate("RestFrames::HistPlot","HistPlot");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::HistPlot*)
   {
      return GenerateInitInstanceLocal((::RestFrames::HistPlot*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::HistPlot*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLHistPlot_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::HistPlot*)0x0)->GetClass();
      RestFramescLcLHistPlot_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLHistPlot_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RestFramescLcLHistPlotVar_Dictionary();
   static void RestFramescLcLHistPlotVar_TClassManip(TClass*);
   static void delete_RestFramescLcLHistPlotVar(void *p);
   static void deleteArray_RestFramescLcLHistPlotVar(void *p);
   static void destruct_RestFramescLcLHistPlotVar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RestFrames::HistPlotVar*)
   {
      ::RestFrames::HistPlotVar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RestFrames::HistPlotVar));
      static ::ROOT::TGenericClassInfo 
         instance("RestFrames::HistPlotVar", "RestFrames/HistPlotVar.hh", 42,
                  typeid(::RestFrames::HistPlotVar), DefineBehavior(ptr, ptr),
                  &RestFramescLcLHistPlotVar_Dictionary, isa_proxy, 0,
                  sizeof(::RestFrames::HistPlotVar) );
      instance.SetDelete(&delete_RestFramescLcLHistPlotVar);
      instance.SetDeleteArray(&deleteArray_RestFramescLcLHistPlotVar);
      instance.SetDestructor(&destruct_RestFramescLcLHistPlotVar);

      ::ROOT::AddClassAlternate("RestFrames::HistPlotVar","HistPlotVar");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RestFrames::HistPlotVar*)
   {
      return GenerateInitInstanceLocal((::RestFrames::HistPlotVar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RestFrames::HistPlotVar*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RestFramescLcLHistPlotVar_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RestFrames::HistPlotVar*)0x0)->GetClass();
      RestFramescLcLHistPlotVar_TClassManip(theClass);
   return theClass;
   }

   static void RestFramescLcLHistPlotVar_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFBase(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFBase : new ::RestFrames::RFBase;
   }
   static void *newArray_RestFramescLcLRFBase(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFBase[nElements] : new ::RestFrames::RFBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFBase(void *p) {
      delete ((::RestFrames::RFBase*)p);
   }
   static void deleteArray_RestFramescLcLRFBase(void *p) {
      delete [] ((::RestFrames::RFBase*)p);
   }
   static void destruct_RestFramescLcLRFBase(void *p) {
      typedef ::RestFrames::RFBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFBase

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFKey(void *p) {
      delete ((::RestFrames::RFKey*)p);
   }
   static void deleteArray_RestFramescLcLRFKey(void *p) {
      delete [] ((::RestFrames::RFKey*)p);
   }
   static void destruct_RestFramescLcLRFKey(void *p) {
      typedef ::RestFrames::RFKey current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFKey

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::RFBase> : new ::RestFrames::RFList<RestFrames::RFBase>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLRFBasegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::RFBase>[nElements] : new ::RestFrames::RFList<RestFrames::RFBase>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::RFBase>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::RFBase>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLRFBasegR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::RFBase> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::RFBase>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::RestFrame> : new ::RestFrames::RFList<RestFrames::RestFrame>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLRestFramegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::RestFrame>[nElements] : new ::RestFrames::RFList<RestFrames::RestFrame>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::RestFrame>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::RestFrame>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLRestFramegR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::RestFrame> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::RestFrame>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<const RestFrames::RestFrame> : new ::RestFrames::RFList<const RestFrames::RestFrame>;
   }
   static void *newArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<const RestFrames::RestFrame>[nElements] : new ::RestFrames::RFList<const RestFrames::RestFrame>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p) {
      delete ((::RestFrames::RFList<const RestFrames::RestFrame>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p) {
      delete [] ((::RestFrames::RFList<const RestFrames::RestFrame>*)p);
   }
   static void destruct_RestFramescLcLRFListlEconstsPRestFramescLcLRestFramegR(void *p) {
      typedef ::RestFrames::RFList<const RestFrames::RestFrame> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<const RestFrames::RestFrame>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::ReconstructionFrame> : new ::RestFrames::RFList<RestFrames::ReconstructionFrame>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::ReconstructionFrame>[nElements] : new ::RestFrames::RFList<RestFrames::ReconstructionFrame>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::ReconstructionFrame>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::ReconstructionFrame>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLReconstructionFramegR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::ReconstructionFrame> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::ReconstructionFrame>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::GeneratorFrame> : new ::RestFrames::RFList<RestFrames::GeneratorFrame>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::GeneratorFrame>[nElements] : new ::RestFrames::RFList<RestFrames::GeneratorFrame>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::GeneratorFrame>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::GeneratorFrame>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLGeneratorFramegR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::GeneratorFrame> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::GeneratorFrame>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::ResonanceGenFrame> : new ::RestFrames::RFList<RestFrames::ResonanceGenFrame>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::ResonanceGenFrame>[nElements] : new ::RestFrames::RFList<RestFrames::ResonanceGenFrame>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::ResonanceGenFrame>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLResonanceGenFramegR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::ResonanceGenFrame> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::ResonanceGenFrame>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::Jigsaw> : new ::RestFrames::RFList<RestFrames::Jigsaw>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLJigsawgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::Jigsaw>[nElements] : new ::RestFrames::RFList<RestFrames::Jigsaw>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::Jigsaw>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::Jigsaw>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLJigsawgR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::Jigsaw> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::Jigsaw>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::Group> : new ::RestFrames::RFList<RestFrames::Group>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLGroupgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::Group>[nElements] : new ::RestFrames::RFList<RestFrames::Group>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::Group>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::Group>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLGroupgR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::Group> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::Group>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLStategR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::State> : new ::RestFrames::RFList<RestFrames::State>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLStategR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::State>[nElements] : new ::RestFrames::RFList<RestFrames::State>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLStategR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::State>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLStategR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::State>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLStategR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::State> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::State>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::VisibleState> : new ::RestFrames::RFList<RestFrames::VisibleState>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::VisibleState>[nElements] : new ::RestFrames::RFList<RestFrames::VisibleState>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::VisibleState>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::VisibleState>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLVisibleStategR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::VisibleState> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::VisibleState>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::InvisibleState> : new ::RestFrames::RFList<RestFrames::InvisibleState>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::InvisibleState>[nElements] : new ::RestFrames::RFList<RestFrames::InvisibleState>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::InvisibleState>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::InvisibleState>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLInvisibleStategR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::InvisibleState> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::InvisibleState>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::CombinatoricState> : new ::RestFrames::RFList<RestFrames::CombinatoricState>;
   }
   static void *newArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFList<RestFrames::CombinatoricState>[nElements] : new ::RestFrames::RFList<RestFrames::CombinatoricState>[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p) {
      delete ((::RestFrames::RFList<RestFrames::CombinatoricState>*)p);
   }
   static void deleteArray_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p) {
      delete [] ((::RestFrames::RFList<RestFrames::CombinatoricState>*)p);
   }
   static void destruct_RestFramescLcLRFListlERestFramescLcLCombinatoricStategR(void *p) {
      typedef ::RestFrames::RFList<RestFrames::CombinatoricState> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFList<RestFrames::CombinatoricState>

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLRFLog(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFLog : new ::RestFrames::RFLog;
   }
   static void *newArray_RestFramescLcLRFLog(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::RFLog[nElements] : new ::RestFrames::RFLog[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLRFLog(void *p) {
      delete ((::RestFrames::RFLog*)p);
   }
   static void deleteArray_RestFramescLcLRFLog(void *p) {
      delete [] ((::RestFrames::RFLog*)p);
   }
   static void destruct_RestFramescLcLRFLog(void *p) {
      typedef ::RestFrames::RFLog current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RFLog

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLRestFrame(void *p) {
      delete ((::RestFrames::RestFrame*)p);
   }
   static void deleteArray_RestFramescLcLRestFrame(void *p) {
      delete [] ((::RestFrames::RestFrame*)p);
   }
   static void destruct_RestFramescLcLRestFrame(void *p) {
      typedef ::RestFrames::RestFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::RestFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLGeneratorFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::GeneratorFrame : new ::RestFrames::GeneratorFrame;
   }
   static void *newArray_RestFramescLcLGeneratorFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::GeneratorFrame[nElements] : new ::RestFrames::GeneratorFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLGeneratorFrame(void *p) {
      delete ((::RestFrames::GeneratorFrame*)p);
   }
   static void deleteArray_RestFramescLcLGeneratorFrame(void *p) {
      delete [] ((::RestFrames::GeneratorFrame*)p);
   }
   static void destruct_RestFramescLcLGeneratorFrame(void *p) {
      typedef ::RestFrames::GeneratorFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::GeneratorFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLLabGenFrame(void *p) {
      delete ((::RestFrames::LabGenFrame*)p);
   }
   static void deleteArray_RestFramescLcLLabGenFrame(void *p) {
      delete [] ((::RestFrames::LabGenFrame*)p);
   }
   static void destruct_RestFramescLcLLabGenFrame(void *p) {
      typedef ::RestFrames::LabGenFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::LabGenFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLDecayGenFrame(void *p) {
      delete ((::RestFrames::DecayGenFrame*)p);
   }
   static void deleteArray_RestFramescLcLDecayGenFrame(void *p) {
      delete [] ((::RestFrames::DecayGenFrame*)p);
   }
   static void destruct_RestFramescLcLDecayGenFrame(void *p) {
      typedef ::RestFrames::DecayGenFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::DecayGenFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLInvisibleGenFrame(void *p) {
      delete ((::RestFrames::InvisibleGenFrame*)p);
   }
   static void deleteArray_RestFramescLcLInvisibleGenFrame(void *p) {
      delete [] ((::RestFrames::InvisibleGenFrame*)p);
   }
   static void destruct_RestFramescLcLInvisibleGenFrame(void *p) {
      typedef ::RestFrames::InvisibleGenFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::InvisibleGenFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLVisibleGenFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleGenFrame : new ::RestFrames::VisibleGenFrame;
   }
   static void *newArray_RestFramescLcLVisibleGenFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleGenFrame[nElements] : new ::RestFrames::VisibleGenFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLVisibleGenFrame(void *p) {
      delete ((::RestFrames::VisibleGenFrame*)p);
   }
   static void deleteArray_RestFramescLcLVisibleGenFrame(void *p) {
      delete [] ((::RestFrames::VisibleGenFrame*)p);
   }
   static void destruct_RestFramescLcLVisibleGenFrame(void *p) {
      typedef ::RestFrames::VisibleGenFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::VisibleGenFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLResonanceGenFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ResonanceGenFrame : new ::RestFrames::ResonanceGenFrame;
   }
   static void *newArray_RestFramescLcLResonanceGenFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ResonanceGenFrame[nElements] : new ::RestFrames::ResonanceGenFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLResonanceGenFrame(void *p) {
      delete ((::RestFrames::ResonanceGenFrame*)p);
   }
   static void deleteArray_RestFramescLcLResonanceGenFrame(void *p) {
      delete [] ((::RestFrames::ResonanceGenFrame*)p);
   }
   static void destruct_RestFramescLcLResonanceGenFrame(void *p) {
      typedef ::RestFrames::ResonanceGenFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::ResonanceGenFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLReconstructionFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ReconstructionFrame : new ::RestFrames::ReconstructionFrame;
   }
   static void *newArray_RestFramescLcLReconstructionFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ReconstructionFrame[nElements] : new ::RestFrames::ReconstructionFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLReconstructionFrame(void *p) {
      delete ((::RestFrames::ReconstructionFrame*)p);
   }
   static void deleteArray_RestFramescLcLReconstructionFrame(void *p) {
      delete [] ((::RestFrames::ReconstructionFrame*)p);
   }
   static void destruct_RestFramescLcLReconstructionFrame(void *p) {
      typedef ::RestFrames::ReconstructionFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::ReconstructionFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLLabRecoFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::LabRecoFrame : new ::RestFrames::LabRecoFrame;
   }
   static void *newArray_RestFramescLcLLabRecoFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::LabRecoFrame[nElements] : new ::RestFrames::LabRecoFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLLabRecoFrame(void *p) {
      delete ((::RestFrames::LabRecoFrame*)p);
   }
   static void deleteArray_RestFramescLcLLabRecoFrame(void *p) {
      delete [] ((::RestFrames::LabRecoFrame*)p);
   }
   static void destruct_RestFramescLcLLabRecoFrame(void *p) {
      typedef ::RestFrames::LabRecoFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::LabRecoFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLDecayRecoFrame(void *p) {
      delete ((::RestFrames::DecayRecoFrame*)p);
   }
   static void deleteArray_RestFramescLcLDecayRecoFrame(void *p) {
      delete [] ((::RestFrames::DecayRecoFrame*)p);
   }
   static void destruct_RestFramescLcLDecayRecoFrame(void *p) {
      typedef ::RestFrames::DecayRecoFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::DecayRecoFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLInvisibleRecoFrame(void *p) {
      delete ((::RestFrames::InvisibleRecoFrame*)p);
   }
   static void deleteArray_RestFramescLcLInvisibleRecoFrame(void *p) {
      delete [] ((::RestFrames::InvisibleRecoFrame*)p);
   }
   static void destruct_RestFramescLcLInvisibleRecoFrame(void *p) {
      typedef ::RestFrames::InvisibleRecoFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::InvisibleRecoFrame

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLVisibleRecoFrame(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleRecoFrame : new ::RestFrames::VisibleRecoFrame;
   }
   static void *newArray_RestFramescLcLVisibleRecoFrame(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleRecoFrame[nElements] : new ::RestFrames::VisibleRecoFrame[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLVisibleRecoFrame(void *p) {
      delete ((::RestFrames::VisibleRecoFrame*)p);
   }
   static void deleteArray_RestFramescLcLVisibleRecoFrame(void *p) {
      delete [] ((::RestFrames::VisibleRecoFrame*)p);
   }
   static void destruct_RestFramescLcLVisibleRecoFrame(void *p) {
      typedef ::RestFrames::VisibleRecoFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::VisibleRecoFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLSelfAssemblingRecoFrame(void *p) {
      delete ((::RestFrames::SelfAssemblingRecoFrame*)p);
   }
   static void deleteArray_RestFramescLcLSelfAssemblingRecoFrame(void *p) {
      delete [] ((::RestFrames::SelfAssemblingRecoFrame*)p);
   }
   static void destruct_RestFramescLcLSelfAssemblingRecoFrame(void *p) {
      typedef ::RestFrames::SelfAssemblingRecoFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::SelfAssemblingRecoFrame

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLGroup(void *p) {
      delete ((::RestFrames::Group*)p);
   }
   static void deleteArray_RestFramescLcLGroup(void *p) {
      delete [] ((::RestFrames::Group*)p);
   }
   static void destruct_RestFramescLcLGroup(void *p) {
      typedef ::RestFrames::Group current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::Group

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLInvisibleGroup(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::InvisibleGroup : new ::RestFrames::InvisibleGroup;
   }
   static void *newArray_RestFramescLcLInvisibleGroup(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::InvisibleGroup[nElements] : new ::RestFrames::InvisibleGroup[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLInvisibleGroup(void *p) {
      delete ((::RestFrames::InvisibleGroup*)p);
   }
   static void deleteArray_RestFramescLcLInvisibleGroup(void *p) {
      delete [] ((::RestFrames::InvisibleGroup*)p);
   }
   static void destruct_RestFramescLcLInvisibleGroup(void *p) {
      typedef ::RestFrames::InvisibleGroup current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::InvisibleGroup

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLCombinatoricGroup(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::CombinatoricGroup : new ::RestFrames::CombinatoricGroup;
   }
   static void *newArray_RestFramescLcLCombinatoricGroup(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::CombinatoricGroup[nElements] : new ::RestFrames::CombinatoricGroup[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLCombinatoricGroup(void *p) {
      delete ((::RestFrames::CombinatoricGroup*)p);
   }
   static void deleteArray_RestFramescLcLCombinatoricGroup(void *p) {
      delete [] ((::RestFrames::CombinatoricGroup*)p);
   }
   static void destruct_RestFramescLcLCombinatoricGroup(void *p) {
      typedef ::RestFrames::CombinatoricGroup current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::CombinatoricGroup

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLJigsaw(void *p) {
      delete ((::RestFrames::Jigsaw*)p);
   }
   static void deleteArray_RestFramescLcLJigsaw(void *p) {
      delete [] ((::RestFrames::Jigsaw*)p);
   }
   static void destruct_RestFramescLcLJigsaw(void *p) {
      typedef ::RestFrames::Jigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::Jigsaw

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLInvisibleJigsaw(void *p) {
      delete ((::RestFrames::InvisibleJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLInvisibleJigsaw(void *p) {
      delete [] ((::RestFrames::InvisibleJigsaw*)p);
   }
   static void destruct_RestFramescLcLInvisibleJigsaw(void *p) {
      typedef ::RestFrames::InvisibleJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::InvisibleJigsaw

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLSetMassInvJigsaw(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::SetMassInvJigsaw : new ::RestFrames::SetMassInvJigsaw;
   }
   static void *newArray_RestFramescLcLSetMassInvJigsaw(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::SetMassInvJigsaw[nElements] : new ::RestFrames::SetMassInvJigsaw[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLSetMassInvJigsaw(void *p) {
      delete ((::RestFrames::SetMassInvJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLSetMassInvJigsaw(void *p) {
      delete [] ((::RestFrames::SetMassInvJigsaw*)p);
   }
   static void destruct_RestFramescLcLSetMassInvJigsaw(void *p) {
      typedef ::RestFrames::SetMassInvJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::SetMassInvJigsaw

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLSetRapidityInvJigsaw(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::SetRapidityInvJigsaw : new ::RestFrames::SetRapidityInvJigsaw;
   }
   static void *newArray_RestFramescLcLSetRapidityInvJigsaw(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::SetRapidityInvJigsaw[nElements] : new ::RestFrames::SetRapidityInvJigsaw[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLSetRapidityInvJigsaw(void *p) {
      delete ((::RestFrames::SetRapidityInvJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLSetRapidityInvJigsaw(void *p) {
      delete [] ((::RestFrames::SetRapidityInvJigsaw*)p);
   }
   static void destruct_RestFramescLcLSetRapidityInvJigsaw(void *p) {
      typedef ::RestFrames::SetRapidityInvJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::SetRapidityInvJigsaw

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLContraBoostInvJigsaw(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ContraBoostInvJigsaw : new ::RestFrames::ContraBoostInvJigsaw;
   }
   static void *newArray_RestFramescLcLContraBoostInvJigsaw(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::ContraBoostInvJigsaw[nElements] : new ::RestFrames::ContraBoostInvJigsaw[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLContraBoostInvJigsaw(void *p) {
      delete ((::RestFrames::ContraBoostInvJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLContraBoostInvJigsaw(void *p) {
      delete [] ((::RestFrames::ContraBoostInvJigsaw*)p);
   }
   static void destruct_RestFramescLcLContraBoostInvJigsaw(void *p) {
      typedef ::RestFrames::ContraBoostInvJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::ContraBoostInvJigsaw

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLCombinatoricJigsaw(void *p) {
      delete ((::RestFrames::CombinatoricJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLCombinatoricJigsaw(void *p) {
      delete [] ((::RestFrames::CombinatoricJigsaw*)p);
   }
   static void destruct_RestFramescLcLCombinatoricJigsaw(void *p) {
      typedef ::RestFrames::CombinatoricJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::CombinatoricJigsaw

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLMinMassesCombJigsaw(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::MinMassesCombJigsaw : new ::RestFrames::MinMassesCombJigsaw;
   }
   static void *newArray_RestFramescLcLMinMassesCombJigsaw(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::MinMassesCombJigsaw[nElements] : new ::RestFrames::MinMassesCombJigsaw[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLMinMassesCombJigsaw(void *p) {
      delete ((::RestFrames::MinMassesCombJigsaw*)p);
   }
   static void deleteArray_RestFramescLcLMinMassesCombJigsaw(void *p) {
      delete [] ((::RestFrames::MinMassesCombJigsaw*)p);
   }
   static void destruct_RestFramescLcLMinMassesCombJigsaw(void *p) {
      typedef ::RestFrames::MinMassesCombJigsaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::MinMassesCombJigsaw

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLState(void *p) {
      delete ((::RestFrames::State*)p);
   }
   static void deleteArray_RestFramescLcLState(void *p) {
      delete [] ((::RestFrames::State*)p);
   }
   static void destruct_RestFramescLcLState(void *p) {
      typedef ::RestFrames::State current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::State

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLVisibleState(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleState : new ::RestFrames::VisibleState;
   }
   static void *newArray_RestFramescLcLVisibleState(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::VisibleState[nElements] : new ::RestFrames::VisibleState[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLVisibleState(void *p) {
      delete ((::RestFrames::VisibleState*)p);
   }
   static void deleteArray_RestFramescLcLVisibleState(void *p) {
      delete [] ((::RestFrames::VisibleState*)p);
   }
   static void destruct_RestFramescLcLVisibleState(void *p) {
      typedef ::RestFrames::VisibleState current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::VisibleState

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLInvisibleState(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::InvisibleState : new ::RestFrames::InvisibleState;
   }
   static void *newArray_RestFramescLcLInvisibleState(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::InvisibleState[nElements] : new ::RestFrames::InvisibleState[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLInvisibleState(void *p) {
      delete ((::RestFrames::InvisibleState*)p);
   }
   static void deleteArray_RestFramescLcLInvisibleState(void *p) {
      delete [] ((::RestFrames::InvisibleState*)p);
   }
   static void destruct_RestFramescLcLInvisibleState(void *p) {
      typedef ::RestFrames::InvisibleState current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::InvisibleState

namespace ROOT {
   // Wrappers around operator new
   static void *new_RestFramescLcLCombinatoricState(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::CombinatoricState : new ::RestFrames::CombinatoricState;
   }
   static void *newArray_RestFramescLcLCombinatoricState(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RestFrames::CombinatoricState[nElements] : new ::RestFrames::CombinatoricState[nElements];
   }
   // Wrapper around operator delete
   static void delete_RestFramescLcLCombinatoricState(void *p) {
      delete ((::RestFrames::CombinatoricState*)p);
   }
   static void deleteArray_RestFramescLcLCombinatoricState(void *p) {
      delete [] ((::RestFrames::CombinatoricState*)p);
   }
   static void destruct_RestFramescLcLCombinatoricState(void *p) {
      typedef ::RestFrames::CombinatoricState current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::CombinatoricState

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLTreePlot(void *p) {
      delete ((::RestFrames::TreePlot*)p);
   }
   static void deleteArray_RestFramescLcLTreePlot(void *p) {
      delete [] ((::RestFrames::TreePlot*)p);
   }
   static void destruct_RestFramescLcLTreePlot(void *p) {
      typedef ::RestFrames::TreePlot current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::TreePlot

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLHistPlot(void *p) {
      delete ((::RestFrames::HistPlot*)p);
   }
   static void deleteArray_RestFramescLcLHistPlot(void *p) {
      delete [] ((::RestFrames::HistPlot*)p);
   }
   static void destruct_RestFramescLcLHistPlot(void *p) {
      typedef ::RestFrames::HistPlot current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::HistPlot

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RestFramescLcLHistPlotVar(void *p) {
      delete ((::RestFrames::HistPlotVar*)p);
   }
   static void deleteArray_RestFramescLcLHistPlotVar(void *p) {
      delete [] ((::RestFrames::HistPlotVar*)p);
   }
   static void destruct_RestFramescLcLHistPlotVar(void *p) {
      typedef ::RestFrames::HistPlotVar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RestFrames::HistPlotVar

namespace {
  void TriggerDictionaryInitialization_RestFrames_Dict_Impl() {
    static const char* headers[] = {
"../inc/RestFrames/CombinatoricGroup.hh",
"../inc/RestFrames/CombinatoricJigsaw.hh",
"../inc/RestFrames/CombinatoricState.hh",
"../inc/RestFrames/ContraBoostInvJigsaw.hh",
"../inc/RestFrames/DecayFrame.hh",
"../inc/RestFrames/DecayGenFrame.hh",
"../inc/RestFrames/DecayRecoFrame.hh",
"../inc/RestFrames/GeneratorFrame.hh",
"../inc/RestFrames/Group.hh",
"../inc/RestFrames/HistPlot.hh",
"../inc/RestFrames/HistPlotVar.hh",
"../inc/RestFrames/InvisibleFrame.hh",
"../inc/RestFrames/InvisibleGenFrame.hh",
"../inc/RestFrames/InvisibleGroup.hh",
"../inc/RestFrames/InvisibleJigsaw.hh",
"../inc/RestFrames/InvisibleRecoFrame.hh",
"../inc/RestFrames/InvisibleState.hh",
"../inc/RestFrames/Jigsaw.hh",
"../inc/RestFrames/LabFrame.hh",
"../inc/RestFrames/LabGenFrame.hh",
"../inc/RestFrames/LabRecoFrame.hh",
"../inc/RestFrames/MinMassesCombJigsaw.hh",
"../inc/RestFrames/ReconstructionFrame.hh",
"../inc/RestFrames/ResonanceGenFrame.hh",
"../inc/RestFrames/RestFrame.hh",
"../inc/RestFrames/RestFrames.hh",
"../inc/RestFrames/RFBase.hh",
"../inc/RestFrames/RFKey.hh",
"../inc/RestFrames/RFList.hh",
"../inc/RestFrames/RFLog.hh",
"../inc/RestFrames/RFPlot.hh",
"../inc/RestFrames/SelfAssemblingRecoFrame.hh",
"../inc/RestFrames/SetMassInvJigsaw.hh",
"../inc/RestFrames/SetRapidityInvJigsaw.hh",
"../inc/RestFrames/State.hh",
"../inc/RestFrames/TreePlot.hh",
"../inc/RestFrames/TreePlotLink.hh",
"../inc/RestFrames/TreePlotNode.hh",
"../inc/RestFrames/VisibleFrame.hh",
"../inc/RestFrames/VisibleGenFrame.hh",
"../inc/RestFrames/VisibleRecoFrame.hh",
"../inc/RestFrames/VisibleState.hh",
0
    };
    static const char* includePaths[] = {
"../inc",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/src/RestFrames/src/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  RFBase;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  RFKey;}
namespace RestFrames{template <class T> class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  RFList;
}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  RestFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  ReconstructionFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  GeneratorFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  ResonanceGenFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  Jigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  Group;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  State;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  VisibleState;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  InvisibleState;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  CombinatoricState;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  RFLog;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  LabGenFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  DecayGenFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  InvisibleGenFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  VisibleGenFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  LabRecoFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  DecayRecoFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  InvisibleRecoFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  VisibleRecoFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  SelfAssemblingRecoFrame;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  InvisibleGroup;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  CombinatoricGroup;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  InvisibleJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  SetMassInvJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  SetRapidityInvJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  ContraBoostInvJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  CombinatoricJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  MinMassesCombJigsaw;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  TreePlot;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  HistPlot;}
namespace RestFrames{class __attribute__((annotate("$clingAutoload$../inc/RestFrames/RestFrames_LinkDef.h")))  HistPlotVar;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "../inc/RestFrames/CombinatoricGroup.hh"
#include "../inc/RestFrames/CombinatoricJigsaw.hh"
#include "../inc/RestFrames/CombinatoricState.hh"
#include "../inc/RestFrames/ContraBoostInvJigsaw.hh"
#include "../inc/RestFrames/DecayFrame.hh"
#include "../inc/RestFrames/DecayGenFrame.hh"
#include "../inc/RestFrames/DecayRecoFrame.hh"
#include "../inc/RestFrames/GeneratorFrame.hh"
#include "../inc/RestFrames/Group.hh"
#include "../inc/RestFrames/HistPlot.hh"
#include "../inc/RestFrames/HistPlotVar.hh"
#include "../inc/RestFrames/InvisibleFrame.hh"
#include "../inc/RestFrames/InvisibleGenFrame.hh"
#include "../inc/RestFrames/InvisibleGroup.hh"
#include "../inc/RestFrames/InvisibleJigsaw.hh"
#include "../inc/RestFrames/InvisibleRecoFrame.hh"
#include "../inc/RestFrames/InvisibleState.hh"
#include "../inc/RestFrames/Jigsaw.hh"
#include "../inc/RestFrames/LabFrame.hh"
#include "../inc/RestFrames/LabGenFrame.hh"
#include "../inc/RestFrames/LabRecoFrame.hh"
#include "../inc/RestFrames/MinMassesCombJigsaw.hh"
#include "../inc/RestFrames/ReconstructionFrame.hh"
#include "../inc/RestFrames/ResonanceGenFrame.hh"
#include "../inc/RestFrames/RestFrame.hh"
#include "../inc/RestFrames/RestFrames.hh"
#include "../inc/RestFrames/RFBase.hh"
#include "../inc/RestFrames/RFKey.hh"
#include "../inc/RestFrames/RFList.hh"
#include "../inc/RestFrames/RFLog.hh"
#include "../inc/RestFrames/RFPlot.hh"
#include "../inc/RestFrames/SelfAssemblingRecoFrame.hh"
#include "../inc/RestFrames/SetMassInvJigsaw.hh"
#include "../inc/RestFrames/SetRapidityInvJigsaw.hh"
#include "../inc/RestFrames/State.hh"
#include "../inc/RestFrames/TreePlot.hh"
#include "../inc/RestFrames/TreePlotLink.hh"
#include "../inc/RestFrames/TreePlotNode.hh"
#include "../inc/RestFrames/VisibleFrame.hh"
#include "../inc/RestFrames/VisibleGenFrame.hh"
#include "../inc/RestFrames/VisibleRecoFrame.hh"
#include "../inc/RestFrames/VisibleState.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CombinatoricGroup", payloadCode, "@",
"CombinatoricJigsaw", payloadCode, "@",
"CombinatoricState", payloadCode, "@",
"ContraBoostInvJigsaw", payloadCode, "@",
"DecayGenFrame", payloadCode, "@",
"DecayRecoFrame", payloadCode, "@",
"GeneratorFrame", payloadCode, "@",
"Group", payloadCode, "@",
"HistPlot", payloadCode, "@",
"HistPlotVar", payloadCode, "@",
"InvisibleGenFrame", payloadCode, "@",
"InvisibleGroup", payloadCode, "@",
"InvisibleJigsaw", payloadCode, "@",
"InvisibleRecoFrame", payloadCode, "@",
"InvisibleState", payloadCode, "@",
"Jigsaw", payloadCode, "@",
"LabGenFrame", payloadCode, "@",
"LabRecoFrame", payloadCode, "@",
"MinMassesCombJigsaw", payloadCode, "@",
"RFBase", payloadCode, "@",
"RFKey", payloadCode, "@",
"RFList<CombinatoricState>", payloadCode, "@",
"RFList<GeneratorFrame>", payloadCode, "@",
"RFList<Group>", payloadCode, "@",
"RFList<InvisibleState>", payloadCode, "@",
"RFList<Jigsaw>", payloadCode, "@",
"RFList<RFBase>", payloadCode, "@",
"RFList<ReconstructionFrame>", payloadCode, "@",
"RFList<ResonanceGenFrame>", payloadCode, "@",
"RFList<RestFrame>", payloadCode, "@",
"RFList<State>", payloadCode, "@",
"RFList<VisibleState>", payloadCode, "@",
"RFList<const RestFrame>", payloadCode, "@",
"RFLog", payloadCode, "@",
"ReconstructionFrame", payloadCode, "@",
"ResonanceGenFrame", payloadCode, "@",
"RestFrame", payloadCode, "@",
"RestFrames::CombinatoricGroup", payloadCode, "@",
"RestFrames::CombinatoricJigsaw", payloadCode, "@",
"RestFrames::CombinatoricState", payloadCode, "@",
"RestFrames::ContraBoostInvJigsaw", payloadCode, "@",
"RestFrames::DecayGenFrame", payloadCode, "@",
"RestFrames::DecayRecoFrame", payloadCode, "@",
"RestFrames::GeneratorFrame", payloadCode, "@",
"RestFrames::Group", payloadCode, "@",
"RestFrames::HistPlot", payloadCode, "@",
"RestFrames::HistPlotVar", payloadCode, "@",
"RestFrames::InvisibleGenFrame", payloadCode, "@",
"RestFrames::InvisibleGroup", payloadCode, "@",
"RestFrames::InvisibleJigsaw", payloadCode, "@",
"RestFrames::InvisibleRecoFrame", payloadCode, "@",
"RestFrames::InvisibleState", payloadCode, "@",
"RestFrames::Jigsaw", payloadCode, "@",
"RestFrames::LabGenFrame", payloadCode, "@",
"RestFrames::LabRecoFrame", payloadCode, "@",
"RestFrames::MinMassesCombJigsaw", payloadCode, "@",
"RestFrames::RFBase", payloadCode, "@",
"RestFrames::RFKey", payloadCode, "@",
"RestFrames::RFList<RestFrames::CombinatoricState>", payloadCode, "@",
"RestFrames::RFList<RestFrames::GeneratorFrame>", payloadCode, "@",
"RestFrames::RFList<RestFrames::Group>", payloadCode, "@",
"RestFrames::RFList<RestFrames::InvisibleState>", payloadCode, "@",
"RestFrames::RFList<RestFrames::Jigsaw>", payloadCode, "@",
"RestFrames::RFList<RestFrames::RFBase>", payloadCode, "@",
"RestFrames::RFList<RestFrames::ReconstructionFrame>", payloadCode, "@",
"RestFrames::RFList<RestFrames::ResonanceGenFrame>", payloadCode, "@",
"RestFrames::RFList<RestFrames::RestFrame>", payloadCode, "@",
"RestFrames::RFList<RestFrames::State>", payloadCode, "@",
"RestFrames::RFList<RestFrames::VisibleState>", payloadCode, "@",
"RestFrames::RFList<const RestFrames::RestFrame>", payloadCode, "@",
"RestFrames::RFLog", payloadCode, "@",
"RestFrames::ReconstructionFrame", payloadCode, "@",
"RestFrames::ResonanceGenFrame", payloadCode, "@",
"RestFrames::RestFrame", payloadCode, "@",
"RestFrames::SelfAssemblingRecoFrame", payloadCode, "@",
"RestFrames::SetMassInvJigsaw", payloadCode, "@",
"RestFrames::SetRapidityInvJigsaw", payloadCode, "@",
"RestFrames::State", payloadCode, "@",
"RestFrames::TreePlot", payloadCode, "@",
"RestFrames::VisibleGenFrame", payloadCode, "@",
"RestFrames::VisibleRecoFrame", payloadCode, "@",
"RestFrames::VisibleState", payloadCode, "@",
"SelfAssemblingRecoFrame", payloadCode, "@",
"SetMassInvJigsaw", payloadCode, "@",
"SetRapidityInvJigsaw", payloadCode, "@",
"State", payloadCode, "@",
"TreePlot", payloadCode, "@",
"VisibleGenFrame", payloadCode, "@",
"VisibleRecoFrame", payloadCode, "@",
"VisibleState", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RestFrames_Dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RestFrames_Dict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RestFrames_Dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RestFrames_Dict() {
  TriggerDictionaryInitialization_RestFrames_Dict_Impl();
}
