# Source this script to set the environmental variables for this RestFrames installation
# 
# Users can automatically call this script at their shell login by adding to their
# .bashrc/.profile/.zlogin etc. file:
#    . $(restframes-config --prefix)/libexec/setup_RestFrames.sh
# assuming restframes-config is in their PATH, or
#    . PATH_OF_RESTFRAMES/libexec/setup_RestFrames.sh
#
# This script if for bash-like shells, see setup_RestFrames.csh for csh-like shells.

drop_from_path()
{
   # Assert that we got enough arguments
   if test $# -ne 2 ; then
      echo "drop_from_path: needs 2 arguments"
      return 1
   fi

   p=$1
   drop=$2

   newpath=`echo $p | sed -e "s;:${drop}:;:;g" \
                          -e "s;:${drop};;g"   \
                          -e "s;${drop}:;;g"   \
                          -e "s;${drop};;g"`
}

if [ -n "${RESTFRAMESSYS}" ] ; then
   old_restframessys=${RESTFRAMESSYS}
fi

RESTFRAMESSYS=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local; export RESTFRAMESSYS

if [ -n "${old_restframessys}" ] ; then
   if [ -n "${PATH}" ]; then
      drop_from_path "$PATH" ${old_restframessys}/bin
      PATH=$newpath
   fi
   if [ -n "${LD_LIBRARY_PATH}" ]; then
      drop_from_path $LD_LIBRARY_PATH ${old_restframessys}/lib
      LD_LIBRARY_PATH=$newpath
   fi
   if [ -n "${DYLD_LIBRARY_PATH}" ]; then
      drop_from_path $DYLD_LIBRARY_PATH ${old_restframessys}/lib
      DYLD_LIBRARY_PATH=$newpath
   fi
   if [ -n "${SHLIB_PATH}" ]; then
      drop_from_path $SHLIB_PATH ${old_restframessys}/lib
      SHLIB_PATH=$newpath
   fi
   if [ -n "${LIBPATH}" ]; then
      drop_from_path $LIBPATH ${old_restframessys}/lib
      LIBPATH=$newpath
   fi
   if [ -n "${PYTHONPATH}" ]; then
      drop_from_path $PYTHONPATH ${old_restframessys}/lib
      PYTHONPATH=$newpath
   fi
   if [ -n "${CPLUS_INCLUDE_PATH}" ]; then
      drop_from_path $PYTHONPATH ${old_restframessys}/include
      CPLUS_INCLUDE_PATH=$newpath
   fi
fi

if [ -z "${PATH}" ]; then
   PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/bin; export PATH
else
   PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/bin:$PATH; export PATH
fi

if [ -z "${LD_LIBRARY_PATH}" ]; then
   LD_LIBRARY_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib; export LD_LIBRARY_PATH       # Linux, ELF HP-UX
else
   LD_LIBRARY_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib:$LD_LIBRARY_PATH; export LD_LIBRARY_PATH
fi

if [ -z "${DYLD_LIBRARY_PATH}" ]; then
   DYLD_LIBRARY_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib; export DYLD_LIBRARY_PATH   # Mac OS X
else
   DYLD_LIBRARY_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib:$DYLD_LIBRARY_PATH; export DYLD_LIBRARY_PATH
fi

if [ -z "${SHLIB_PATH}" ]; then
   SHLIB_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib; export SHLIB_PATH                 # legacy HP-UX
else
   SHLIB_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib:$SHLIB_PATH; export SHLIB_PATH
fi

if [ -z "${LIBPATH}" ]; then
   LIBPATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib; export LIBPATH                       # AIX
else
   LIBPATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib:$LIBPATH; export LIBPATH
fi

if [ -z "${PYTHONPATH}" ]; then
   PYTHONPATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib; export PYTHONPATH
else
   PYTHONPATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/lib:$PYTHONPATH; export PYTHONPATH
fi

if [ -z "${CPLUS_INCLUDE_PATH}" ]; then
   CPLUS_INCLUDE_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/include; export CPLUS_INCLUDE_PATH
else
   CPLUS_INCLUDE_PATH=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local/include:$CPLUS_INCLUDE_PATH; export CPLUS_INCLUDE_PATH
fi

unset old_restframessys

