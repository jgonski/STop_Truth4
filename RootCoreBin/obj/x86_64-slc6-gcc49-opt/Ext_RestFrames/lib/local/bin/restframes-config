#!/bin/bash
#
# restframes-config.  Generated from restframes-config.in by configure.
#
# This is the base script for retrieving all information
# regarding compiling and linking programs using RestFrames.
# Run ./restframes-config without arguments for usage details
#########################################################

# print a usage message and exit
# exit code passed as argument:
#   0 if it is a normal call
#   1 if it is due to a misusage.
usage()
{
  cat 1>&2 <<EOF

This is RestFrames v1.0.0 configuration tool.
Usage:
  restframes-config [--help] [--version] [--prefix] [--config] [--cxxflags] [--libs]

The arguments are:

  --help       prints this message and exits
  --version    prints RestFrames version and exits
  --prefix     prints the RestFrames installation directory
  --config     prints a summary of how RestFrames was configured
  --cxxflags   returns the compilation flags to be used with C++ programs
  --libs       returns the flags to pass to the linker

EOF
}
# first deal with the case where no argument is passed
[ $# -gt 0 ] || usage 1

# tools to parse options
########################

# option_name _string
# Returns NAME if _string is of the form: --NAME[=...]
option_name()
{
    echo "$1" | sed 's/^--//;s/=.*//' | tr '-' '_'
}

# option_value _string
# Returns FOO if _string is of the form: --option=FOO
option_value()
{
    echo "$1" | sed 's/^[^=]*=//'
}

# is_in_list _arg _arg_list
# return true if the argument _arg is in the list _arg_list
# and false otherwise
is_in_list()
{
    arg_match="$1"
    shift
    for arg_match_i do
        [ "x$arg_match_i" != "x$arg_match" ] || return 0
    done
    false
}


# useful utilities
##################

# wite error messages and exit
write_error()
{
    echo "Error: $1"
    echo "Use restframes-config --help for more information"
    exit 1
}


# browse the argument list
# This is done the following way:
#  - at first pass, we check if the --help argument is set. If yes, 
#    print usage and exit.
#    we also and make sure that there is no interference between the
#    arguments (e.g. --cxxflags --libs is wrong)
#  - we then check for extra arguments and return the requested info
#####################################################################
# useful lists of arguments
arg_query_list="version prefix help config" 

# no query found initially
found_query="no"
found_cxxflags="no"
found_libs="no"

# browse arguments
for arg do
    case "$arg" in
	--help|-h)
	    usage 0
	    ;;
	--cxxflags)
	    # we've found a query, make sure we don't already have one
	    # except if it is --libs
	    if [[ "x$found_query" != "xno" && "x$found_query" != "xlibs" ]]; then
		write_error "--cxxflags cannot be used with --$found_query"
	    fi

	    # update found_query 
	    # note: don't overwrite if libs are already asked for
	    found_cxxflags="yes"
	    if [ "x$found_query" != "xlibs" ]; then
		found_query="cxxflags"
	    fi	    
	    ;;
	--libs)
	    # we've found a query, make sure we don't already have one
	    # except if it is --cxxflags
	    if [[ "x$found_query" != "xno" && "x$found_query" != "xcxxflags" ]]; then
		write_error "--libs cannot be used with --$found_query"
	    fi

	    # update found_query 
	    found_libs="yes"
	    found_query="libs"
	    ;;
	--*)
	    arg_name=`option_name $arg`
	    if is_in_list $arg_name $arg_query_list ; then
		# we've found a query, make sure we don't already have one
		if [ "x$found_cxxflags" != "xno" ] ; then
		    write_error "--$arg_name cannot be used with --cxxflags"
		fi
		if [ "x$found_libs" != "xno" ] ; then
		    write_error "--$arg_name cannot be used with --libs"
		fi
		if [ "x$found_query" != "xno" ] ; then
		    write_error "Too many arguements to restframes-config"
		fi
		found_query="$arg_name"
	    else
		write_error "$arg: unrecognised argument"
	    fi
	    ;;
	*)
	    write_error "$arg is not a valid argument"
	    ;;
    esac
done


# now deal with the output
case $found_query in
    no)
	write_error "you must at least specify one argument to restframes-config"
	;;
    version)
	echo 1.0.0
	;;
    prefix)
	echo /afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local
	;;
    cxxflags)
	echo -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local${prefix}/include 
	;;
    libs)
	libs_string=" -lm -L/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local${exec_prefix}/lib -lRestFrames"
	if [ "x$found_cxxflags" = "xyes" ] ; then
	    echo -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local${prefix}/include  $libs_string
	else
	    echo $libs_string
	fi
	;;
    config)
	echo "This is RestFrames version 1.0.0"
	echo ""
	echo "Configuration invocation was"
	echo ""
	echo "  ./configure  '--prefix=/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/obj/x86_64-slc6-gcc49-opt/Ext_RestFrames/lib/local' '--enable-silent-rules' 'CXX=g++' 'CXXFLAGS=-I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames/Root -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames -O2 -Wall -fPIC -pthread -std=c++11 -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/include -g -Wno-tautological-undefined-compare -pthread -std=c++11 -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -DROOTCORE_RELEASE_SERIES=24 -DROOTCORE_TEST_FILE="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DROOTCORE_TEST_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725" -DASG_TEST_FILE_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1" -DASG_TEST_FILE_MC="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DASG_TEST_FILE_MCAFII=""' 'LDFLAGS=-m64 -L/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/lib -lCore -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic ' 'CC=gcc' 'CFLAGS=-I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames/Root -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames -O2 -Wall -fPIC -pthread -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/include -g -Wno-tautological-undefined-compare -pthread -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -DROOTCORE_RELEASE_SERIES=24 -DROOTCORE_TEST_FILE="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DROOTCORE_TEST_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725" -DASG_TEST_FILE_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1" -DASG_TEST_FILE_MC="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DASG_TEST_FILE_MCAFII=""' 'CXXCPP=g++ -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames/Root -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/Ext_RestFrames -O2 -Wall -fPIC -pthread -std=c++11 -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -I/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/include -g -Wno-tautological-undefined-compare -pthread -std=c++11 -Wno-deprecated-declarations -m64 -I/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include -DROOTCORE_RELEASE_SERIES=24 -DROOTCORE_TEST_FILE="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DROOTCORE_TEST_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725" -DASG_TEST_FILE_DATA="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1" -DASG_TEST_FILE_MC="/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1" -DASG_TEST_FILE_MCAFII="" -E'"
	echo ""
	;;
esac
