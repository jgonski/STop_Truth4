#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIjdIjgonskidIHarvarddIhiggsinosdISTop_Truth4dIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdIMyAnalysisdIobjdIMyAnalysisCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "MyAnalysis/TruthxAODAnalysis_Higgsinos.h"
#include "MyAnalysis/TruthxAODAnalysis_ttbar.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p = 0);
   static void *newArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(Long_t size, void *p);
   static void delete_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p);
   static void deleteArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p);
   static void destruct_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)
   {
      ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos", ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos::Class_Version(), "MyAnalysis/TruthxAODAnalysis_Higgsinos.h", 60,
                  typeid(::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos), DefineBehavior(ptr, ptr),
                  &::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos::Dictionary, isa_proxy, 4,
                  sizeof(::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos) );
      instance.SetNew(&new_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos);
      instance.SetNewArray(&newArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos);
      instance.SetDelete(&delete_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos);
      instance.SetDeleteArray(&deleteArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos);
      instance.SetDestructor(&destruct_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)
   {
      return GenerateInitInstanceLocal((::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p = 0);
   static void *newArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(Long_t size, void *p);
   static void delete_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p);
   static void deleteArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p);
   static void destruct_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)
   {
      ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MyAnalysis_ttbar::TruthxAODAnalysis_ttbar", ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar::Class_Version(), "MyAnalysis/TruthxAODAnalysis_ttbar.h", 60,
                  typeid(::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar), DefineBehavior(ptr, ptr),
                  &::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar::Dictionary, isa_proxy, 4,
                  sizeof(::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar) );
      instance.SetNew(&new_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar);
      instance.SetNewArray(&newArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar);
      instance.SetDelete(&delete_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar);
      instance.SetDeleteArray(&deleteArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar);
      instance.SetDestructor(&destruct_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)
   {
      return GenerateInitInstanceLocal((::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace MyAnalysis_Higgsinos {
//______________________________________________________________________________
atomic_TClass_ptr TruthxAODAnalysis_Higgsinos::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TruthxAODAnalysis_Higgsinos::Class_Name()
{
   return "MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos";
}

//______________________________________________________________________________
const char *TruthxAODAnalysis_Higgsinos::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TruthxAODAnalysis_Higgsinos::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TruthxAODAnalysis_Higgsinos::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TruthxAODAnalysis_Higgsinos::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace MyAnalysis_Higgsinos
namespace MyAnalysis_ttbar {
//______________________________________________________________________________
atomic_TClass_ptr TruthxAODAnalysis_ttbar::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TruthxAODAnalysis_ttbar::Class_Name()
{
   return "MyAnalysis_ttbar::TruthxAODAnalysis_ttbar";
}

//______________________________________________________________________________
const char *TruthxAODAnalysis_ttbar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TruthxAODAnalysis_ttbar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TruthxAODAnalysis_ttbar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TruthxAODAnalysis_ttbar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace MyAnalysis_ttbar
namespace MyAnalysis_Higgsinos {
//______________________________________________________________________________
void TruthxAODAnalysis_Higgsinos::Streamer(TBuffer &R__b)
{
   // Stream an object of class MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos::Class(),this);
   } else {
      R__b.WriteClassBuffer(MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos::Class(),this);
   }
}

} // namespace MyAnalysis_Higgsinos
namespace ROOT {
   // Wrappers around operator new
   static void *new_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p) {
      return  p ? new(p) ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos : new ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos;
   }
   static void *newArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(Long_t nElements, void *p) {
      return p ? new(p) ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos[nElements] : new ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos[nElements];
   }
   // Wrapper around operator delete
   static void delete_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p) {
      delete ((::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)p);
   }
   static void deleteArray_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p) {
      delete [] ((::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos*)p);
   }
   static void destruct_MyAnalysis_HiggsinoscLcLTruthxAODAnalysis_Higgsinos(void *p) {
      typedef ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos

namespace MyAnalysis_ttbar {
//______________________________________________________________________________
void TruthxAODAnalysis_ttbar::Streamer(TBuffer &R__b)
{
   // Stream an object of class MyAnalysis_ttbar::TruthxAODAnalysis_ttbar.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MyAnalysis_ttbar::TruthxAODAnalysis_ttbar::Class(),this);
   } else {
      R__b.WriteClassBuffer(MyAnalysis_ttbar::TruthxAODAnalysis_ttbar::Class(),this);
   }
}

} // namespace MyAnalysis_ttbar
namespace ROOT {
   // Wrappers around operator new
   static void *new_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p) {
      return  p ? new(p) ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar : new ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar;
   }
   static void *newArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(Long_t nElements, void *p) {
      return p ? new(p) ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar[nElements] : new ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar[nElements];
   }
   // Wrapper around operator delete
   static void delete_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p) {
      delete ((::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)p);
   }
   static void deleteArray_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p) {
      delete [] ((::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar*)p);
   }
   static void destruct_MyAnalysis_ttbarcLcLTruthxAODAnalysis_ttbar(void *p) {
      typedef ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MyAnalysis_ttbar::TruthxAODAnalysis_ttbar

namespace {
  void TriggerDictionaryInitialization_MyAnalysisCINT_Impl() {
    static const char* headers[] = {
"MyAnalysis/TruthxAODAnalysis_Higgsinos.h",
"MyAnalysis/TruthxAODAnalysis_ttbar.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/MyAnalysis/Root",
"/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/MyAnalysis",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/MyAnalysis/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace MyAnalysis_Higgsinos{class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/MyAnalysis/Root/LinkDef.h")))  TruthxAODAnalysis_Higgsinos;}
namespace MyAnalysis_ttbar{class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/j/jgonski/Harvard/higgsinos/STop_Truth4/MyAnalysis/Root/LinkDef.h")))  TruthxAODAnalysis_ttbar;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "MyAnalysis"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "MyAnalysis/TruthxAODAnalysis_Higgsinos.h"
#include "MyAnalysis/TruthxAODAnalysis_ttbar.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos", payloadCode, "@",
"MyAnalysis_ttbar::TruthxAODAnalysis_ttbar", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MyAnalysisCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MyAnalysisCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MyAnalysisCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MyAnalysisCINT() {
  TriggerDictionaryInitialization_MyAnalysisCINT_Impl();
}
