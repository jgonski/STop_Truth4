#ifndef MyAnalysis_TruthxAODAnalysis_STop_H
#define MyAnalysis_TruthxAODAnalysis_STop_H

#pragma once
#include <RootCore/Packages.h>
#ifdef ROOTCORE_PACKAGE_Ext_RestFrames
#include "RestFrames/RestFrames.hh"
using namespace RestFrames;
#endif

//#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <xAODBase/IParticleContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include "xAODJet/JetAuxContainer.h"
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
//#include "xAODTruth/TruthJetContainer.h"

#include <xAODRootAccess/TStore.h>
#include <xAODRootAccess/TEvent.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <StopPolarization/PolarizationReweight.h>
#include <MyAnalysis/NumericReweight.h>

#include <RestFrames/RestFrames.hh>
#include <EventLoop/Algorithm.h>

#include <TRandom3.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TF2.h>
#include <TF3.h>
#include <TPostScript.h>
#include <TArrow.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TFile.h>

#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

namespace MyAnalysis_STop {

class TruthxAODAnalysis_STop : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the )
public:

  //-- Tree         
  TTree *tree; //!  
  
  const xAOD::MissingETContainer* truthMetContainer; //!
  vector<TLorentzVector> *m_truthJets; //!
  vector<bool> *m_truthJetIsBTagged; //!
  const xAOD::JetContainer* truthJets; //!

  // compressed variables
  double m_PTISR; //!
  double m_RISR; //!
  double m_cosS; //!
  double m_MS; //!
  double m_MISR; //!
  double m_MV; //!
  double m_dphiCMI; //!
  double m_dphiISRI; //!
  double m_pTjV1; //!
  double m_pTjV2; //!
  double m_pTjV3; //!
  double m_pTjV4; //!
  double m_pTjV5; //!
  double m_pTjV6; //!
  double m_pTbV1; //!
  double m_pTbV2; //!
  int m_NbV; //!
  int m_NbISR; //!
  int m_NjV; //!
  int m_NjISR; //!

  double m_stop1_pt ;
  double m_stop1_b_pt ;
  double m_stop1_w1_pt ;
  double m_stop1_w2_pt ;
  double m_stop1_xi_pt ;
  double m_stop1_eta ;
  double m_stop1_b_eta ;
  double m_stop1_w1_eta ;
  double m_stop1_w2_eta ;
  double m_stop1_xi_eta ;
  double m_stop1_phi ;
  double m_stop1_b_phi ;
  double m_stop1_w1_phi ;
  double m_stop1_w2_phi ;
  double m_stop1_xi_phi ;
  double m_stop1_m ;
  double m_stop1_b_m ;
  double m_stop1_w1_m ;
  double m_stop1_w2_m ;
  double m_stop1_xi_m ;
  double m_stop2_pt ;
  double m_stop2_b_pt ;
  double m_stop2_w1_pt ;
  double m_stop2_w2_pt ;
  double m_stop2_xi_pt ;
  double m_stop2_eta ;
  double m_stop2_b_eta ;
  double m_stop2_w1_eta ;
  double m_stop2_w2_eta ;
  double m_stop2_xi_eta ;
  double m_stop2_phi ;
  double m_stop2_b_phi ;
  double m_stop2_w1_phi ;
  double m_stop2_w2_phi ;
  double m_stop2_xi_phi ;
  double m_stop2_m ;
  double m_stop2_b_m ;
  double m_stop2_w1_m ;
  double m_stop2_w2_m ;
  double m_stop2_xi_m ;
  double m_TotISR_pt ;
  double m_TotISR_eta ;
  double m_TotISR_phi ;
  double m_TotISR_m ;
  double m_TotISR_ptThrust ;
  double m_metTruth ;
  double m_stop1_pThrust ;
  double m_stop2_pThrust ;
  double m_distop_pt ;
  double m_distop_eta ;
  double m_distop_phi  ;
  double m_distop_m ;
  double m_distop_pThrust ;
  double m_dixi_pt ;
  double m_dixi_eta ;
  double m_dixi_phi ;
  double m_dixi_m ;
  double m_dixi_pThrust ;
 
  // RestFrames frames and friends
  LabRecoFrame*        LAB; //!
  DecayRecoFrame*      CM; //!
  DecayRecoFrame*      S; //!
  VisibleRecoFrame*    ISR; //!
  VisibleRecoFrame*    V; //!
  InvisibleRecoFrame*  I; //!
  InvisibleGroup*      INV; //!
  SetMassInvJigsaw*    InvMass; //!
  CombinatoricGroup*   VIS; //!
  MinMassesCombJigsaw* SplitVis; //!
 
  int m_eventCounter; //!
  int count; //!
  std::string evt_display_name; //!
  TPostScript *ps; //!

  TLorentzVector thrust_vec; //!  
  TLorentzVector thrust_perp_vec; //!

  TLorentzVector stopxi_tmp; //!
  TLorentzVector stopb_tmp; //!
  TLorentzVector stopwj1_tmp; //!
  TLorentzVector stopwj2_tmp; //!

  TLorentzVector ditop; //!
  TLorentzVector stop1_t; //!
  TLorentzVector stop2_t; //!

  TLorentzVector stop1; //!
  TLorentzVector stop1_b; //!
  TLorentzVector stop1_wj1; //!
  TLorentzVector stop1_wj2; //!
  TLorentzVector stop1_xi; //!
  TLorentzVector stop2; //!
  TLorentzVector stop2_b; //!
  TLorentzVector stop2_wj1; //!
  TLorentzVector stop2_wj2; //!
  TLorentzVector stop2_xi; //!

  TLorentzVector distop; //!
  TLorentzVector dixi; //!
  TLorentzVector stop1_jets; //!
  TLorentzVector stop2_jets; //!
  TLorentzVector stop1_w; //!
  TLorentzVector stop2_w; //!

  TLorentzVector ISR_total; //!

  TVector3 B1_3vec; //!
  TVector3 B2_3vec; //!

  TVector3 k_3vec_u; //!
  TVector3 B1_perp_k; //!

  TVector3 B1_cross_k; //!
  TVector3 T_Calc_Boost_Vect1; //!
  TVector3 T_Calc_Boost_Vect2; //!

  TLorentzVector BCand_TopFrame2_MinPThrust; //!
  TLorentzVector WCand_TopFrame2_MinPThrust; //!
  TLorentzVector WCand_j1_TopFrame2_MinPThrust; //!
  TLorentzVector WCand_j2_TopFrame2_MinPThrust; //!

  TLorentzVector BCand_TopFrame2_MaxPThrust; //!
  TLorentzVector WCand_TopFrame2_MaxPThrust; //!
  TLorentzVector WCand_j1_TopFrame2_MaxPThrust; //!
  TLorentzVector WCand_j2_TopFrame2_MaxPThrust; //!

  TLorentzVector WCand_TTFrame2_MinPThrust; //!
  TLorentzVector TCand_TTFrame2_MinPThrust; //!
  TLorentzVector WCand_TTFrame2_MaxPThrust; //!
  TLorentzVector TCand_TTFrame2_MaxPThrust; //!

  TLorentzVector WCand_LabFrame2_MinPThrust; //!
  TLorentzVector TCand_LabFrame2_MinPThrust; //!
  TLorentzVector WCand_LabFrame2_MaxPThrust; //!
  TLorentzVector TCand_LabFrame2_MaxPThrust; //!

  bool FoundMaxPThrustTop; //!
  bool FoundMinPThrustTop; //! 

  double w_polemass; //!
  double b_polemass; //!
  double t_polemass; //!
  double t_reduced_mass; //!
  double expected_b_momentum; //!

  TLorentzVector TTVect; //!
  std::vector<TLorentzVector> AllJets_Vec; //!
  std::vector<int> AllJets_Lost; //!

  std::vector <std::vector<float>> ISR_p; //!

  TLorentzVector MET_TLV; //!

  bool isMC; //!
  bool verbose; //!
  int nISR; //! 
  double GeV; //!
  int isStop1Had; //!
  int isStop2Had; //!
  bool plotEvent; //!

  // this is a standard constructor
  TruthxAODAnalysis_STop ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  //  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  int PlotTruth_SoftTop(TVector3 boost1, TVector3 boost2, double weight);
  void setEvtDisplay(std::string eps);
  void DrawEvtDisplay();
  void ResetVariables();
  void plot_signal(int icut, double weight);
  void SetVerbose(int i);
  bool FindSoftTop(int b_top_index, int b_othertop_index);
  void FindThrustDir();
  double CalculatePAlongThrust( double px, double py );
  double CalculatePAlongThrust( TLorentzVector *Vec );
  int Find_STopDecayProducts( const xAOD::TruthParticle* particle, 
			      TLorentzVector *Top_tmp,
			      TLorentzVector *Xi_tmp, TLorentzVector *B_tmp,
			      TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp );
  int Find_TopDecayProducts( const xAOD::TruthParticle* particle,
			     TLorentzVector *B_tmp,
			     TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp );
  
  int Find_WDecayProducts( const xAOD::TruthParticle* particle, 
			   TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp );

  // this is needed to distribute the algorithm to the workers
  ClassDef(TruthxAODAnalysis_STop, 1);
};

}

#endif
