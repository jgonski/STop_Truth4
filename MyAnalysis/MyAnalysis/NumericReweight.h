#ifndef NumericReweight_H
#define NumericReweight_H

#include <iostream>
#include <cmath>
#include <string>
#include <sstream>

#include <TLorentzVector.h>
#include <TVector3.h>
#include <TMath.h>
#include <TString.h>
#include <TH1F.h>
#include <TFile.h>

#include "xAODTruth/TruthParticleContainer.h"

//-------------------------------------------------------------------------------//
// PolarozatonReweight provides weight for stop polarization reweighting         //
// code is validate for Herwig++ and MadGraph+Pythia8 (with Pythia8 stop decay)  //
//                                                                               //
// see the following wiki for detail                                             //
// https://twiki.cern.ch/twiki/bin/view/Main/StopPolarization                    //
//                                                                               //
// default unit is MeV                                                           //
// if you want to use GeV unit,                                                  //
// do NumericReweight::setUnitGeV(true);                                    //
//-------------------------------------------------------------------------------//

namespace NumericReweight{

  class NumericReweight
  {
  public:
    
    NumericReweight();
    ~NumericReweight();

    //----------------------------------------------------------------------------------//
    // return weight for polarization reweighting                                       //
    // ~t1->t+~chi10 decay is only considered                                           //
    //                                                                                  //
    //----------------------------------------------------------------------------------//

    void   SetFileName(TString filename);
    void   SetDSID( int dsid );
    void   Init();

    double getNumericalReweight(const xAOD::TruthParticleContainer *truth);
    
  private:

    TString m_filename;
    TFile *file_fullME;
    TH1F *h_bw_weight;
    
    int m_dsid;

    bool m_useGeV;
    bool m_decayPythia;
    bool m_phaseSpaceOnly;

    double m_newIntegralW;
    double m_oriIntegralW;
    double m_massW;
    double m_widthW;
    double m_massWThreshold;

    double m_newIntegralZ;
    double m_oriIntegralZ;
    double m_massZ;
    double m_widthZ;
    double m_massZThreshold;

    double m_newIntegralTop;
    double m_oriIntegralTop;
    double m_massTop;
    double m_widthTop;
    double m_massTopThreshold;

    std::string m_generatorName;
  };

} // namespace StopPolarization

#endif // NumericReweight_H
