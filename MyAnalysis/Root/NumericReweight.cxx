#include "MyAnalysis/NumericReweight.h"

namespace NumericReweight{


  NumericReweight::NumericReweight() {
    m_massW = 80399.; // MeV
    m_widthW = 2085.; // MeV
    m_massWThreshold = 0.;

    m_oriIntegralW = -1.;
    m_newIntegralW = -1.;

    m_massZ = 91187.6; // MeV
    m_widthZ = 2495.2; // MeV
    m_massZThreshold = 0.;

    m_oriIntegralZ = -1.;
    m_newIntegralZ = -1.;

    m_massTop = 172500.; // MeV
    m_widthTop = 1333.13; // MeV
    m_massTopThreshold = 86000.; // MeV

    m_oriIntegralTop = -1.;
    m_newIntegralTop = -1.;

    m_useGeV = false; // default unit is MeV

    m_generatorName = "MadGraphPythia8";
    m_decayPythia = true;
    m_phaseSpaceOnly = true;
  }
  NumericReweight::~NumericReweight() {
    file_fullME->Close();
  }

  void NumericReweight::SetFileName(TString filename){
    m_filename = filename;
    return;
  }
  
  void NumericReweight::SetDSID( int dsid ){
    m_dsid = dsid;
    return;
  }

  void NumericReweight::Init() {
    file_fullME = new TFile(m_filename.Data());
    
    std::stringstream ssi;
    ssi << m_dsid;
    std::string istr = ssi.str();

    TString name = istr+"_bWN_massReweightFactor";
    h_bw_weight = (TH1F*) file_fullME->Get(name.Data());

    return;
  }

  double NumericReweight::getNumericalReweight(const xAOD::TruthParticleContainer *truth)
  {
    TLorentzVector stop_hlv;
    TLorentzVector neut_hlv;
    TLorentzVector top_hlv;
    TLorentzVector wboson_hlv;
    TLorentzVector downtype_hlv;
    TLorentzVector uptype_hlv;
    TLorentzVector bottom_hlv;
    bool foundNeut = false;
    bool foundTop = false;
    bool foundWboson = false;
    bool foundDownType = false;
    bool foundUpType = false;
    bool foundBottom = false;
    bool selfDecay = false;
    int countReweightW = 0;
    int countReweightTop = 0;
    double newWeightW = 1.;
    double newWeightTop = 1.;
    const xAOD::TruthParticle* stop = 0;
    const xAOD::TruthParticle* top = 0;
    const xAOD::TruthParticle* wboson = 0;

    // loop over the truth particle in the container
    xAOD::TruthParticleContainer::const_iterator truth_itr = truth->begin();
    xAOD::TruthParticleContainer::const_iterator truth_end = truth->end();
    for( ; truth_itr != truth_end; ++truth_itr ) {
      if( (*truth_itr)->absPdgId()==1000006 ){
	stop = (*truth_itr);
	selfDecay = false;
	if( stop->nChildren()>0 ){
	  for(unsigned int i=0; i<stop->nChildren(); i++){
	    const xAOD::TruthParticle* child = stop->child(i);
	    if( child->pdgId()==(*truth_itr)->pdgId()){
	      stop = child;
	      selfDecay = true;
	      break;
	    }else if( child->absPdgId()==1000022 ){
	      neut_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundNeut = true;
	    }else if( child->isTop() ){
	      top_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundTop = true;
	      top = child;
	    }else if( child->isW() ){
	      wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundWboson = true;
	      wboson = child;
	    }else if( child->absPdgId()==5 ){
	      bottom_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundBottom = true;
	    }else if( child->absPdgId()==1 || child->absPdgId()==3 || child->isChLepton() ){
	      downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundDownType = true;
	    }else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
	      uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
	      foundUpType = true;
	    }
	  } // for nChildren
	} // nChildren>0
	if( selfDecay ) continue;

	stop_hlv.SetPtEtaPhiM( stop->pt(), stop->eta(), stop->phi(), stop->m() );

	if( foundTop ){
	  do{
	    top_hlv.SetPtEtaPhiM( top->pt(), top->eta(), top->phi(), top->m() );

	    selfDecay = false;
	    if( top->nChildren()>0 ){
	      for(unsigned int i=0; i<top->nChildren(); i++){
		const xAOD::TruthParticle* child = top->child(i);
		if( child->pdgId()==top->pdgId() ){
		  top = child;
		  selfDecay = true;
		  break;
		}else if( child->isW() ){
		  wboson_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  wboson = child;
		  foundWboson = true;
		}else if( child->absPdgId()==5 || child->absPdgId()==1 || child->absPdgId()==3){
		  // treat down and strange as bottom also
		  bottom_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundBottom = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundTop
	
	if( foundWboson ){
	  do{
	    selfDecay = false;
	    if( wboson->nChildren()>0 ){
	      for(unsigned int i=0; i<wboson->nChildren(); i++){
		const xAOD::TruthParticle* child = wboson->child(i);
		if( child->pdgId()==wboson->pdgId() ){
		  wboson = child;
		  selfDecay = true;
		  break;
		}else if( child->absPdgId()==1 || child->absPdgId()==3 || child->absPdgId()==5 || child->isChLepton() ){
		  downtype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundDownType = true;
		}else if( child->absPdgId()==2 || child->absPdgId()==4 || child->isNeutrino() ){
		  uptype_hlv.SetPtEtaPhiM( child->pt(), child->eta(), child->phi(), child->m() );
		  foundUpType = true;
		}
	      } // for nChildren
	    } // nChildren>0
	  }while(selfDecay);
	} // foundWboson
	
	double massTop = m_massTop;
	double widthTop = m_widthTop;
	double massTopThreshold = m_massTopThreshold;
	double massW = m_massW;
	double widthW = m_widthW;
	double massWThreshold = m_massWThreshold;

	if(!m_useGeV){
	  // scale to GeV unit
	  stop_hlv *= 0.001;
	  neut_hlv *= 0.001;
	  top_hlv *= 0.001;
	  wboson_hlv *= 0.001;
	  bottom_hlv *= 0.001;
	  downtype_hlv *= 0.001;
	  uptype_hlv *= 0.001;
	  massTop *= 0.001;
	  widthTop *= 0.001;
	  massTopThreshold *= 0.001;
	  massW *= 0.001;
	  widthW *= 0.001;
	  massWThreshold *= 0.001;
	}

	if( foundNeut && foundBottom && foundDownType && foundUpType ) {

	  double mstop = stop_hlv.M();
	  double mchi0 = neut_hlv.M();
	  double mb = bottom_hlv.M();

	  if(!foundWboson){
	    wboson_hlv = downtype_hlv + uptype_hlv;
	  }
	  double mw = wboson_hlv.M();

	  if(!foundTop){
	    top_hlv = wboson_hlv + bottom_hlv;
	  }
	  double mtop = top_hlv.M();

	  newWeightTop *= h_bw_weight->Interpolate(mtop);
	  countReweightTop ++;
	}

	foundNeut = false;
	foundTop = false;
	foundWboson = false;
	foundDownType = false;
	foundBottom = false;
      
      } // absPdgId()==1000006
    } // truth_itr
    
    if(countReweightTop!=2){
      std::cerr << "WARNING: There are not two stops in the event!" << std::endl;
    }
    return newWeightTop;

  } // getRweightTopNeutralino()


} // namespace StopPolarization
