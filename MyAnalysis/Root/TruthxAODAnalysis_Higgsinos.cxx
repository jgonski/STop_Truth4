#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <MyAnalysis/TruthxAODAnalysis_Higgsinos.h>
#include <MyAnalysis/Util.h>
#include <TString.h>

#include "xAODRootAccess/tools/Message.h"

using namespace MyAnalysis_Higgsinos;
using namespace RestFrames;

ClassImp(TruthxAODAnalysis_Higgsinos)

TruthxAODAnalysis_Higgsinos :: TruthxAODAnalysis_Higgsinos ()
{
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: setupJob (EL::Job& job)
{
  job.useXAOD ();
  xAOD::Init( "TruthxAODAnalysis_Higgsinos" ).ignore(); // call before opening first file 
  EL_RETURN_CHECK( "setupJob()", xAOD::Init() );
  
  EL::OutputStream treeOutput ( "TruthTree" );
  job.outputAdd (treeOutput);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: histInitialize ()
{
  if (verbose) std::cout << "In histInitialize " << std::endl;

  TFile *outputFile = wk()->getOutputFile ("TruthTree");
  tree = new TTree ("TruthTree", "TruthTree");
  tree->SetDirectory(outputFile);

  tree->Branch( "charg_pt", &m_charg_pt );
  tree->Branch( "charg_w1_pt", &m_charg_w1_pt );
  tree->Branch( "charg_w2_pt", &m_charg_w2_pt );
  tree->Branch( "charg_xi_pt", &m_charg_xi_pt );
  tree->Branch( "charg_eta", &m_charg_eta );
  tree->Branch( "charg_w1_eta", &m_charg_w1_eta );
  tree->Branch( "charg_w2_eta", &m_charg_w2_eta );
  tree->Branch( "charg_xi_eta", &m_charg_xi_eta );
  tree->Branch( "charg_phi", &m_charg_phi );
  tree->Branch( "charg_w1_phi", &m_charg_w1_phi );
  tree->Branch( "charg_w2_phi", &m_charg_w2_phi );
  tree->Branch( "charg_xi_phi", &m_charg_xi_phi );
  tree->Branch( "charg_m", &m_charg_m );
  tree->Branch( "charg_w1_m", &m_charg_w1_m );
  tree->Branch( "charg_w2_m", &m_charg_w2_m );
  tree->Branch( "charg_xi_m", &m_charg_xi_m );
  tree->Branch( "neutr2_pt", &m_neutr2_pt );
  tree->Branch( "neutr2_w1_pt", &m_neutr2_w1_pt );
  tree->Branch( "neutr2_w2_pt", &m_neutr2_w2_pt );
  tree->Branch( "neutr2_xi_pt", &m_neutr2_xi_pt );
  tree->Branch( "neutr2_eta", &m_neutr2_eta );
  tree->Branch( "neutr2_w1_eta", &m_neutr2_w1_eta );
  tree->Branch( "neutr2_w2_eta", &m_neutr2_w2_eta );
  tree->Branch( "neutr2_xi_eta", &m_neutr2_xi_eta );
  tree->Branch( "neutr2_phi", &m_neutr2_phi );
  tree->Branch( "neutr2_w1_phi", &m_neutr2_w1_phi );
  tree->Branch( "neutr2_w2_phi", &m_neutr2_w2_phi );
  tree->Branch( "neutr2_xi_phi", &m_neutr2_xi_phi );
  tree->Branch( "neutr2_m", &m_neutr2_m );
  tree->Branch( "neutr2_w1_m", &m_neutr2_w1_m );
  tree->Branch( "neutr2_w2_m", &m_neutr2_w2_m );
  tree->Branch( "neutr2_xi_m", &m_neutr2_xi_m );
  tree->Branch( "TotISR_pt", &m_TotISR_pt );
  tree->Branch( "TotISR_eta", &m_TotISR_eta );
  tree->Branch( "TotISR_phi", &m_TotISR_phi );
  tree->Branch( "TotISR_m", &m_TotISR_m );
  tree->Branch( "TotISR_ptThrust", &m_TotISR_ptThrust );
  tree->Branch( "PTISR", &m_PTISR );
  tree->Branch( "PTI", &m_PTI );
  tree->Branch( "RISR", &m_RISR );
  tree->Branch( "NjV", &m_NjV );
  tree->Branch( "nISR", &m_nISR );
  tree->Branch( "NjISR", &m_NjISR );
  tree->Branch( "MS", &m_MS );
  tree->Branch( "ML", &m_ML );
  tree->Branch( "MV", &m_MV );
  tree->Branch( "MISR", &m_MISR );
  tree->Branch( "MET", &m_metTruth );
  tree->Branch( "dixi_pt", &m_dixi_pt );
  tree->Branch( "dixi_eta", &m_dixi_eta );
  tree->Branch( "dixi_phi", &m_dixi_phi );
  tree->Branch( "dixi_m", &m_dixi_m );
  tree->Branch( "dixi_pThrust", &m_dixi_pThrust );
  tree->Branch( "dphiISRI", &m_dphiISRI );
  tree->Branch( "mll", &m_mll );
  
  return EL::StatusCode::SUCCESS;
}

void TruthxAODAnalysis_Higgsinos :: ResetVariables() {
   m_charg_pt = 0.0;
   m_charg_w1_pt = 0.0;
   m_charg_w2_pt = 0.0;
   m_charg_xi_pt = 0.0;
   m_charg_eta = 0.0;
   m_charg_w1_eta = 0.0;
   m_charg_w2_eta = 0.0;
   m_charg_xi_eta = 0.0;
   m_charg_phi = 0.0;
   m_charg_w1_phi = 0.0;
   m_charg_w2_phi = 0.0;
   m_charg_xi_phi = 0.0;
   m_charg_m = 0.0;
   m_charg_w1_m = 0.0;
   m_charg_w2_m = 0.0;
   m_charg_xi_m = 0.0;
   m_neutr2_pt = 0.0;
   m_neutr2_w1_pt = 0.0;
   m_neutr2_w2_pt = 0.0;
   m_neutr2_xi_pt = 0.0;
   m_neutr2_eta = 0.0;
   m_neutr2_w1_eta = 0.0;
   m_neutr2_w2_eta = 0.0;
   m_neutr2_xi_eta = 0.0;
   m_neutr2_phi = 0.0;
   m_neutr2_w1_phi = 0.0;
   m_neutr2_w2_phi = 0.0;
   m_neutr2_xi_phi = 0.0;
   m_neutr2_m = 0.0;
   m_neutr2_w1_m = 0.0;
   m_neutr2_w2_m = 0.0;
   m_neutr2_xi_m = 0.0;
   m_TotISR_pt = 0.0;
   m_TotISR_eta = 0.0;
   m_TotISR_phi = 0.0;
   m_TotISR_m = 0.0;
   m_TotISR_ptThrust = 0.0;
   m_metTruth = 0.0;
   m_metPhiTruth = 0.0;
   m_PTISR = 0.0;
   m_PTI = 0.0;
   m_RISR = 0.0;
   m_NjV = 0;
   m_nISR = 0;
   m_NjISR = 0;
   m_MS = 0.0;
   m_ML = 0.0;
   m_MV = 0.0;
   m_MISR = 0.0;
   m_distop_pt = 0.0;
   m_distop_eta = 0.0;
   m_distop_phi  = 0.0;
   m_distop_m = 0.0;
   m_distop_pThrust = 0.0;
   m_dixi_pt = 0.0;
   m_dixi_eta = 0.0;
   m_dixi_phi = 0.0;
   m_dixi_m = 0.0;
   m_dixi_pThrust = 0.0;
   m_dphiISRI = 0.0;
   m_mll = 0.0;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: initialize ()
{
  xAOD::TEvent* event = wk()->xaodEvent();
  evt_display_name = "event_display.eps";
  ps = new TPostScript(evt_display_name.c_str(), 112);

  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // RestFrames stuff
  m_truthJets = new vector<TLorentzVector>;
  m_truthLeptons = new vector<TLorentzVector>;
  m_truthJetIsBTagged = new vector<bool>;
  truthJets = 0;       

  ////////////// Tree set-up /////////////////
  LAB = new LabRecoFrame("LAB", "lab");
  CM = new DecayRecoFrame("CM", "CM");
  S = new DecayRecoFrame("S", "S");
  ISR = new VisibleRecoFrame("ISR", "ISR");
  V = new VisibleRecoFrame("V", "Vis");
  L = new VisibleRecoFrame("L","Lep");
  I = new InvisibleRecoFrame("I", "Inv");
 
  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  S->AddChildFrame(*L);
 
  LAB->InitializeTree();
  ////////////// Tree set-up /////////////////
 
  ////////////// Jigsaw rules set-up /////////////////
  INV = new InvisibleGroup("INV", "Invisible System");
  INV->AddFrame(*I);
 
  VIS = new CombinatoricGroup("VIS", "Visible Objects");
  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR, 1, false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V, 0, false);
 
  // set the invisible system mass to zero
  InvMass = new SetMassInvJigsaw("InvMass", "Invisible system mass Jigsaw");
  INV->AddJigsaw(*InvMass);
 
 
  // define the rule for partitioning objects between "ISR" and "V"
  SplitVis = new MinMassesCombJigsaw("SplitVis", "Minimize M_{ISR} and M_{S} Jigsaw");
  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I + L)
  SplitVis->AddFrame(*V, 1);
  SplitVis->AddFrame(*I, 1);
  SplitVis->AddFrame(*L, 1);
 
  LAB->InitializeAnalysis();
  ////////////// Jigsaw rules set-up /////////////////

  if (verbose) std::cout << "in Initialize, about to initialise variables" << std::endl;
  m_eventCounter = 0;

  count = 0;

  thrust_vec = TLorentzVector(0.,0.,0.,0.);
  thrust_perp_vec = TLorentzVector(0.,0.,0.,0.);
  
  charg = TLorentzVector(0.,0.,0.,0.);
  charg_wj1 = TLorentzVector(0.,0.,0.,0.);
  charg_wj2 = TLorentzVector(0.,0.,0.,0.);
  charg_xi =  TLorentzVector(0.,0.,0.,0.);

  neutr2 = TLorentzVector(0.,0.,0.,0.);
  neutr2_wj1 = TLorentzVector(0.,0.,0.,0.);
  neutr2_wj2 = TLorentzVector(0.,0.,0.,0.);
  neutr2_xi = TLorentzVector(0.,0.,0.,0.);
               
  ISR_total = TLorentzVector(0.,0.,0.,0.);
  verbose = true;

  w_polemass = 80.35;
  b_polemass = 4.65;
  t_polemass = 173.34;

  t_reduced_mass= t_polemass*t_polemass- w_polemass*w_polemass- b_polemass*b_polemass;

  expected_b_momentum = sqrt( t_reduced_mass *t_reduced_mass / 4. / t_polemass / t_polemass -
                              b_polemass*b_polemass*w_polemass*w_polemass/t_polemass/t_polemass );

  GeV = 1000.;

  if (verbose) std::cout << "Finished intializing variables " << std::endl;
  return EL::StatusCode::SUCCESS;
}

void TruthxAODAnalysis_Higgsinos::setEvtDisplay(std::string eps) {
  evt_display_name = eps;
  return;
}

void TruthxAODAnalysis_Higgsinos::DrawEvtDisplay(){

  if(verbose) std::cout << "drawing display" << std::endl;

  TCanvas *c2 = new TCanvas("c2");
  c2->Range(0,0,1,1);

  TArrow *metArrow = new TArrow();
  TArrow *arrow = new TArrow();

  TLatex p;
  p.SetTextSize(0.038);
  p.SetTextFont(42);

  metArrow->SetLineColor(13);
  metArrow->SetLineWidth(3);
  metArrow->SetLineStyle(2);
  arrow->SetLineWidth(3);

  std::vector<float> jetpt_vec;
  jetpt_vec.resize(0);
  jetpt_vec.push_back(charg_wj1.Pt());
  jetpt_vec.push_back(charg_wj2.Pt());
  //jetpt_vec.push_back(neutr2_wj1.Pt());   Z only contributes leptons!
  //jetpt_vec.push_back(neutr2_wj2.Pt());
  jetpt_vec.push_back(m_metTruth);
  for (int i=0;i<nISR;i++){
    jetpt_vec.push_back(ISR_p[i][3]);
  }
  std::sort (jetpt_vec.begin(), jetpt_vec.end());
  double maximum = 2.6*jetpt_vec.back();

  
  double length  = m_metTruth/maximum;
  metArrow->DrawArrow(0.62, 0.5, 0.7-length*cos(m_metPhiTruth), 0.5+length*sin(m_metPhiTruth),0.02,"|>");

  //no b jets hereeee
  
  if(verbose) { 
    std::cout << "Chargino 1 jet: pT: " << charg_wj1.Pt() << ", eta: " << charg_wj1.Eta() << ", phi: " << charg_wj1.Phi() << std::endl;
    std::cout << "Chargino 2 jet: pT: " << charg_wj2.Pt() << ", eta: " << charg_wj2.Eta() << ", phi: " << charg_wj2.Phi() << std::endl;
    std::cout << "Neutralino 1 lepton: pT: " << neutr2_wj1.Pt() << ", eta: " << neutr2_wj1.Eta() << ", phi: " << neutr2_wj1.Phi() << std::endl;
    std::cout << "Neutralino 2 lepton: pT: " << neutr2_wj2.Pt() << ", eta: " << neutr2_wj2.Eta() << ", phi: " << neutr2_wj2.Phi() << std::endl;
  }

  //chargino goes to W jets 
  length = charg_wj1.Pt()/maximum;
  double phi =charg_wj1.Phi();
  arrow->SetLineColor(kOrange);
  p.DrawLatex(0.1,0.46,Form("#color[800]{Wjet p_{T} = %3.2f GeV}",charg_wj1.Pt()));
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  length = charg_wj2.Pt()/maximum;
  phi =charg_wj2.Phi();
  p.DrawLatex(0.1,0.40,Form("#color[800]{Wjet p_{T} = %3.2f GeV}",charg_wj2.Pt()));
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");

  //neutralino goes to leptons 
  length = neutr2_wj1.Pt()/maximum;
  phi =neutr2_wj1.Phi();
  arrow->SetLineColor(kMagenta);
  p.DrawLatex(0.1,0.58,Form("#color[6]{lepton1 p_{T} = %3.2f GeV}",neutr2_wj1.Pt()));
  arrow->SetLineStyle(1);
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");
  length = neutr2_wj2.Pt()/maximum;
  phi =neutr2_wj2.Phi();
  p.DrawLatex(0.1,0.52,Form("#color[6]{lepton2 p_{T} = %3.2f GeV}",neutr2_wj2.Pt()));
  arrow->DrawArrow(0.62, 0.5, 0.62-length*cos(phi), 0.5+length*sin(phi),0.02,"|>");

  TLorentzVector vecISR; 
  double place = 0.14;
  for (int i=0; i<nISR; i ++){
    vecISR.SetPxPyPzE(ISR_p[i][0],ISR_p[i][1],ISR_p[i][2],ISR_p[i][4]);
    if(verbose) std::cout << "ISR jet " << i << ": pT: " << vecISR.Pt() << ", eta: " << vecISR.Eta() << ", phi: " << vecISR.Phi() << std::endl;
    arrow->SetLineColor(kRed);
    arrow->SetLineStyle(1);
    arrow->DrawArrow(0.62, 0.5, 0.62-ISR_p[i][0]/maximum, 0.5+ISR_p[i][1]/maximum,0.02,"|>");
    p.DrawLatex(0.1,place,Form("#color[2]{ISR jet_{%1i} p_{T} = %3.0f GeV}",i+1 , ISR_p[i][3]));
    place -= 0.06;
  }

  p.DrawLatex(0.1,0.97,Form("#color[1]{R_{ISR} = %3.2f, p_{T,ISR} = %3.0f GeV, M_{T,S} = %3.0f GeV, M_{T,L} = %3.2f GeV, pT_I = %3.2f GeV}",m_RISR,m_PTISR,m_MS,m_ML,m_PTI) );
  p.DrawLatex(0.1,0.90,Form("#color[13]{MET = %3.0f GeV, NjV = %3.0i, NjISR = %3.0i, nISR = %3.0i}",m_metTruth, m_NjV,m_NjISR,m_nISR) );
  p.DrawLatex(0.1,0.20,Form("#color[2]{Tot. ISR p_{T}  = %3.0f GeV}",ISR_total.Pt()) );

  c2->Update();  ps->NewPage();

  delete c2;
  delete arrow;
  delete metArrow;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: execute ()
{
  if ( verbose ) std::cout << std::endl;
  if ( verbose ) std::cout << "new evt" << std::endl;

  // reset variables saved in output tree
  ResetVariables();

  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent* event = wk()->xaodEvent();

  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));  


  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }   

  //--------------------------------------------------------//
  //     Look in the event's truth container                //
  //--------------------------------------------------------//

  //  Full Truth information is not available.
  //  Check here for basic information
  //
  //  https://indico.cern.ch/event/307549/session/7/contribution/31/material/slides/0.pdf

  //-------------------------------------------------------//

  //  Load the list of truth particles in the event
  //  Note not all particles in the truth has been saved:  Only the main decay tree have been saved
  //  If your code dies because you can't find a particle->barcode() for example
  //  it might mean that you are trying to access a particle that was not saved.
  
  const xAOD::TruthParticleContainer* TruthParticleCont = 0;
  EL_RETURN_CHECK("execute()",event->retrieve(TruthParticleCont, "TruthParticles"));

  //--------------------------------------------------------------//
  //             Loop over all saved truth particles              //
  //--------------------------------------------------------------//

  xAOD::TruthParticleContainer::const_iterator ipart_itr = TruthParticleCont->begin();
  xAOD::TruthParticleContainer::const_iterator ipart_end = TruthParticleCont->end();

  if (verbose) std::cout << "in execute, about to set lorentz vectors to 0 " << std::endl;
  charg.SetPxPyPzE(0.,0.,0.,0.);
  charg_wj1.SetPxPyPzE(0.,0.,0.,0.);
  charg_wj2.SetPxPyPzE(0.,0.,0.,0.);
  charg_xi.SetPxPyPzE(0.,0.,0.,0.);

  neutr2.SetPxPyPzE(0.,0.,0.,0.);
  neutr2_wj1.SetPxPyPzE(0.,0.,0.,0.);
  neutr2_wj2.SetPxPyPzE(0.,0.,0.,0.);
  neutr2_xi.SetPxPyPzE(0.,0.,0.,0.);
  dixi.SetPxPyPzE(0.,0.,0.,0.);

  nISR = 0;
  ISR_p.resize(0);

  thrust_vec.SetXYZT(0,0,0,0);
  thrust_perp_vec.SetXYZT(0,0,0,0);

  plotEvent = false;
  
  //--------------------------------------------------------//
  //           Start to loop over truth here                //
  //--------------------------------------------------------//

  for( ; ipart_itr != ipart_end; ++ipart_itr ) {

    //if (verbose) std::cout << "inside particle loop #1" << std::endl;
    //    Get the particle from the particle container
    const xAOD::TruthParticle* particle = (*ipart_itr);

    //--------------------------------------------------------------------------------------------//
    //  access all the information from the particle
    //  what information can be accessed is documented here
    //  http://hep.uchicago.edu/~kkrizka/rootcoreapis/dd/dc2/classxAOD_1_1TruthParticle__v1.html
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    // This is the unique barcode of the particle.
    // if two particles have with the same barcode then they
    // are the same particle with same pt, pz, m, etc...
    //int barcode = particle->barcode(); 
    //  The PDG "ID" is the physical type of particle
    //  an id of 6 is a top quark, -6 is a anti-top
    //  an id of 11 is an electron, -11 is an positron
    //  a table of pdg ID can be found here
    //  http://pdg.lbl.gov/2005/reviews/montecarlorpp.pdf
    //--------------------------------------------------------------------------------------------//

    int pdgId = particle->pdgId();

    //--------------------------------------------------------------------------------------------//
    //  status of the particle is the stage of the particle in the decay chain
    //  Unfortunately depending on the MC generator, different status code means different things
    //  http://lcgapp.cern.ch/project/simu/HepMC/205/status.html
    //  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookGenParticleCandidate
    //  a status code of 1 is a final state particle.  It will not decay anymore
    //  a status code of 2 is an intermediate particle. It will decay further
    //  a status code of 3 identifies the "hard part" of the interaction, 
    //  i.e. the partons that are used in the matrix element calculation, including immediate decays of resonances.
    //  Sometimes a mid decay particle is more interesting than a final state particle.
    //  For quarks/gluon, you are not interested in all the final quarks.  You want the initial
    //  quark/gluon with status code 2 that makes all the other quark/gluons in the chain
    //  The original unstable quark/gluon will have the energy of the full jet
    //  ignore particles with all other status codes 
    //  they dont mean anything they are internal book-keeping for 
    //  the MC generation to balance out 4 momenta.  They aren't "real" or part of the simulation
    //--------------------------------------------------------------------------------------------//

    int status = particle->status();

    //--------------------------------------------------------------------------------------------//    
    //  Four momentum values of the particle
    //  Everthing is in MeV so you'll have to convert to GeV manually
    //--------------------------------------------------------------------------------------------//

        float px = particle->px();
        float py = particle->py();
        float pz = particle->pz();
        float pt = particle->pt();

    //--------------------------------------------------------------------------------------------//
    //         Charge of the particle
    //--------------------------------------------------------------------------------------------//

        float e = particle->e();

    //--------------------------------------------------------------------------------------------//
    //          Mass of the particle
    //--------------------------------------------------------------------------------------------//

        float m = particle->m();

    // Other relevant infos 
    int npart_daughter = particle->nChildren();
    int npart_parents = particle->nParents();


    //  if debugging output every particle
      if ( verbose && pdgId > 1000010 && pdgId < 10000100) std::cout << "pdgId " << pdgId << " status " << status << " nChildren " << npart_daughter << " nParents " << npart_parents << " pt " << pt << " event " << m_eventCounter << std::endl;

    //---------------------------------------------------------//
    //           Look for all the inos             //
    //---------------------------------------------------------//

    //  In MC, stops and any paricle can be passed along a chain to tweak its momentum slightly
    //  each stop in the chain will have a different status
    // Chargino = 1000024, Neutralino2 = 1000023, Neutralino 1 = 1000022
     int chargOut = 0;
     if (  fabs(pdgId) ==  1000024 && status==62) chargOut = Find_CharginoDecayProducts( particle, &charg_xi, &charg_wj1, &charg_wj2 );
	if(chargOut == -999) return EL::StatusCode::SUCCESS;
     if (  pdgId ==  1000023 && status==62) int neutr2Out = Find_NeutralinoDecayProducts( particle, &neutr2_xi,&neutr2_wj1, &neutr2_wj2 ); // fill empty 4 vectors of objects 
   } // end of first loop over particles

  if(verbose) std::cout << "Four vectors filled; now need to actually group objects into inos/ISR and draw event display" << std::endl;

  //-------------------------------------------------------//     
  //    Find the initial pp -> chargino + neutralino + ISR decay     //  
  //-------------------------------------------------------//     
  ipart_itr = TruthParticleCont->begin();
  if(verbose) std::cout << "Filled all four vectors, starting loop #2!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
  for( ; ipart_itr != ipart_end; ++ipart_itr ) {
    //if (verbose) std::cout << "inside particle loop #2" << std::endl;
    const xAOD::TruthParticle* particle = (*ipart_itr);
    int pdgId = particle->pdgId();
    int status = particle->status();

    //  only considering one ino to avoid double-counting
    //TODO : should I get a particular status here to know that I've found the right ino? 
    // think I just need to do this to make sure I get a prod vtx. Later I loop over all particles in the vtx anyway. 
    if ( pdgId == 1000023 && status==22 ) {
      if (verbose) std::cout << "time to calculate ISR by removing the retrieved ino values" << std::endl;
      
      bool hasProdVtx = particle->hasProdVtx();
      if (hasProdVtx){

	const xAOD::TruthVertex* prodVtx = particle->prodVtx();
	int npart = prodVtx->nOutgoingParticles();

	if (verbose) std::cout << "npart for prodvtx is  " << npart << std::endl;
	//---------------------------------------------------------------------//
	//     Loop over all products of the pp -> Ino+ Ino + ISR Vertex    //
	//---------------------------------------------------------------------//

	for (int j=0; j<npart; j++){
	  const xAOD::TruthParticle* particle = prodVtx->outgoingParticle(j);
	  if( !particle ) {
	    std::cout << " Missing particle link for particle " << j << " of " << npart << std::endl;
	    return EL::StatusCode::SUCCESS;
	  } else {
	  pdgId = particle->pdgId();
	  status = particle->status();

	  //      Chargino      //
	  if (pdgId ==  -1000024) { // chargino
	    if (verbose) std::cout << "inside chargino production vertex loop..... " << std::endl;
	    if (verbose) std::cout << "The id of particle " << j << " of chargino prodvtx is " << pdgId << std::endl;
	    if (verbose) std::cout << "The status of particle "<< j << " of chargino prodvtx " << status << std::endl;
	  }
          //      Neutralino     //
	  else if (pdgId == 1000023){  //neutralino
	    if (verbose) std::cout << "inside neutralino production vertex loop..... " << std::endl;
	    if (verbose) std::cout << "The id of particle " << j << " of neutr2 prodvtx is " << pdgId << std::endl;
	    if (verbose) std::cout << "The status of particle "<< j << " of neutr2 prodvtx " << status << std::endl;
	  }
          //      Find Decay Products of ISR           //
	  else{

	    nISR++;

	    float temp_px=0.;
	    float temp_py=0.;
	    float temp_pz=0.;
	    float temp_pt=0.;
	    float temp_e=0.;
	    std::vector<float> p;
	    p.resize(0);

	    temp_px = particle->px()/GeV;
	    temp_py = particle->py()/GeV;
	    temp_pz = particle->pz()/GeV;
	    temp_pt = particle->pt()/GeV;
	    temp_e = particle->e()/GeV;

	    p.push_back(temp_px);
	    p.push_back(temp_py);
	    p.push_back(temp_pz);
	    p.push_back(temp_pt);
	    p.push_back(temp_e);
	    ISR_p.push_back(p);

	    if ( verbose ) std::cout << "ISR E " << temp_e << "ISR pt " << sqrt(temp_px*temp_px + temp_py*temp_py) << std::endl;
	  } // ISR vectoring
	} // all particles have links
      } // loop over particles in vertex
     } // if prodVtx
    }//is it 24 or 23? 
  } // finished loop over all truth particles
  if (verbose) std::cout << "Outside ino loop" << std::endl;

  //-------------------------------------------------//
  //             Find Direction of Thrust            //
  //-------------------------------------------------//

  charg_t=charg_wj1+charg_wj2;  //charg  = wj1 + wj2 
  neutr2_t=neutr2_wj1+neutr2_wj2;   //neutr2 = wl + wv

  ditop = charg_t + neutr2_t;

  float temp_px = 0.;
  float temp_py = 0.;
  float temp_pz = 0.;
  float temp_pt = 0.;
  float temp_e = 0.;
  if(verbose) std::cout << "Number of ISR jets: " << nISR << std::endl;
  if(nISR ==0) return EL::StatusCode::SUCCESS; // cut off for now
  m_nISR = nISR; 

  for (int i=0; i<nISR; i ++){
    temp_px += ISR_p[i][0];
    temp_py += ISR_p[i][1];
    temp_pz += ISR_p[i][2];
    temp_pt += ISR_p[i][3];
    temp_e += ISR_p[i][4];
  }
  ISR_total.SetPxPyPzE(temp_px,temp_py,temp_pz,temp_e);

  TLorentzVector total = ditop+ISR_total;

  dixi = charg_xi + neutr2_xi;

  //  std::cout << (stop1_t+stop1_xi).M() << " " << stop1_xi.M() << " " 
  //	    << (stop2_t+stop2_xi).M() << " " << stop2_xi.M() << std::endl;

  MET_TLV = dixi;

  //  if ( dixi.Pt() < 250. ) { return EL::StatusCode::SUCCESS; }

  if ( verbose ) std::cout << "di top P " << ditop.P() << " M " << ditop.M() << std::endl;
  if ( verbose ) std::cout << " +ISR P " <<  total.P() << " M " << total.M() << std::endl;
  if ( verbose ) std::cout << " +dixi P " <<  dixi.P() << " M " << dixi.M() << std::endl;


  FindThrustDir();
  
  //-----------------------------------------------------//
  //           Construct Combined Objects                //
  //-----------------------------------------------------//

  charg = charg_wj1 + charg_wj2 + charg_xi;
  neutr2 = neutr2_wj1 + neutr2_wj2 + neutr2_xi;

  distop = charg + neutr2;
  dixi = charg_xi + neutr2_xi;
  charg_jets = charg_wj1 + charg_wj2;
  neutr2_jets = neutr2_wj1 + neutr2_wj2;
  charg_w = charg_wj1 + charg_wj2;
  neutr2_w = neutr2_wj1 + neutr2_wj2;

  //-----------------------------------------------------//  
  // -------------------------------------
  // Fill output variables
  // -------------------------------------  
  m_charg_pt = charg.Pt();
  m_charg_w1_pt = charg_wj1.Pt();
  m_charg_w2_pt = charg_wj2.Pt();
  m_charg_xi_pt = charg_xi.Pt();
  m_charg_eta = charg.Eta();
  m_charg_w1_eta = charg_wj1.Eta();
  m_charg_w2_eta = charg_wj2.Eta();
  m_charg_xi_eta = charg_xi.Eta();
  m_charg_phi = charg.Phi();
  m_charg_w1_phi = charg_wj1.Phi();
  m_charg_w2_phi = charg_wj2.Phi();
  m_charg_xi_phi = charg_xi.Phi();
  m_charg_m = charg.M();
  m_charg_w1_m = charg_wj1.M();
  m_charg_w2_m = charg_wj2.M();
  m_charg_xi_m = charg_xi.M();

  m_neutr2_pt = neutr2.Pt();
  m_neutr2_w1_pt = neutr2_wj1.Pt();
  m_neutr2_w2_pt = neutr2_wj2.Pt();
  m_neutr2_xi_pt = neutr2_xi.Pt();
  m_neutr2_eta = neutr2.Eta();
  m_neutr2_w1_eta = neutr2_wj1.Eta();
  m_neutr2_w2_eta = neutr2_wj2.Eta();
  m_neutr2_xi_eta = neutr2_xi.Eta();
  m_neutr2_phi = neutr2.Phi();
  m_neutr2_w1_phi = neutr2_wj1.Phi();
  m_neutr2_w2_phi = neutr2_wj2.Phi();
  m_neutr2_xi_phi = neutr2_xi.Phi();
  m_neutr2_m = neutr2.M();
  m_neutr2_w1_m = neutr2_wj1.M();
  m_neutr2_w2_m = neutr2_wj2.M();
  m_neutr2_xi_m = neutr2_xi.M();
  
  m_charg_pThrust = CalculatePAlongThrust( &charg );
  m_neutr2_pThrust = CalculatePAlongThrust( &neutr2 );

  m_distop_pt = distop.Pt();
  m_distop_eta = distop.Eta(); 
  m_distop_phi = distop.Phi();
  m_distop_m = distop.M();
  m_distop_pThrust = CalculatePAlongThrust( &distop );

  m_dixi_pt = dixi.Pt();
  m_dixi_eta = dixi.Eta();
  m_dixi_phi = dixi.Phi();
  m_dixi_m = dixi.M();
  m_dixi_pThrust = CalculatePAlongThrust( &dixi );

  m_TotISR_pt = ISR_total.Pt();
  m_TotISR_eta = ISR_total.Eta();
  m_TotISR_phi = ISR_total.Phi();
  m_TotISR_m = ISR_total.M();
  m_TotISR_ptThrust = CalculatePAlongThrust( &ISR_total );

  //-------------------------------------------------//

  //return EL::StatusCode::SUCCESS; // cut off  

  std::stringstream ssi;
  ssi << eventInfo->runNumber();
  std::string istr = ssi.str();


  //----------------------------------------//
  m_truthJets->resize(0);

  truthJets = 0;
  if (event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
    for (unsigned int jetI=0; jetI<truthJets->size(); ++jetI) {
      const xAOD::Jet* jet = (truthJets->at(jetI));
      std::cout << "Chargino 2 jet: pT: " << charg_wj2.Pt() << ", eta: " << charg_wj2.Eta() << ", phi: " << charg_wj2.Phi() << std::endl;
      if (jet->pt()/1000. < 20) // Modify this and make it 35 GeV
	continue;
      if ( fabs(jet->eta()) > 4.5 )
	continue;
      if ( fabs(jet->eta()) > 2.8 )
	continue;

      m_truthJets->push_back(TLorentzVector(jet->px()/1000.,jet->py()/1000.,
					    jet->pz()/1000.,jet->e()/1000.));

      //      int jetFlav = xAOD::jetFlavourLabel(jet, xAOD::GAFinalHadron);

    }
  }
    //if (charg_wj1.Pt() > 20. && fabs(charg_wj1.Eta()) < 2.5 ){
      //m_truthJets->push_back(charg_wj1);
      //m_truthJets->push_back(charg_wj2);
   // }
  
  //----------------------------------------//
  m_truthLeptons->resize(0);
  m_truthLeptons->push_back(neutr2_wj1);
  m_truthLeptons->push_back(neutr2_wj2);

  //  const xAOD::JetContainer* truthJets = 0;
 // truthLeptons = 0;
 // if (event->retrieve(truthLeptons, "MuonTruthParticles").isSuccess()) {
 //   for (unsigned int lepI=0; lepI<truthLeptons->size(); ++lepI) {
 //     const xAOD::Muon* mu = (truthLeptons->at(lepI));
////      if (jet->pt()/1000. < 20) // Modify this and make it 35 GeV
////	continue;
////      if ( fabs(jet->eta()) > 4.5 )
////	continue;
////      if ( fabs(jet->eta()) > 2.8 )
////	continue;

 //     m_truthLeptons->push_back(TLorentzVector(mu->px()/1000.,mu->py()/1000.,
 //       				    mu->pz()/1000.,mu->e()/1000.));

 //     //      int jetFlav = xAOD::jetFlavourLabel(jet, xAOD::GAFinalHadron);

 //   }
 // }

  //--------------------------//

  double m_metXTruth=0.0;
  double m_metYTruth=0.0;
  m_metPhiTruth=0.0;
  double m_metSumEtTruth=0.0;
  m_metTruth=0.0;

  truthMetContainer = 0;
  if (event->retrieve(truthMetContainer, "MET_Truth").isSuccess()) {
    xAOD::MissingETContainer::const_iterator it = truthMetContainer->find("NonInt");
    if (it == truthMetContainer->end()) {
      std::cout << "not truthmet" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
    m_metXTruth        = (*it)->mpx()/1000.;
    m_metYTruth        = (*it)->mpy()/1000.;
    m_metPhiTruth   = (*it)->phi();
    m_metSumEtTruth = (*it)->sumet()/1000.;
    m_metTruth = TMath::Sqrt(m_metXTruth*m_metXTruth + m_metYTruth*m_metYTruth);
  }

  //--------------------------//

  // analyze event in RestFrames tree
  LAB->ClearEvent();
  vector<RFKey> jetID;
  for (UInt_t i = 0; i < m_truthJets->size(); i++)
  {
	  TLorentzVector jet;
	  jet.SetPtEtaPhiM(m_truthJets->at(i).Pt(), 0.0, m_truthJets->at(i).Phi(), m_truthJets->at(i).M());
	  jetID.push_back(VIS->AddLabFrameFourVector(jet));
  }
  TLorentzVector lepSys;
  vector<RFKey> lepID;
  for(UInt_t i = 0; i < m_truthLeptons->size(); i++){
	  TLorentzVector lep1;
	  lep1.SetPtEtaPhiM(m_truthLeptons->at(i).Pt(), 0.0, m_truthLeptons->at(i).Phi(), m_truthLeptons->at(i).M());
	  lepID.push_back(VIS->AddLabFrameFourVector(lep1));
	  lepSys = lepSys + lep1;
  }

  L->SetLabFrameFourVector(lepSys);

  TVector3 met(m_metXTruth,m_metYTruth, 0.0);
  INV->SetLabFrameThreeVector(met);
  if (!LAB->AnalyzeEvent()) cout << "Something went wrong..." << endl;
 
  // Compressed variables from tree
  m_NjV = 0;
  m_NjISR = 0;
  m_pTjV1 = 0.;
  m_pTjV2 = 0.;
  m_pTjV3 = 0.;
  m_pTjV4 = 0.;
  m_pTjV5 = 0.;
  m_pTjV6 = 0.;
  m_pTbV1 = 0.;
  m_pTbV2 = 0.;

  double Ht = 0;
  double jet1_pt = -999;
  double jet2_pt = -999;
  double jet3_pt = -999;
  double jet4_pt = -999;
  double jet5_pt = -999;
  double jet6_pt = -999;

  if(verbose) std::cout << "Number of truth jets: " << m_truthJets->size() << std::endl;
  for (uint i = 0; i < m_truthJets->size(); i++) {
    TLorentzVector *jet = &(m_truthJets->at(i));

    Ht += jet->Pt();
    
    if ( i == 0 ) jet1_pt = jet->Pt();
    if ( i == 1 ) jet2_pt = jet->Pt();
    if ( i == 2 ) jet3_pt = jet->Pt();
    if ( i == 3 ) jet4_pt = jet->Pt();
    if ( i == 4 ) jet5_pt = jet->Pt();
    if ( i == 5 ) jet6_pt = jet->Pt();

    if (VIS->GetFrame(jetID[i]) == *V) { // sparticle group
      m_NjV++;
      if (m_NjV == 1)
	m_pTjV1 = jet->Pt();
      if (m_NjV == 2)
	m_pTjV2 = jet->Pt();
      if (m_NjV == 3)
	m_pTjV3 = jet->Pt();
      if (m_NjV == 4)
	m_pTjV4 = jet->Pt();
      if (m_NjV == 5)
	m_pTjV5 = jet->Pt();
      if (m_NjV == 6)
	m_pTjV6 = jet->Pt();
    }
    else {
      m_NjISR++;
    }
  }

 
  // need at least one jet associated with sparticle-side of event
  //if (m_NjV + m_NlV < 1) {
    m_PTISR = 0.;
    m_PTI = 0.;
    m_RISR = 0.;
    m_cosS = 0.;
    m_ML = 0.;
    m_MS = 0.;
    m_MV = 0.;
    m_MISR = 0.;
    m_dphiCMI = 0.;
    m_dphiISRI = 0.;
  //}
  //else {
 
    TLorentzVector lepSum = neutr2_wj1+neutr2_wj2;
    m_mll = lepSum.M2();
 
    TVector3 vP_ISR = ISR->GetFourVector(*CM).Vect();
    TVector3 vP_I   = I->GetFourVector(*CM).Vect();
 
    m_PTISR = vP_ISR.Mag();
    m_PTI = vP_I.Mag();
    m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
    m_cosS = S->GetCosDecayAngle();
    m_MS = S->GetMass();
    m_MV = V->GetMass();
    m_ML = L->GetMass();
    m_MISR = ISR->GetMass();
    m_dphiCMI = acos(-1.)-fabs(CM->GetDeltaPhiBoostVisible());
    m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));

   if(verbose) {std::cout << "RISR: " << m_RISR << ", NjV: " << m_NjV << ", NjISR: " << m_NjISR << ", Nl: " << m_truthLeptons->size() << std::endl;
	for(int k =0; k<m_truthJets->size(); k++){
		std::cout << " jet pt: " << m_truthJets->at(k).Pt();}
	for(int j =0; j<m_truthLeptons->size(); j++){
		std::cout << " lepton pt: " << m_truthLeptons->at(j).Pt();}
	std::cout << "Truth MET: " << m_metTruth << std::endl;
	std::cout << "Charg pt: " << m_charg_pt << ", neutralino2 pt: " << m_neutr2_pt << ", charg thrust: " << m_charg_pThrust << ", neutr2 thrust: " << m_neutr2_pThrust << std::endl;
	std::cout << "dixi pt; " << m_dixi_pt << ", total ISR pt: " << m_TotISR_pt << ", distop? " << m_distop_pt << endl;
	std::cout << "Things that go into RISR: PTI " << m_PTI << ", dot product numerator: " << fabs(vP_I.Dot(vP_ISR.Unit())) << ", PTISR: " << m_PTISR << std::endl;
   }

  //}

  // Draw event displays 
  DrawEvtDisplay();

  // fill the output tree                                                                                                                                                          
  tree->Fill();

  return EL::StatusCode::SUCCESS; // cut off for now
}

double TruthxAODAnalysis_Higgsinos::CalculatePAlongThrust( TLorentzVector *Vec ) {
  return Vec->Px()*thrust_vec.Px() + Vec->Py()*thrust_vec.Py();
}
double TruthxAODAnalysis_Higgsinos::CalculatePAlongThrust( double px, double py ) {
  return px*thrust_vec.Px() + py*thrust_vec.Py();
}

int TruthxAODAnalysis_Higgsinos::Find_CharginoDecayProducts( const xAOD::TruthParticle* particle, 
						     TLorentzVector *Xi_tmp,  
						     TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp ) {
  if(verbose) std::cout << "Find_CharginoDecayProducts" << std::endl;

  //  const xAOD::TruthVertex* decayVtx = particle->decayVtx();
  const xAOD::TruthParticle* particle_daughter;
  //  const xAOD::TruthVertex* decayVtx_daughter = decayVtx;

  int npart_daughter = particle->nChildren(); //decayVtx_daughter->nOutgoingParticles();
  int pdgId, status, isSTopHad;


  //------------------------------------------------------------//
  //       Look at decay vertex see what the ino decays to     //

  if (verbose) std::cout << "npart daughter before whileloop is : " << npart_daughter << std::endl;

  bool foundIno = false;

  while (!foundIno){
    //particle_daughter = particle->child(0); //decayVtx_daughter->outgoingParticle(0);
    //decayVtx = decayVtx_daughter;
    //decayVtx_daughter = particle_daughter->decayVtx();
    npart_daughter = particle->nChildren();//decayVtx_daughter->nOutgoingParticles();

    if ( npart_daughter == 1 ) {
      foundIno = false;
      particle_daughter = particle->child(0);
      particle = particle_daughter;
    }
    else  {
      foundIno = true;

      /*
      for ( int i=0; i<npart_daughter; i++) {
	particle_daughter = particle->child(i);
	int pdgId = particle_daughter->pdgId();//decayVtx_daughter->outgoingParticle(i)->pdgId();
	if ( abs(pdgId)==1000006 ) {
	  foundSTop = false;
	  particle = particle_daughter;
	}
      }
      */
    }

    //    particle = particle_daughter;
  }

  status = particle->status();
  if (verbose) std::cout << "npart daughter after while is : " << npart_daughter << std::endl;
  if (verbose) std::cout << "particle's status is: " << status << std::endl;

  //----------------------------------------------------------------//
  //               Found last ino in the chain.                    //
  //  This ino decay vertex should be the ino -> W/Z + Xi vertex  //
  //----------------------------------------------------------------//

  //  bool hasDecayVtx = particle->hasDecayVtx();
  //  if (hasDecayVtx){

  //    const xAOD::TruthVertex* decayVtx = particle->decayVtx();
  int npart = particle->nChildren(); //decayVtx->nOutgoingParticles();

    //----------------------------------------------------------------//
    //             Loop over decay products of the ino                //
    //----------------------------------------------------------------//
    bool j1_exists = false; 
    for (int j=0; j<npart; j++){

      const xAOD::TruthParticle* particle1 = particle->child(j); //decayVtx->outgoingParticle1(j);
      pdgId = particle1->pdgId();
      status = particle1->status();

      double px = particle1->px()/GeV;
      double py = particle1->py()/GeV;
      double pz = particle1->pz()/GeV;
      double e = particle1->e()/GeV;

      if (verbose) std::cout << "The id of particle " << j << " of ino1 decayvtx is " << pdgId << std::endl;
      if (verbose) std::cout << "The status of particle "<< j << " of ino1 decayvtx " << status << std::endl;
      if (verbose) std::cout << "px: " << px << ", py: " << py << ", pz: " << pz << ", e:" << e << std::endl;
      
      //Get out of leptonically decaying off shell Ws
      if(fabs(pdgId)>=11 && fabs(pdgId) <=16) return -999; 

      //----------------------------------------------//
      //     Find any susy particle1s, this is the Xi  //
      if (abs(pdgId) >= 100000){
	Xi_tmp->SetPxPyPzE(px,py,pz,e);
      }

      //-------------------------//
      //     Find the W boson    //
      //if (abs(pdgId) == 24){
	//isSTopHad = Find_WDecayProducts( particle1, WJ1_tmp, WJ2_tmp );          
     // }

      //     Find the quarks    //
      else {
	if ( !j1_exists ) {
          //if (verbose) std::cout << "inside chargW first jet loop " << std::endl;
          j1_exists = true;
          WJ1_tmp->SetPxPyPzE(px,py,pz,e);
        }
        else {
          //if (verbose) std::cout << "inside neutr2 first jet loop " << std::endl;
          WJ2_tmp->SetPxPyPzE(px,py,pz,e);
          isSTopHad = 0;
        }
      }
      
       
      //no tops here 
      //if ( abs(pdgId) == 6 ) {
      //  Top_tmp->SetPxPyPzE(px,py,pz,e);
      //  if (verbose) std::cout << "stop1_t_p is " << sqrt(px*px+py*py+pz*pz) << std::endl;
      //  isSTopHad = Find_TopDecayProducts( particle1, B_tmp, WJ1_tmp, WJ2_tmp );
      //}
    }

  return isSTopHad;
}

int TruthxAODAnalysis_Higgsinos::Find_NeutralinoDecayProducts( const xAOD::TruthParticle* particle, 
						     TLorentzVector *Xi_tmp,  
						     TLorentzVector *WJ1_tmp, TLorentzVector *WJ2_tmp ) {

  if(verbose) std::cout << "Find_NeutralinoDecayProducts" << std::endl;
  //  const xAOD::TruthVertex* decayVtx = particle->decayVtx();
  const xAOD::TruthParticle* particle_daughter;
  //  const xAOD::TruthVertex* decayVtx_daughter = decayVtx;

  int npart_daughter = particle->nChildren(); //decayVtx_daughter->nOutgoingParticles();
  int pdgId, status, isSTopHad;


  //------------------------------------------------------------//
  //       Look at decay vertex see what the ino decays to     //

  if (verbose) std::cout << "npart daughter before while is : " << npart_daughter << std::endl;

  bool foundIno = false;

  while (!foundIno){
    //particle_daughter = particle->child(0); //decayVtx_daughter->outgoingParticle(0);
    //decayVtx = decayVtx_daughter;
    //decayVtx_daughter = particle_daughter->decayVtx();
    npart_daughter = particle->nChildren();//decayVtx_daughter->nOutgoingParticles();

    if ( npart_daughter == 1 ) {
      foundIno = false;
      particle_daughter = particle->child(0);
      particle = particle_daughter;
    }
    else  {
      foundIno = true;

      /*
      for ( int i=0; i<npart_daughter; i++) {
	particle_daughter = particle->child(i);
	int pdgId = particle_daughter->pdgId();//decayVtx_daughter->outgoingParticle(i)->pdgId();
	if ( abs(pdgId)==1000006 ) {
	  foundSTop = false;
	  particle = particle_daughter;
	}
      }
      */
    }

    //    particle = particle_daughter;
  }

  status = particle->status();
  if (verbose) std::cout << "npart daughter after while is : " << npart_daughter << std::endl;
  if (verbose) std::cout << "particle's status is: " << status << std::endl;

  //----------------------------------------------------------------//
  //               Found last ino in the chain.                    //
  //  This ino decay vertex should be the ino -> W/Z + Xi vertex  //
  //----------------------------------------------------------------//

  //  bool hasDecayVtx = particle->hasDecayVtx();
  //  if (hasDecayVtx){

  //    const xAOD::TruthVertex* decayVtx = particle->decayVtx();
  int npart = particle->nChildren(); //decayVtx->nOutgoingParticles();

    //----------------------------------------------------------------//
    //             Loop over decay products of the ino                //
    //----------------------------------------------------------------//

    for (int j=0; j<npart; j++){

      const xAOD::TruthParticle* particle1 = particle->child(j); //decayVtx->outgoingParticle1(j);
      pdgId = particle1->pdgId();
      status = particle1->status();

      double px = particle1->px()/GeV;
      double py = particle1->py()/GeV;
      double pz = particle1->pz()/GeV;
      double e = particle1->e()/GeV;

      if (verbose) std::cout << "The id of particle " << j << " of ino1 decayvtx is " << pdgId << std::endl;
      if (verbose) std::cout << "The status of particle "<< j << " of ino1 decayvtx " << status << std::endl;

      //----------------------------------------------//
      //     Find any susy particle1s, this is the Xi  //
      if (abs(pdgId) >= 100000){
	Xi_tmp->SetPxPyPzE(px,py,pz,e);
      }

      //-------------------------//
      //     Find the Z boson    //
      //if (abs(pdgId) == 23){
      //isSTopHad = Find_ZDecayProducts( particle1, WJ1_tmp, WJ2_tmp );          
      // }

      if (pdgId == 11 || pdgId==13 || pdgId==15) {
	      WJ1_tmp->SetPxPyPzE(px,py,pz,e);
	      //isSTopHad = 1;
      }
      if (pdgId == -11 || pdgId == -13 || pdgId== -15) {
	      WJ2_tmp->SetPxPyPzE(px,py,pz,e);
      }

      //no tops here 
      //if ( abs(pdgId) == 6 ) {
      //  Top_tmp->SetPxPyPzE(px,py,pz,e);
      //  if (verbose) std::cout << "stop1_t_p is " << sqrt(px*px+py*py+pz*pz) << std::endl;
      //  isSTopHad = Find_TopDecayProducts( particle1, B_tmp, WJ1_tmp, WJ2_tmp );
      //}
    }

  return isSTopHad;
}


void TruthxAODAnalysis_Higgsinos::SetVerbose(int i) {
  if ( i == 0 ) verbose = false;
  else verbose = true;
  return;
}

void TruthxAODAnalysis_Higgsinos::plot_signal(int icut, double weight){
}

void TruthxAODAnalysis_Higgsinos::FindThrustDir(){

  if (verbose) std::cout <<"finding thrust"<<std::endl;

  thrust_vec.SetXYZT(0,0,0,0);

  TString Formula("(abs(x*[0]+y*[1])+");
  TString tmp;

  uint nsize = 4+nISR;
  if ( nISR > 3 ) nsize = 7;
  for( uint i=1; i < nsize; i++ ) {
    tmp = TString(Form("abs(x*[%u]+y*[%u])+",2*i,2*i+1));
    Formula = Formula+tmp;
    }

  tmp = TString(Form("abs(x*[%u]+y*[%u]))",2*nsize,2*nsize+1));
  Formula = Formula+tmp;

  TF2 fcn("ThrustFunction",Formula.Data(),-1.,1.,-1.,1.);

  fcn.FixParameter(0, charg_wj1.Px());
  fcn.FixParameter(1, charg_wj1.Py());
  fcn.FixParameter(2, charg_wj2.Px());
  fcn.FixParameter(3, charg_wj2.Py());

  fcn.FixParameter(4, neutr2_wj1.Px());
  fcn.FixParameter(5, neutr2_wj1.Py());
  fcn.FixParameter(6, neutr2_wj2.Px());
  fcn.FixParameter(7, neutr2_wj2.Py());

  fcn.FixParameter(8, dixi.Px());
  fcn.FixParameter(9, dixi.Py());

  for (int i = 0; i<nISR; i++) {
    fcn.FixParameter(8+2*i, ISR_p[i][0]);
    fcn.FixParameter(9+2*i, ISR_p[i][1]);
  }
  /*
  if ( nISR > 1 ) {
    fcn.FixParameter(12, ISR_p[;
    fcn.FixParameter(13, ISR2.Py());
  }
  if ( nISR > 2 ) {
    fcn.FixParameter(14, ISR3.Px());
    fcn.FixParameter(15, ISR3.Py());
    }*/

  double bestX, bestY;
  fcn.Eval(0.5,0.5);
  fcn.GetMaximumXY(bestX,bestY);

  double total = sqrt(bestX*bestX+bestY*bestY);
  bestX = bestX/total;
  bestY = bestY/total;

  // point positive in the direction of the met                                                     
  if ( (dixi.Px()*bestX + dixi.Py()*bestY) < 0 ) {
    bestX = -bestX;
    bestY = -bestY;
  }

  thrust_vec.SetXYZT(bestX,bestY,0,0);

  double bestPerpX, bestPerpY;
  bestPerpX = 1.0 - bestX*bestX;
  bestPerpY = 0.0 - bestX*bestY;
  double totPerp = sqrt(bestPerpX*bestPerpX+bestPerpY*bestPerpY);
  bestPerpX = bestPerpX/totPerp;
  bestPerpY = bestPerpY/totPerp;

  //  define positive as cross product with positive result              
  if ( (bestX*bestPerpY - bestPerpX*bestY) < 0 ) {
    bestPerpX = - bestPerpX;
    bestPerpY = - bestPerpY;
  }

  thrust_perp_vec.SetXYZT(bestPerpX,bestPerpY,0,0);

  return;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: finalize ()
{
  ps->Close();
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthxAODAnalysis_Higgsinos :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}

//  LocalWords:  DeltaEta
