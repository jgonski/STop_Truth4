#include <MyAnalysis/TruthxAODAnalysis_Higgsinos.h>
#include <MyAnalysis/TruthxAODAnalysis_ttbar.h>

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class MyAnalysis_Higgsinos::TruthxAODAnalysis_Higgsinos+;
#pragma link C++ class MyAnalysis_ttbar::TruthxAODAnalysis_ttbar+;
#endif
